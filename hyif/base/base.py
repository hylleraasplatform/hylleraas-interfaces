from dataclasses import replace
from pathlib import Path
from typing import List, Optional, Union

from hyobj import HylleraasObject, Units
from hyset import (ComputeSettings, RunSettings, create_compute_settings,
                   create_run_settings)
from hyset.compute import acompute, compute
from hytools.logger import get_logger

from ..abc import HylleraasInterface
from ..utils import unique_filename


class HylleraasInterfaceBase(HylleraasInterface):
    """Base class for Interfaces containing commonly used routines."""

    def __repr__(self):
        """Print dataclass-like representaion of __class__."""
        class_name = self.__class__.__name__
        attributes = []
        for k in self.__dict__.keys():
            try:
                v = str(self.__dict__[k])
            except (TypeError, AttributeError):
                v = self.__dict__[k].__class__.__name__
            attributes.append(f'{k}={v}')
        return f'{class_name}({attributes})'

    def __str__(self):
        """Return a string representation of the object."""
        return self.__repr__()

    @property
    def units_default(self) -> Units:
        """Return the default units for the class."""
        # raise NotImplementedError('units_default must be implemented')

    @property
    def author(self) -> str:
        """Return the author's email address."""
        # raise NotImplementedError('author must be implemented')

    def setup(self, *args, **kwargs):
        """Set up the calculation."""
        # raise NotImplementedError('setup must be implemented')

    def parse(self, *args, **kwargs):
        """Parse the output."""
        # raise NotImplementedError('parse must be implemented')

    def initiate_interface(self, *args, **kwargs):
        """Initialize common variables for the class.

        This method accepts a variable number of positional and keyword
        arguments, initializes various settings, and logs debug information.

        Parameters
        ----------
        *args : tuple
            A variable number of positional arguments. Only the first argument
            is used as the method dictionary. If more than one argument is
            passed, an error log is created.
        **kwargs : dict
            A variable number of keyword arguments. These are used to configure
            various settings.

        Attributes Set
        --------------
        logger : Logger
            A logger with the name specified by the keyword argument
            `logger_name`. If no `logger_name` is provided, 'hyif' is used as
            the default.
        compute_settings : dict
            Set by calling the `_set_compute_settings` method with the passed
            keyword arguments.
        method : dict
            Set by calling the `_set_method` method with the passed positional
            and keyword arguments.
        run_settings : dict
            Created by calling the `hyset.create_run_settings` function with
            `compute_settings` as the argument.
        units : str
            Set to the value specified by the 'units' keyword in the `method`
            dictionary. If 'units' is not specified, the default value of
            `units_default` is used.
        version : str or None
            If the version is to be checked (based on the value of
            'check_version' in the `method` dictionary and the `arch_type` in
            `compute_settings`), `version` is set by calling the
            `check_version` method. Otherwise, `version` is set to None.

        Returns
        -------
        None

        """
        self.logger = get_logger(logger_name=kwargs.pop('logger_name', 'hyif'),
                                 **kwargs)
        if len(args) > 1:
            self.logger.error(
                'Too many arguments.' +
                'Only the first positional argument is used as method dict.'
            )
        self.compute_settings = self._set_compute_settings(**kwargs)
        self.method = self._set_method(*args, **kwargs)
        self.run_settings = create_run_settings(self.compute_settings)
        self.units = self.method.get('units',
                                     getattr(self, 'units_default', None))

        check_version = self.method.get(
            'check_version', self.compute_settings.arch_type == 'local'
        )
        try:
            self.version = self.check_version() if check_version else None
        except Exception as e:
            self.logger.error('Error in check_version: %s', e)
            self.version = None
        self._log_debug_info()

    def _log_debug_info(self):
        """Log debug info."""
        self.logger.debug('\nINTERFACE INITIATED\n')
        self.logger.debug('logger: %s\n', self.logger)
        self.logger.debug('compute_settings: %s\n', self.compute_settings)
        self.logger.debug('method: %s\n', self.method)
        self.logger.debug('run_settings: %s\n', self.run_settings)
        self.logger.debug('units: %s\n', self.units)
        self.logger.debug('version: %s\n', self.version)

    def _set_compute_settings(self, **kwargs) -> ComputeSettings:
        """Set compute settings for the class.

        This method accepts a variable number of keyword arguments, checks if
        a 'compute_settings' argument is provided, and if not, creates default
        compute settings. It then checks if a file named 'compute_settings.yml'
        exists in the 'overwrite' directory, and if it does, overwrites the
        compute settings with the settings from the file.

        Parameters
        ----------
        **kwargs : dict
            A variable number of keyword arguments. The 'compute_settings'
            argument, if provided, is used to set the initial compute settings.

        Returns
        -------
        ComputeSettings
            The final compute settings, either provided through
            'compute_settings', created by default, or loaded from
            'compute_settings.yml' if it exists.

        """
        compute_settings = (kwargs.pop('compute_settings', None) or
                            create_compute_settings())
        p = Path('./overwrite/compute_settings.yml')
        if p.exists():
            compute_settings = create_compute_settings(p)
        return replace(compute_settings)

    def _set_method(self, *args, **kwargs) -> dict:
        """Set the method dictionary for the class.

        This method accepts a variable number of positional and keyword
        arguments. It first checks if a positional argument is provided, and
        if not, it looks for a 'method' keyword argument. If neither is
        provided, it uses the remaining keyword arguments as the method
        dictionary. If the final method is not a dictionary, it raises a
        TypeError.

        Parameters
        ----------
        *args : tuple
            A variable number of positional arguments. The first argument, if
            provided, is used as the method dictionary.
        **kwargs : dict
            A variable number of keyword arguments. The 'method' argument, if
            provided and no positional arguments are provided, is used as the
            method dictionary.

        Returns
        -------
        dict
            The method dictionary.

        Raises
        ------
        TypeError
            If the final method is not a dictionary.

        """
        method = (args[:1] or [kwargs.pop('method', {})])[0]
        # if len(method) == 0 and kwargs:
        #     method = kwargs
        method.update(kwargs)
        if not isinstance(method, dict):
            raise TypeError(
                f'Expected dict for method, got {type(method).__name__}.'
            )
        return method

    def initiate_setup(self, *args, **kwargs) -> RunSettings:
        """Set up run_settings for setup function.

        This method accepts a variable number of keyword arguments. It first
        retrieves the current run settings or creates them if they don't exist.
        Then, if a 'run_opt' keyword argument is provided, it updates the run
        settings with 'run_opt'. The updated run settings are returned, but the
        original run settings are not modified.

        Parameters
        ----------
        **kwargs : dict
            A variable number of keyword arguments. The 'run_opt' argument, if
            provided, is used to update the run settings.

        Returns
        -------
        RunSettings
            A copy of the run settings, possibly updated with 'run_opt'.

        """
        run_settings = getattr(self, 'run_settings',
                               create_run_settings(self.compute_settings))
        return replace(run_settings, **kwargs.pop('run_opt', {}))

    def unique_filename(self, input_string: Union[list, List[str]],
                        split_char: Optional[str] = None) -> str:
        """Return a unique filename based on the input string.

        This method accepts a string and returns a unique filename based on the
        input_string. The filename is unique in the sense that it is not the
        same as any other filename in the current directory.

        Parameters
        ----------
        input_string : str
            The input string to be used as the basis for the filename.
        split_char : str, optional
            The character used to split the input string into a list of
            strings. Defaults to newline.

        Returns
        -------
        str
            A unique filename based on the input string.

        """
        if isinstance(input_string, str):
            split_char = split_char or '\n'
            inp = input_string.split(split_char)
        elif isinstance(input_string, list):
            inp = input_string

        return unique_filename(inp)

    def run(self, obj: HylleraasObject, *args, **kwargs):
        """Run calculation in a context-based manner.

        This method accepts an object of type HylleraasObject, along with a
        variable number of positional and keyword arguments. It runs the
        calculation within a context created by the `compute` function.

        Parameters
        ----------
        obj : HylleraasObject
            The object on which the calculation is to be run.
        *args : tuple
            A variable number of positional arguments to be passed to the
            `compute` function.
        **kwargs : dict
            A variable number of keyword arguments to be passed to the
            `compute` function.

        Returns
        -------
        Any
            The result of the calculation, as returned by the `compute`
            function.

        """
        with compute(self,
                     obj,
                     *args,
                     **kwargs) as r:
            return r

    async def arun(self, obj: HylleraasObject, *args, **kwargs):
        """Run calculation in an asynchronous context-based manner.

        This method accepts an object of type HylleraasObject, along with a
        variable number of positional and keyword arguments. It runs the
        calculation within an asynchronous context created by the `acompute`
        function.

        Parameters
        ----------
        obj : HylleraasObject
            The object on which the calculation is to be run.
        *args : tuple
            A variable number of positional arguments to be passed to the
            `acompute` function.
        **kwargs : dict
            A variable number of keyword arguments to be passed to the
            `acompute` function.

        Returns
        -------
        Any
            The result of the calculation, as returned by the `acompute`
            function.

        """
        async with acompute(
            self,
            obj,
            *args,
            **kwargs
        ) as r:
            return r
