from abc import ABC, abstractmethod
from typing import Union

from hyobj import Units

output_type = Union[str, dict]


class HylleraasInterface(ABC):
    """Base class for Interfaces."""

    @property
    @abstractmethod
    def author(self) -> str:
        """Set the authors email adress."""

    @property
    @abstractmethod
    def units_default(self) -> Units:
        """Set the default units."""

    @abstractmethod
    def parse(self):
        """Parse output."""

    @abstractmethod
    def setup(self):
        """Set up."""
