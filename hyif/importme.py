from abc import ABC, abstractmethod
from typing import Union

output_type = Union[str, dict]


class HylleraasInterface(ABC):
    """Base class for Interfaces."""

    @property
    @abstractmethod
    def version(self) -> str:
        """Set the version."""

    @property
    @abstractmethod
    def author(self) -> str:
        """Set the authors email adress."""
