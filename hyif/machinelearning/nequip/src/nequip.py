import copy
import inspect
import warnings
from dataclasses import replace
from pathlib import Path
from typing import Any, Dict, List, Optional, Tuple, Union

import numpy as np
import packaging.version as pv

try:
    import yaml
except ImportError:
    yaml = None

try:
    import pandas as pd

    df_type = pd.DataFrame
except ImportError:
    pd = None
    df_type = Any

from hyobj import DataSet
from hyset import (ComputeResult, ComputeSettings, RunSettings,
                   create_compute_settings)

from ....manager import File, acompute, compute
from ....utils import is_file, unique_filename
from ...abc import HylleraasMLInterface, Parser
from ...common import Common
from .input_parser import InputParser
from .units import NequIPUnits

VERSIONS_SUPPORTED = ['0.6.1']

data_type = Union[DataSet, str, Path]


class NequIP(HylleraasMLInterface, Common):
    """NequIP interface."""

    def __init__(
        self,
        method: dict,
        compute_settings: Optional[ComputeSettings] = None,
        neighbor_method: str = 'matscipy',
    ):
        """Initialize.

        Initializes the class instance with the given method and
        compute_settings.

        Args:
        ----
            method (dict): A dictionary containing the method information.
            compute_settings (Optional[ComputeSettings]): An optional
                ComputeSettings object.
            neighbor_method (str): The neighbor method to use
                ('ase', 'matscipy' or 'vesin').

        Return:
        ------
            None

        """
        if yaml is None:
            raise ImportError('yaml not installed')

        # set environment variable for a faster NequIP infer
        # do this when instantiating because NequIP is not imported yet
        import os

        self.nequip_nl = neighbor_method

        os.environ['NEQUIP_NL'] = self.nequip_nl

        if compute_settings is None:
            self.compute_settings = create_compute_settings()
        else:
            self.compute_settings = compute_settings

        self.method = method
        self.Runner = self.compute_settings.Runner
        self.inpParser: Parser = InputParser()  # type: ignore
        self.arch_type = getattr(self.compute_settings, 'arch_type', 'remote')

        if 'input' not in method.keys():
            raise ValueError('input not found in method dictionary')
        read_input = method.get('input', None)
        if isinstance(read_input, (str, Path)):
            self.inp: dict = self.inpParser.parse(read_input)
        elif isinstance(read_input, dict):
            self.inp = read_input
        else:
            raise ValueError(
                'input must be a string or Path (to .yaml file) or dict'
            )

        # TODO: implement restart
        files_for_restarting: List[Path] = []

        defaults = {
            'program': '',  # adjusted in each setup as argument
            'files_for_restarting': files_for_restarting,
        }
        self.run_settings = replace(
            self.compute_settings.run_settings, **defaults
        )

        self.version: pv.Version = (
            self.check_version()
            if self.method.get(
                'check_version', self.compute_settings.arch_type == 'local'
            )
            else None
        )
        self.debug = self.method.get('debug', False)
        self.units = self.method.get('units', NequIPUnits)
        self.type_map = self.method.get('type_map', None)

        if self.type_map is None:  # infer type_map from input
            if 'type_names' not in self.inp.keys():
                try:
                    self.type_map = self.get_type_map_from_input(self.inp)
                except ValueError:
                    self.type_map = (
                        None  # will use sorted type_map in setup_train
                    )
        else:  # check if type_map is consistent with input
            try:
                type_map = self.get_type_map_from_input(self.inp)
                if type_map != self.type_map:
                    raise AssertionError(
                        'type_map argument is inconsistent with input'
                    )
            except ValueError:
                pass

        # if we got a dictionary, convert the type_map to a list
        if self.type_map is not None:
            self.type_map = self.fix_type_map(self.type_map)

    def check_version(self):
        """Check deepmd version."""
        version = None
        try:
            import nequip

            version = pv.parse(nequip.__version__)
        except ImportError:
            raise ImportError('nequip not installed.')

        if version not in (pv.parse(v) for v in VERSIONS_SUPPORTED):
            raise NotImplementedError(
                f' NequIP version {version} not supported.',
                f' Please contact {self.author} ',
            )
        return version

    def fix_type_map(self, type_map: Any):
        """Turn type_map into a list in case it isn't."""
        if isinstance(type_map, dict):
            list_type_map = [''] * len(type_map.keys())
            for key, value in type_map.items():
                list_type_map[value] = key
            type_map = list_type_map
        elif isinstance(type_map, np.ndarray):
            type_map = type_map.tolist()
        elif not isinstance(type_map, list):
            raise ValueError('type_map should be a dict or a list')

        return type_map

    def get_type_map_from_input(self, input_file: Union[str, Path, dict]):
        """Get type map."""
        if isinstance(input_file, (str, Path)):
            input_dict = self.inpParser.parse(input_file)
        else:
            input_dict = input_file  # already a dict

        def recursive_search(dictionary, target):
            for k, v in dictionary.items():
                if k == target:
                    return v
                elif isinstance(v, dict):
                    found = recursive_search(v, target)
                    if found is not None:
                        return found
                elif isinstance(v, list):
                    for d in v:
                        if isinstance(d, dict):
                            found = recursive_search(d, target)
                            if found is not None:
                                return found
            return None

        type_map = recursive_search(input_dict, 'chemical_symbol_to_type')
        if type_map is None:
            raise ValueError('type_map not found in input')

        # check if type_map is a dictionary and convert to a list if needed
        return self.fix_type_map(type_map)

    def get_type_map_from_model(self, model: Any):
        """Get type map."""
        if isinstance(model, (str, Path)):
            _, graph_opt = self.preload_model(str(model), device='cpu')
        else:
            try:
                _, graph_opt = model
            except Exception:
                raise ValueError('Could not get type map from model')

        type_map = graph_opt['type_names'].split()
        return self.fix_type_map(type_map)

    def get_type_map(self, any_arg: Any = None) -> dict:
        """Get type map for DeePMD model."""
        warning_msg = (
            'Using type_map from constructor or previous training.'
            ' This can be unreliable.'
        )
        if isinstance(any_arg, Dict):
            try:
                return self.get_type_map_from_input(any_arg)
            except ValueError:
                pass

        if any_arg is None:
            warnings.warn(warning_msg)
            type_map = self.type_map
            if type_map is None:
                raise ValueError('Type map cannot be inferred.')
            else:
                return self.fix_type_map(type_map)

        try:
            return self.get_type_map_from_model(any_arg)
        except Exception:
            pass
        if isinstance(any_arg, (str, Path)):
            try:
                return self.get_type_map_from_input(any_arg)
            except Exception:
                pass
        warnings.warn(warning_msg)
        type_map = self.type_map
        if type_map is None:
            raise ValueError('Type map cannot be inferred.')
        else:
            return self.fix_type_map(type_map)

    def author(self):
        """Get author of this interface."""
        return 'h.m.cezar@kjemi.uio.no'

    async def atrain(self, data: data_type, **kwargs) -> dict:
        """Train model asynchronously.

        Arguments
        ---------
        data (data_type): The training data.
        **kwargs: Additional keyword arguments.

        Returns
        -------
        dict: The output of the training.

        """
        run_opt = kwargs.pop('run_opt', {})
        validation_set = kwargs.pop('validation_set', None)
        validation_fraction = kwargs.pop('validation_fraction', None)
        rng = kwargs.pop('rng', None)
        rng_seed = kwargs.pop('rng_seed', 0)

        if validation_set is None:
            if validation_fraction is None:
                raise ValueError(
                    'Either validation set or validation_fraction '
                    'must be set for Nequip training'
                )
            else:
                if rng is None:
                    rng = np.random.default_rng(rng_seed)
                if not isinstance(data, DataSet):
                    data = DataSet(data)
                data, validation_set = self.split_dataset(
                    data, validation_fraction, rng, safe=True
                )
        else:
            if validation_fraction is not None:
                raise ValueError(
                    'Cannot provide both validation_set '
                    'and validation_fraction'
                )

        setup_train = self.setup(
            data,
            validation_set=validation_set,
            train=True,
            run_opt=run_opt,
            **kwargs,
        )

        setup_deploy = self.setup(
            setup_train.output_file.name,
            deploy=True,
            run_opt=run_opt,
            **kwargs,
        )
        setup_deploy.data_files = []

        output_unparsed = await self.compute_settings.arun(
            [setup_train, setup_deploy]
        )

        output: Dict[str, Any] = dict()
        train = self.parse(output_unparsed[0])
        deploy = self.parse(output_unparsed[1])

        all_converged = True
        if 'is_converged' in train.keys():
            if not all(train['is_converged']):
                output.update({'is_converged': False})
                all_converged = False

        if all_converged:
            output.update({'train': train})

        all_converged = True
        if 'is_converged' in deploy.keys():
            if not all(deploy['is_converged']):
                output.update({'is_converged': False})
                all_converged = False

        if all_converged:
            if 'type_names' not in self.inp.keys():
                self.type_map = self.get_type_map_from_input(train)
            output.update(
                {
                    'deploy': deploy,
                    'model': deploy['model'],
                    'type_map': self.type_map,
                }
            )

        if self.debug:
            print(f'TRAINING RESULTS:\n {output}')

        return output

    def split_dataset(
        self,
        dataset: DataSet,
        split_ratio,
        rng: np.random.Generator,
        safe=False,
    ) -> Tuple[DataSet, DataSet]:
        """Split a dataset into two datasets."""
        if len(dataset) == 0:
            raise ValueError('Dataset is empty')

        num_train = int(np.round(len(dataset.data) * (1 - split_ratio)))

        # If safe and num_train is 0, set it to 1 to avoid empty training set
        if safe and num_train == 0:
            num_train = 1
        num_test = len(dataset) - num_train

        if num_train > 0:
            train_set = dataset.data.sample(n=num_train, random_state=rng)
        if num_test > 0:
            if num_train == 0:
                test_set = dataset.data
            else:
                test_set = dataset.data.drop(train_set.index)

        if num_train == 0:
            train_set = None  # DataSet(pd.DataFrame())
        else:
            train_set.reset_index(drop=True, inplace=True)
            train_set = DataSet(train_set, units=dataset.units)

        if num_test == 0:
            test_set = None  # DataSet(pd.DataFrame())
        else:
            test_set.reset_index(drop=True, inplace=True)
            test_set = DataSet(test_set, units=dataset.units)

        return train_set, test_set

    def train(self, data, **kwargs):
        """Train model.

        Arguments
        ---------
        data (data_type): The training data.
        **kwargs: Additional keyword arguments.

        Returns
        -------
        dict: The output of the training.

        """
        run_opt = kwargs.pop('run_opt', {})
        validation_set = kwargs.pop('validation_set', None)
        validation_fraction = kwargs.pop('validation_fraction', None)
        rng = kwargs.pop('rng', None)
        rng_seed = kwargs.pop('rng_seed', 0)

        if validation_set is None:
            if validation_fraction is None:
                raise ValueError(
                    'Either validation set\
or validation_fraction must be set for Nequip training'
                )
            else:
                if rng is None:
                    rng = np.random.default_rng(rng_seed)
                if not isinstance(data, DataSet):
                    data = DataSet(data)
                data, validation_set = self.split_dataset(
                    data, validation_fraction, rng, safe=True
                )

        setup_train = self.setup(
            data,
            validation_set=validation_set,
            train=True,
            run_opt=run_opt,
            **kwargs,
        )
        setup_deploy = self.setup(
            setup_train.output_file.name, deploy=True, run_opt=run_opt
        )
        setup_deploy.data_files = []

        output_unparsed = self.compute_settings.run(
            [setup_train, setup_deploy]
        )

        output: Dict[str, Any] = dict()
        train = self.parse(output_unparsed[0])
        deploy = self.parse(output_unparsed[1])

        all_converged = True
        if 'is_converged' in train.keys():
            if not all(train['is_converged']):
                output.update({'is_converged': False})
                all_converged = False

        if all_converged:
            output.update({'train': train})

        all_converged = True
        if 'is_converged' in deploy.keys():
            if not all(deploy['is_converged']):
                output.update({'is_converged': False})
                all_converged = False

        if all_converged:
            if 'type_names' not in self.inp.keys():
                self.type_map = self.get_type_map_from_input(train)
            output.update(
                {
                    'deploy': deploy,
                    'model': deploy['model'],
                    'type_map': self.type_map,
                }
            )

        if self.debug:
            print(f'TRAINING RESULTS:\n {output}')

        return output

    def setup(self, data: Union[DataSet, Path, str], **kwargs) -> RunSettings:
        """Set up the run."""
        run_settings = copy.deepcopy(self.run_settings)

        if 'run_opt' in kwargs.keys():
            run_settings = replace(run_settings, **kwargs.pop('run_opt'))

        program_opt = kwargs.pop('program_opt', None)
        if program_opt is None:
            program_opt = {}

        if data is None:
            return run_settings

        nopt = sum(
            map(
                bool,
                [
                    kwargs.get('train'),  # nequip-train
                    kwargs.get('deploy'),  # nequip-deploy
                    kwargs.get('test'),  # nequip-evaluate
                    kwargs.get('infer'),  # ase
                ],
            )
        )
        if nopt != 1:
            raise ValueError(
                'only one of train, deploy, test or infer has to be True'
            )

        for k in ['train', 'deploy', 'test', 'infer']:
            if kwargs.pop(k, False):
                return getattr(self, f'setup_{k}')(
                    run_settings, data, **program_opt, **kwargs
                )

        raise ValueError('No valid option for setup')

    def parse(self, output: ComputeResult, **kwargs) -> dict:
        """Parse output."""
        result: Dict[str, Union[str, list, dict, float, np.ndarray]] = {}

        # create a loader that ignores unknown keys
        # avoding loading torch/e3nn keys
        class SafeLoaderIgnoreUnknown(yaml.SafeLoader):
            def ignore_unknown(self, node):
                return None

        SafeLoaderIgnoreUnknown.add_constructor(
            None, SafeLoaderIgnoreUnknown.ignore_unknown
        )

        # units = kwargs.get('units', None)

        if self.debug:
            print('DEBUG: PARSER, output:', output)
        if output.files_to_parse:
            if not isinstance(output.files_to_parse, list):
                output.files_to_parse = [output.files_to_parse]
            for f in output.files_to_parse:
                if isinstance(f, File):
                    p = Path(f.work_path_local)
                elif isinstance(f, (str, Path)):
                    p = Path(f)
                else:
                    raise TypeError(
                        'files_to_parse must be a list of File, str or Path'
                    )
                if p.name == 'log':
                    with open(p, 'rt') as ff:
                        result[str(p)] = ff.read()
                if p.suffix == '.yaml':
                    with open(p, 'r') as ff:
                        result[str(p)] = yaml.load(
                            ff, Loader=SafeLoaderIgnoreUnknown
                        )
                if p.name == 'best_model.pth':
                    result['out_dir'] = p.parent
                    result['model'] = str(p)
                if p.suffix == '.csv':
                    try:
                        df = pd.read_csv(p)
                    except Exception:
                        result['is_converged'] = [False]
                        return result

                    fields = df.columns.values.tolist()
                    if 'training_e_rmse' in fields:
                        result['energy_rmse'] = df['training_e_rmse'].values
                    if 'training_e/N_rmse' in fields:
                        result['energy_rmse_per_atom'] = df[
                            'training_e/N_rmse'
                        ].values
                    if 'training_f_rmse' in fields:
                        result['force_rmse'] = df['training_f_rmse'].values
                    if 'training_f/N_rmse' in fields:
                        result['force_rmse_per_atom'] = df[
                            'training_f/N_rmse'
                        ].values
                    if 'training_virial_rmse' in fields:
                        result['virial_rmse'] = df[
                            'training_virial_rmse'
                        ].values
                    if 'training_virial_rmse_per_atom' in fields:
                        result['virial_rmse_per_atom'] = df[
                            'training_virial_rmse_per_atom'
                        ].values

        if output.output_file:
            try:
                output_file = Path(output.output_file)
            except (ValueError, OSError):
                result['is_converged'] = [False]
                return result

            if output_file.suffix == '.pth':
                result['model'] = str(output_file)

            if 'output_train.out' in str(output_file):
                with open(output_file, 'r') as f:
                    for line in f:
                        if line.strip().startswith('e_rmse'):
                            result['e_rmse'] = float(line.split('=')[1])
                        if line.strip().startswith('e/N_rmse'):
                            result['e/N_rmse'] = float(line.split('=')[1])
                        if line.strip().startswith('f_rmse'):
                            result['f_rmse'] = float(line.split('=')[1])
            result['output_file'] = str(output_file)

        if output.stdout:
            result['stdout'] = output.stdout

        if output.stderr:
            result['stderr'] = output.stderr

        return result

    def setup_deploy(
        self, run_settings: RunSettings, nequip_train_dir, **kwargs
    ) -> RunSettings:
        """Set up the deploy of a model."""
        running_dict = {}

        model_name = kwargs.pop('model_name', nequip_train_dir)
        model_filename = f'{Path(model_name).name}' + '.pth'

        input_dir = File(
            name=nequip_train_dir, handler=run_settings.file_handler
        )
        out_file = File(name=model_filename, handler=run_settings.file_handler)

        args = [
            'build',
            '--train-dir',
            str(input_dir.path.name),
            str(out_file.path),
        ]
        output_file = out_file

        if self.debug:
            print('DEBUG: DEPLOYING MODEL FROM:', str(input_dir.path))

        sub_dir = getattr(run_settings, 'sub_dir')
        if sub_dir is None:
            sub_dir = '.'

        running_dict.update(
            {
                'program': 'nequip-deploy',
                'args': args,
                'output_file': output_file,
                'sub_dir': sub_dir,
                'data_files': [input_dir],
            }
        )

        run_settings = replace(run_settings, **running_dict)
        if self.debug:
            print('DEBUG: RUN SETTINGS DEPLOY:', run_settings)

        return run_settings

    def setup_train(
        self,
        run_settings: RunSettings,
        data: Union[DataSet, str, Path],
        validation_set: Union[DataSet, str, Path],
        **kwargs,
    ) -> RunSettings:
        """Set up the training of a model."""
        running_dict: Dict[str, Any] = {}

        train_args = kwargs.pop('train_args', [])
        seed = kwargs.pop('seed', 1)
        log_batch_freq = kwargs.pop('log_batch_freq', 1)
        log_epoch_freq = kwargs.pop('log_epoch_freq', 1)
        max_epochs = kwargs.pop('max_epochs', 1000000)
        root_dir = kwargs.pop('root', '.')
        type_map = kwargs.get('type_map', self.type_map)

        sub_dir = getattr(run_settings, 'sub_dir')
        if sub_dir is None:
            sub_dir = Path('.')

        if not isinstance(data, DataSet):
            data = DataSet(data)

        if not isinstance(validation_set, DataSet):
            validation_set = DataSet(validation_set)

        n_train = len(data)
        n_val = len(validation_set)

        # Make sure stress is handled correctly before export to extxyz
        # dataset extxyz should use stress column of dataset, and that
        # stress column needs to be in energy/length^3 units, aka eV/Å^3
        # for nequip using metal units

        def dataset_virial_to_stress(dataset):
            def triclinic_cell_volume(cell_vectors):
                a, b, c = cell_vectors
                volume = np.dot(a, np.cross(b, c))  # Scalar triple product
                return abs(volume)  # Volume is always positive

            stress_list = []
            for i in range(len(dataset)):
                virial = dataset.data.iloc[i].virial
                volume = triclinic_cell_volume(
                    np.reshape(dataset.data.iloc[i].box, (3, 3))
                )
                stress = list(-np.array(virial) / volume)
                stress_list.append(stress)

            dataset.data.insert(1, 'stress', stress_list)
            return dataset

        if 'virial' in data.data.columns:
            data = dataset_virial_to_stress(data)
        if 'virial' in validation_set.data.columns:
            validation_set = dataset_virial_to_stress(validation_set)

        xyz_string = data.to_extxyz_string()
        xyz_string_val = validation_set.to_extxyz_string()

        # Make sure type map provided here does not conflict with
        # type map in template
        # This is to avoid surprising errors for the user, in particular if
        # Per atom energies from DFT are set

        if type_map is None:
            type_map = np.sort(np.unique(np.concatenate(data.data['atoms'])))

        type_map = self.fix_type_map(type_map)  # make sure it's a list

        input_dict = kwargs.get('input', self.inp)

        if 'type_names' in input_dict.keys():
            type_map = None

        if 'per_species_rescale_shifts' in input_dict.keys():
            if 'chemical_symbol_to_type' not in input_dict.keys():
                raise ValueError(
                    'Must provide chemical symbol to type mapping '
                    'if per_species_rescale_shifts is set'
                )

        if 'chemical_symbol_to_type' in input_dict.keys():
            type_dict = input_dict['chemical_symbol_to_type']
            type_map_template = [''] * len(type_dict.keys())
            for key, value in type_dict.items():
                type_map_template[value] = key
            if type_map != type_map_template:
                raise ValueError(
                    'Type mapping ("chemical_symbol_to_type") in Allegro '
                    'configuration input is incompatible with the type_map '
                    'inferred from the training dataset\n'
                    f'Inferred (or type_map override): {type_map}\n'
                    f'Input (e.g. template yaml): {type_map_template}\n'
                    'Must be either alphabetical or type_map override '
                    'consistent with template.'
                )

        if type_map is not None:
            input_dict['chemical_symbol_to_type'] = dict(
                {key: value for value, key in enumerate(type_map)}
            )

        input_dict['dataset'] = 'ase'
        input_dict['ase_args'] = {'format': 'extxyz'}
        input_dict['validation_dataset'] = 'ase'
        input_dict['n_train'] = n_train
        input_dict['n_val'] = n_val
        input_dict['seed'] = seed
        input_dict['root'] = root_dir
        input_dict['log_batch_freq'] = log_batch_freq
        input_dict['log_epoch_freq'] = log_epoch_freq
        input_dict['max_epochs'] = max_epochs

        # Before making the hash string, make sure that all keys that will be
        # set later are currently set to default value
        # This is because when launching multiple model trainings from the
        # same NequIP object, the previous values are stored
        # and if they are not reset here, then model
        # names are not deterministic.
        input_dict['validation_dataset_file_name'] = 'dummy'
        input_dict['dataset_file_name'] = 'dummy'
        input_dict['run_name'] = 'dummy'

        yaml_string = yaml.dump(input_dict, indent=4, sort_keys=True)

        folder_name = unique_filename(
            [yaml_string, xyz_string, xyz_string_val]
        )

        unique_datafile_name = folder_name + '.xyz'
        training_data = File(
            name=sub_dir / unique_datafile_name,
            content=xyz_string,
            handler=run_settings.file_handler,
        )

        unique_validationfile_name = folder_name + '_validation' + '.xyz'
        validation_data = File(
            name=sub_dir / unique_validationfile_name,
            content=xyz_string_val,
            handler=run_settings.file_handler,
        )

        if self.arch_type == 'local':
            datafile_path = training_data.scratch_path_local
            validation_datafile_path = validation_data.scratch_path_local
        else:
            datafile_path = training_data.name
            validation_datafile_path = validation_data.name

        input_dict['validation_dataset_file_name'] = str(
            validation_datafile_path
        )
        input_dict['dataset_file_name'] = str(datafile_path)

        # folder_name
        run_name = folder_name + '_output'
        input_dict['run_name'] = run_name
        input_str = yaml.dump(input_dict, indent=4, sort_keys=False)

        input_file = File(
            name=folder_name + '.yaml',
            content=input_str,
            handler=run_settings.file_handler,
        )

        files_to_write = [input_file, training_data, validation_data]

        args = [input_file]

        files_to_parse = [
            File(
                name=sub_dir / root_dir / run_name / 'metrics_epoch.csv',
                handler=run_settings.file_handler,
            ),
            File(
                name=sub_dir / root_dir / run_name / 'best_model.pth',
                handler=run_settings.file_handler,
            ),
            File(
                name=sub_dir / root_dir / run_name / 'config.yaml',
                handler=run_settings.file_handler,
            ),
        ]

        output_file = Path(root_dir) / run_name

        running_dict.update(
            {
                'program': 'nequip-train',
                'files_to_write': files_to_write,
                'args': args + train_args,
                'files_to_parse': files_to_parse,
                'data_files': [],
                'output_file': output_file,
                'sub_dir': sub_dir,
            }
        )

        run_settings = replace(run_settings, **running_dict)
        if self.debug:
            print('DEBUG: RUN SETTINGS TRAIN:', run_settings)
        return run_settings

    def setup_test(self, run_settings, data, **kwargs) -> RunSettings:
        """Set up the testing of a model.

        DataSet input here should be on the same format as training data.
        """
        running_dict = {}

        try:
            data_path = Path(data)
        except (ValueError, OSError):
            raise ValueError('data not found')

        if not data_path.is_dir():
            raise AssertionError(
                'the data for testing must be the output directory of training'
            )

        data_dir = File(name=data_path, handler=run_settings.file_handler)

        model = kwargs.get('model', None)
        if model is None:
            raise ValueError('model not specified')

        model_file = File(name=model, handler=run_settings.file_handler)

        args = [
            '--model',
            str(model_file.path.name),
            '--train-dir',
            str(data_dir.path.name),
        ]
        output_file = File(
            name='output_train.out', handler=run_settings.file_handler
        )

        sub_dir = getattr(run_settings, 'sub_dir')
        if sub_dir is None:
            sub_dir = Path('.')

        running_dict.update(
            {
                'program': 'nequip-evaluate',
                'args': args,
                'output_file': output_file,
                'stdout_file': output_file,
                'data_files': [data_dir, model_file],
                'sub_dir': sub_dir,
            }
        )

        if self.debug:
            print('DEBUG: RUN SETTINGS TEST:', run_settings)

        return replace(run_settings, **running_dict)

    def preload_model(
        self, model: str, run_settings: Optional[RunSettings] = None, **kwargs
    ) -> Any:
        """Preload model."""
        if run_settings is None:
            run_settings = self.run_settings
        if not is_file(model):
            model_file = File(
                name=Path(model), handler=run_settings.file_handler
            )
            model2 = model_file.data_path_local
            if is_file(model2):
                model = model2
            else:
                raise FileNotFoundError(f'could not find model {model}')

        device = kwargs.get('device', 'cuda')

        try:
            from nequip.scripts.deploy import load_deployed_model
        except ImportError:
            raise ImportError('could not find nequip')

        calculator, model_opt = load_deployed_model(model, device=device)

        return calculator, model_opt

    def setup_infer_external(
        self,
        model: Union[str, Path, List],
        data: Union[str, Path] = None,
        **kwargs,
    ) -> RunSettings:
        """Set up the inference of a model."""
        run_settings = copy.deepcopy(self.run_settings)
        if 'program_opt' in kwargs.keys():
            program_opt = kwargs.pop('program_opt', None)
            if program_opt is None:
                program_opt = {}
        else:
            program_opt = {}

        program = kwargs.pop('program', 'python')
        device = kwargs.get('device', 'cuda')
        nl_method = kwargs.pop('nl_method', self.nequip_nl)

        # check for run_opt
        if 'run_opt' in kwargs.keys():
            run_settings = replace(run_settings, **kwargs.pop('run_opt'))
            # make sure that work_dir_local is existing
            run_settings.work_dir_local.mkdir(parents=True, exist_ok=True)
            run_settings.data_dir_local.mkdir(parents=True, exist_ok=True)

        if isinstance(data, DataSet):
            dataset = data
        else:
            dataset = DataSet(data)

        # check if multiple models are given
        if isinstance(model, List):
            models = copy.deepcopy(model)
        else:
            models = [model]

        dataset_tmp = copy.deepcopy(dataset)

        if dataset_tmp.has_units:
            dataset_tmp.change_units(self.units)
        else:
            if self.debug:
                warnings.warn(
                    'dataset has no units, using default units for inference'
                )
            dataset_tmp.change_units(self.units)
        dataset_tmp.standardize_columns()
        hash_dataset = dataset_tmp.hash

        model_hashes = []
        for model in models:
            if isinstance(model, (str, Path)):
                model_basename = Path(model).name
                if model_basename.endswith('.pth'):
                    model_hashes.append(model_basename)
                else:
                    model_hashes.append(unique_filename([str(model)]))
            else:
                raise ValueError(
                    'model must be a path for external inference.'
                )

        hash_out = unique_filename(
            [hash_dataset]
            + model_hashes
            + [str(kwargs.get('full_output', False))]
        )
        # if kwargs.get('cleanup', False):
        # use same hash for input and output
        hash_in = hash_out
        input_file = File(
            name=hash_in + '_in.pkl', handler=run_settings.file_handler
        )
        dataset_tmp.write_data(input_file.work_path_local)
        model_files = []
        model_filenames = []
        for model in models:
            model_file = File(
                name=Path(model),  # type: ignore
                handler=run_settings.file_handler,
            )
            model_files.append(model_file)
            if self.arch_type != 'local':
                model_filenames.append(model_file.data_path_remote.name)
            else:
                model_filenames.append(model_file.data_path_local)

        if self.arch_type != 'local':
            data_filename = input_file.data_path_remote.name
            # Make sure is a basename
            data_filename = Path(data_filename).name
        else:
            data_filename = input_file.work_path_local

        data_files = [
            File(
                name=input_file.work_path_local,
                handler=run_settings.file_handler,
            )
        ] + model_files
        output_file = File(name=f'{hash_out}_out.pkl')

        model_filenames = [f"\"{m}\"" for m in model_filenames]
        model_hashes = [f"\"{m}\"" for m in model_hashes]
        data_filename = str(data_filename)
        output_filename = str(output_file.name)
        joined_model_filenames = ', '.join(model_filenames)
        joined_model_hashes = ', '.join(model_hashes)
        dataset_id = f"\"{hash_dataset}\""
        input_str = """
import pandas as pd
import os
os.environ["NEQUIP_NL"] = '{nl_method}'
import numpy as np
from nequip.scripts.deploy import load_deployed_model
from typing import List, Any

df_type = pd.DataFrame

data_in = pd.read_pickle('{data_filename}')
if 'dataset_id' in data_in.columns:
    dataset_ids = data_in['dataset_id'].values
else:
    data_in['dataset_id'] = [{dataset_id}]*len(data_in)
    data_in['dataset_id'].astype('category')
    dataset_ids = data_in['dataset_id'].values[0]

if 'geometry_id' not in data_in.columns:
    data_in['geometry_id'] = np.arange(len(data_in))
    data_in['geometry_id'].astype('category')

graphs = [
    load_deployed_model(model, device='{device}')
    for model in [{model_filenames}]
    ]

{infer_minimal_function}

df = infer_minimal(graphs, model_hashes=[{model_hashes}],
                   df=data_in, full_output={full_output})

df['energy'] = df['energy'].astype(np.float64)
df['forces'] = df['forces'].apply(lambda x: np.array(x, dtype=np.float64))
df['virial'] = df['virial'].apply(lambda x: np.array(x, dtype=np.float64))

if {full_output}:
    if 'box' in df.columns:
        df['box'] = df['box'].apply(lambda x: np.array(x, dtype=np.float64))

    df['coordinates'] = df['coordinates'].apply(lambda x:
                                                np.array(x, dtype=np.float64))
    df['atoms'] = df['atoms'].apply(lambda x: np.array(x))
    df['method_id'] = df['method_id'].astype('category')
    df['dataset_id'] = df['dataset_id'].astype('category')
    df['geometry_id'] = df['geometry_id'].astype(np.int64)

pd.to_pickle(df, '{output_filename}')
""".format(
            nl_method=nl_method,
            device=device,
            model_filenames=joined_model_filenames,
            model_hashes=joined_model_hashes,
            data_filename=data_filename,
            output_filename=output_filename,
            dataset_id=str(dataset_id),
            full_output=str(kwargs.get('full_output', False)),
            infer_minimal_function=inspect.getsource(infer_minimal),
        )

        input_filename = hash_out + '.py'
        input_script = File(name=input_filename, content=input_str)
        files_to_write = [input_script]
        files_to_parse = [input_script] + [output_file] + [input_file]

        running_dict = {
            'program': program,
            'args': str(input_script.name),
            'files_to_parse': files_to_parse,
            'files_to_write': files_to_write,
            'output_file': output_file,
            'data_files': data_files,
        }
        run_settings = replace(run_settings, **running_dict)
        return run_settings

    def parse_infer_external(self, output: ComputeResult, **kwargs) -> dict:
        """Parse output."""
        out_dict = {}
        try:
            out_dict.update(
                DataSet(
                    Path(output.output_file), units=self.units
                ).data.to_dict(orient='list')
            )

            if kwargs.get('cleanup', True):
                for filename in output.files_to_parse:
                    filename.unlink()
        except Exception:
            out_dict.update(dict(is_converged=[False]))

        return out_dict

    def infer_external(self, model, data, **kwargs):
        """Infer model on Dataset object.

        Arguments
        ---------
        model (str): The model to infer with.
        data (DataSet): The dataset to infer on, for hetrogenous data,
        use keyword "dataset_id"
        to infer on chunks with the atoms.
        **kwargs: Additional keyword arguments.

        Returns
        -------
        dict: The output of the inference.

        """
        setup_tmp = self.setup_infer_external(model, data, **kwargs)
        unparsed_output = self.compute_settings.run(setup_tmp)
        return self.parse_infer_external(unparsed_output, **kwargs)

    async def ainfer_external(self, model, data, **kwargs):
        """Infer model on Dataset object asynchronously.

        Arguments
        ---------
        model (str): The model to infer with. Multiple models can be
        given as a list of strings.
        data (DataSet): The dataset to infer on, for hetrogenous data,
        use keyword "dataset_id" and "geometry_id".
        to infer on chunks with the atoms.
        **kwargs: Additional keyword arguments.

        Returns
        -------
        dict: The output of the inference.

        """
        setup_tmp = self.setup_infer_external(model, data, **kwargs)
        unparsed_output = await self.compute_settings.arun(setup_tmp)
        return self.parse_infer_external(unparsed_output, **kwargs)

    # Duplicate ainfer_external to ainfer
    ainfer = ainfer_external

    def infer(
        self,
        model: Optional[data_type] = None,
        data: Optional[data_type] = None,
        **kwargs,
    ) -> dict:
        """Infer result from model."""
        if kwargs.get('run_external', False):
            return self.infer_external(model, data, **kwargs)
        if model is None:
            raise ValueError('model not specified')

        run_settings = kwargs.pop('run_settings', None)
        if run_settings is None:
            run_settings = self.run_settings
        if not isinstance(model, List):
            models = [model]
        if not isinstance(data, DataSet):
            data = DataSet(data)

        # Loop over models
        graphs = []
        model_hashes = []
        for model_idx, model_it in enumerate(models):
            if isinstance(model_it, (str, Path)):
                model_hash = Path(model_it).name
                try:
                    graph, graph_opt = self.preload_model(
                        str(model_it), run_settings=run_settings, **kwargs
                    )
                except ImportError:
                    return self.infer_external(
                        model, data, run_settings=run_settings
                    )
            else:  # use preloaded model
                graph, graph_opt = model_it
                try:
                    model_hash = kwargs.get('model_hashes')[model_idx]
                except Exception:
                    model_hash = f'model_{model_idx}'

            model_hashes.append(model_hash)
            graphs.append((graph, graph_opt))

        ds = copy.deepcopy(data)
        if ds.has_units:
            ds.change_units(self.units)
        else:
            if self.debug:
                warnings.warn(
                    'dataset has no units, '
                    + 'using default units for inference'
                )
            ds = DataSet(ds.data, units=self.units)
        ds.standardize_columns()

        if 'dataset_id' not in ds.data.columns:
            ds.data['dataset_id'] = [ds.hash] * len(ds.data)
        ds.data['dataset_id'] = ds.data['dataset_id'].astype('category')
        if 'geometry_id' not in ds.data.columns:
            ds.data['geometry_id'] = np.arange(len(ds.data))
            ds.data['geometry_id'].astype('category')

        try:
            return infer_minimal(
                graphs=graphs, model_hashes=model_hashes, df=ds.data, **kwargs
            ).to_dict(
                orient='list'
            )  # type: ignore
        except Exception as e:
            print('Exception in infer_minimal', e)
            return {'is_converged': [False] * len(ds.data)}

    def test(self, data, **kwargs):
        """Test model."""
        return self.run(data, test=True, **kwargs)

    def _concatenate_inputs(self, paths: List[File]) -> str:
        """Concatenate paths in a single input."""
        raise NotImplementedError('Not implemented yet!')

    def run(self, data, *args, **kwargs):
        """Run calculation context-based."""
        with compute(self, data, *args, **kwargs) as r:
            return r

    async def arun(self, data, *args, **kwargs):
        """Run calculation context-based and asyncronous."""
        async with acompute(self, data, *args, **kwargs) as r:
            return r


def infer_minimal(
    graphs: List[Any], model_hashes: List[str], df: pd.DataFrame, **kwargs
) -> pd.DataFrame:
    """Perform inference using NequIP models.

    Arguments
    ---------
        graphs (list): List of NequIP ase calculators.
        model_hashes (list): List of model hashes.
        df (pandas.DataFrame): DataFrame containing the dataset.
        **kwargs: Additional keyword arguments.

    Returns
    -------
        pandas.DataFrame: DataFrame containing the inference results.

    """
    results = pd.DataFrame()
    pbc = 'box' in df.columns

    device = kwargs.get('device', 'cuda')

    try:
        import torch
        from nequip.data import AtomicData
    except ImportError:
        raise ImportError('could not find nequip or torch')

    # definitions are here so they are accessible to infer_external
    def type_map(atoms, type_mapping):
        out_data = torch.full(
            (len(atoms),), fill_value=-1
        )  # Default value -1 for unknown types
        for atom_type, index in type_mapping.items():
            out_data[atoms == atom_type] = index
        return out_data

    def nequip_infer(graph, config, frame, device, pbc):
        type_names = config['type_names'].split()
        type_mapping = {type_names[i]: i for i in range(len(type_names))}

        box = frame.box if hasattr(frame, 'box') else None
        if box is not None and (
            np.shape(box) == (9,) or np.shape(box) == (1, 9)
        ):
            box = np.array(frame.box).reshape(3, 3)

        # mirror input/output data shapes
        coordinates = np.array(frame.coordinates).reshape(-1, 3)

        atoms_data = AtomicData.from_points(
            coordinates, r_max=config['r_max'], cell=box, pbc=pbc
        )

        atoms_data['atom_types'] = type_map(
            np.asarray(frame.atoms), type_mapping
        )
        atoms_data.to(device=device)
        input_dict = AtomicData.to_AtomicDataDict(atoms_data)

        return graph(input_dict)

    # Loop over dataset_ids
    for dataset_id in np.unique(df['dataset_id']):
        data = df[df['dataset_id'] == dataset_id]

        for (graph, graph_opt), model_hash in zip(graphs, model_hashes):

            nframes = len(data)

            result = {}
            e = []
            f = []
            v = []
            boxes = []
            full_output = kwargs.get('full_output', False)
            if full_output:
                coord = []
                atoms = []

            for frame in range(len(data)):

                return_shape_coord = np.shape(data.iloc[frame].coordinates)
                inferred = nequip_infer(
                    graph,
                    graph_opt,
                    data.iloc[frame],
                    device=device,
                    pbc=[pbc, pbc, pbc],
                )

                e.append(inferred['total_energy'].item())
                f.append(
                    inferred['forces']
                    .cpu()
                    .detach()
                    .reshape(return_shape_coord)
                    .tolist()
                )
                try:
                    if len(return_shape_coord) == 1:
                        v.append(
                            inferred['virial']
                            .cpu()
                            .detach()
                            .reshape(-1)
                            .tolist()
                        )
                    elif len(return_shape_coord) == 2:
                        v.append(inferred['virial'].cpu().detach().tolist())
                    else:
                        raise ValueError(
                            'Invalid shape of positions input array',
                            return_shape_coord,
                        )
                except KeyError:
                    v.append(None)  # model has no virial
                if len(return_shape_coord) == 1:
                    boxes.append(
                        inferred['cell'].cpu().detach().reshape(-1).tolist()
                    )
                elif len(return_shape_coord) == 2:
                    boxes.append(inferred['cell'].cpu().detach().tolist())

                if full_output:
                    coord.append(inferred['pos'].cpu().detach().tolist())
                    atoms.append(
                        [
                            graph_opt['type_names'].split()[x]
                            for x in inferred['atom_types']
                            .cpu()
                            .detach()
                            .tolist()
                        ]
                    )

            result = {'energy': e, 'forces': f, 'virial': v}
            if full_output:
                result.update(
                    {
                        'coordinates': coord,
                        'atoms': atoms,
                        'method_id': [model_hash] * nframes,
                        'dataset_id': [dataset_id] * nframes,
                        'geometry_id': np.array(data['geometry_id'].values)
                        .flatten()
                        .tolist(),
                    }
                )
                if pbc:
                    result['box'] = boxes

            if len(results) == 0:
                results = pd.DataFrame(result)
            else:
                results = pd.concat([results, pd.DataFrame(result)])
    return results
