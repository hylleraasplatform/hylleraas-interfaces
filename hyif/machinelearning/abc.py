from abc import ABC, abstractmethod
from pathlib import Path
from typing import Any, Optional, Union


class HylleraasMLInterface(ABC):
    """Base class for machinelearning."""

    @abstractmethod
    def train(self, data: Any, **kwargs):
        """Train a ML model."""

    @abstractmethod
    def test(self, dataset: Any):
        """Test a ML model."""

    @abstractmethod
    def infer(self,
              model: Optional[Any] = None,
              data: Optional[Any] = None,
              **kwargs
              ):
        """Predict data from a ML model."""

    @abstractmethod
    def check_version(self):
        """Check version."""


class Runner(ABC):
    """Runner base class."""

    @abstractmethod
    def run(self, input_dict: dict) -> Union[str, dict]:
        """Run a calculation."""


class Parser(ABC):
    """Parser base class."""

    @abstractmethod
    def parse(self, input_str: Union[str, Path]) -> dict:
        """Parse file or input_str."""
