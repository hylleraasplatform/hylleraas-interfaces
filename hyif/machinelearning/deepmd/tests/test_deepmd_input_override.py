from hyobj import Molecule, PeriodicSystem
from hyset import create_compute_settings

from hyif.machinelearning.deepmd import DeepMD

cs = create_compute_settings('local', debug=True)

mydp = DeepMD({'check_version': False}, compute_settings=cs)

water = PeriodicSystem(Molecule('O'), pbc=[4, 0, 0, 0, 4, 0, 0, 0, 4])

coordinates = 3 * [water.coordinates]
forces = 3 * [water.coordinates * 0.05]
energies = 3 * [-100.0]
atoms = 3 * [water.atoms]
boxes = 3 * [water.box]
dp_data_dict = {
    'coordinates': coordinates,
    'forces': forces,
    'energies': energies,
    'atoms': atoms,
    'boxes': boxes
}


def test_input_overrides():
    """Test that input overriding works."""
    dp_data = mydp.get_data(dp_data_dict)
    dp_data.dump_data(format='standard',
                      data_dir='./dp_data_test_input_overrides',
                      overwrite=True)
    dp_data = mydp.get_data('./dp_data_test_input_overrides', load=True)
    outgoing_run_settings = mydp.setup_train(
        run_settings=mydp.run_settings, data=dp_data)

    mlp_override = {'numb_steps': 100000,
                    'input_override': {
                        'loss': {
                            'start_pref_f': 0.1,
                            'limit_pref_e': 0.25
                        }
                    }}

    outgoing_run_settings_2 = mydp.setup_train(
        run_settings=mydp.run_settings, data=dp_data, **mlp_override)

    import json
    out_dict = json.loads(outgoing_run_settings.files_to_write[0].content)
    out_dict_2 = json.loads(outgoing_run_settings_2.files_to_write[0].content)

    def dict_diff(d1, d2):
        """Recursively find differences between two nested dictionaries.

        Args
            d1 (dict): The first dictionary.
            d2 (dict): The second dictionary.

        Returns
        -------
            dict: A dictionary containing differences between d1 and d2.

        """
        diff = {}
        # Combine keys from both dictionaries
        all_keys = d1.keys() | d2.keys()
        for key in all_keys:
            if key in d1 and key in d2:
                # If both are dictionaries, recurse
                if isinstance(d1[key], dict) and isinstance(d2[key], dict):
                    sub_diff = dict_diff(d1[key], d2[key])
                    if sub_diff:
                        diff[key] = sub_diff
                elif d1[key] != d2[key]:
                    diff[key] = {'d1': d1.get(key, 'Key not present'),
                                 'd2': d2.get(key, 'Key not present')}
            elif key in d1:
                diff[key] = {'d1': d1[key], 'd2': 'Key not present'}
            elif key in d2:
                diff[key] = {'d1': 'Key not present', 'd2': d2[key]}
        return diff

    diff_dict = dict_diff(out_dict, out_dict_2)

    expected_difference = {
        'training': {'numb_steps': {'d1': 1000, 'd2': 100000}},
        'loss': {'start_pref_f': {'d1': 1000, 'd2': 0.1},
                 'limit_pref_e': {'d1': 1, 'd2': 0.25}}
    }

    assert dict_diff(diff_dict, expected_difference) == {}
