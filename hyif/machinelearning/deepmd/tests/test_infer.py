
import os

import hyset
from hyobj import DataSet

import hyif


def test_infer():
    """Test infer function."""
    fn1 = 'graph-5e32e274f44f580ca30b2a9780caa81a6fc08030.pb'
    fn2 = 'graph-adscsadf.pb'

    if not os.path.exists(fn1) or not os.path.exists(fn2):
        os.system(f'cp graph.pb {fn1}')
        os.system(f'cp graph.pb {fn2}')

    test_set = DataSet(
        {
            'atoms': ['O', 'H', 'H'],
            'coordinates': [[0.0, 0.0, 0.0] + [0, 0, 1] + [0, 1, 0]],
            'box': [4, 0, 0, 0, 4, 0, 0, 0, 4],
            'energy': [-100.0],
            'forces': [0.0, 0.0, 0.0] * 3,
        },
        units='metal',
    )

    test_set2 = DataSet(
        {
            'atoms': ['O', 'H', 'H', 'H'],
            'coordinates': [
                [0.0, 0.0, 0.0] + [0, 0, 1] + [0, 1, 0] + [1, 0, 0]
            ],
            'box': [4, 0, 0, 0, 4, 0, 0, 0, 4],
            'energy': [-100.0],
            'forces': [0.0, 0.0, 0.0] * 4,
        },
        units='metal',
    )

    mydp = hyif.DeepMD(
        {'check_version': False},
        compute_settings=hyset.create_compute_settings('local', debug=True),
    )
    test_set.data['dataset_id'] = 'awsd'
    test_set2.data['dataset_id'] = 'sdfsedf'

    # concat
    test_set.append(test_set2)
    fn1 = os.path.abspath(fn1)
    fn2 = os.path.abspath(fn2)

    test_wrong = DataSet(
        {
            'atoms': ['O', 'H', 'H', 'H', 'H'],
            'coordinates': [
                [0.0, 0.0, 0.0] + [0, 0, 1] + [0, 1, 0] + [1, 0, 0]
            ],
            'box': [4, 0, 0, 0, 4, 0, 0, 0, 4],
            'energy': [-100.0],
            'forces': [0.0, 0.0, 0.0] * 4,
        },
        units='metal',
    )
    res = DataSet(
        mydp.infer_external(
            [fn1, fn2],
            test_wrong,
            run_opt=dict(sub_dir='test_wong'),
            cleanup=True,
            full_output=True,
        )
    )
    assert not res.data['is_converged'][0]
