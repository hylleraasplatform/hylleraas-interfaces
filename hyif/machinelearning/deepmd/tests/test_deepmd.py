import os
import pathlib
from datetime import timedelta

import hyset
import numpy as np
import pytest
from hyobj import DataSet, Molecule, PeriodicSystem, Units
from hyset import create_compute_settings

from hyif.machinelearning.deepmd import DeepMD, DeepMDData
from hyif.quantumchemistry.vasp import VASP

cs = create_compute_settings('local')

mydp = DeepMD({'check_version': False}, compute_settings=cs)

water = PeriodicSystem(Molecule('O'), pbc=[4, 0, 0, 0, 4, 0, 0, 0, 4])

coordinates = 3 * [water.coordinates]
forces = 3 * [water.coordinates * 0.05]
energies = 3 * [-100.0]
atoms = 3 * [water.atoms]
boxes = 3 * [water.box]
dp_data_dict = {
    'coordinates': coordinates,
    'forces': forces,
    'energies': energies,
    'atoms': atoms,
    'boxes': boxes,
}


def test_deepmddata():
    """Test DeepMDData.

    This function tests the functionality of the DeepMDData class.
    It performs various assertions to check if the class behaves as expected.
    """
    print(os.getcwd())
    data = DeepMDData()
    assert data.units.energy[0] == 'eV'
    with pytest.raises(ValueError):
        DeepMDData(format='wfef')
    data = DeepMDData(**dp_data_dict, format='mixed_type', frames=[0, 1])
    np.testing.assert_allclose(
        data.forces[0], np.array(forces).reshape(3, -1)[0]
    )
    assert len(data.energies) == 2
    assert data.type_map_raw == ['O', 'H']
    assert all(x == 0 for x in data.type_raw)
    assert len(data.type_raw) == 3
    np.testing.assert_allclose(data.real_atom_types[0], [0, 1, 1])
    type_raw, real_atom_types = data.pad(
        2, data.real_atom_types, data.type_raw
    )
    assert all(x == 0 for x in type_raw)
    assert len(type_raw) == 5
    np.testing.assert_allclose(real_atom_types[0], [0, 1, 1, -1, -1])

    data = DeepMDData(**dp_data_dict)
    np.testing.assert_allclose(data.type_raw, [0, 1, 1])
    assert data.real_atom_types is None

    data.dump_data(format='mixed_type', data_dir='./dp_data_test1', nopbc=True)
    data.dump_data(format='mixed_type', data_dir='./dp_data_test1', nopbc=True)
    data = DeepMDData(
        data_dir='./dp_data_test1', load=True, format='mixed_type'
    )
    ds = data.to_dataset()
    assert ds.data.shape == (6, 6)
    assert 'energies' in ds.data.columns

    data = DeepMDData(**dp_data_dict, units=Units('atomic'))
    np.testing.assert_allclose(
        data.energies / 27.2114, energies, atol=1e-4, rtol=1e-4
    )


def test_read_input():
    """Test reading input."""
    with pytest.raises(FileNotFoundError):
        mydp.InputParser.parse('test')


#     a = mydp.InputParser.parse(file)
#     assert a['training']['numb_steps'] == 100


# def test_check_version():
#     DeepMD({'check_version':True})


def test_deepmddata_interface():
    """Test reading data from directory."""
    ref = DeepMDData(**dp_data_dict)
    dp_data = mydp.get_data(ref)
    np.testing.assert_allclose(dp_data.coordinates, ref.coordinates)
    dp_data = mydp.get_data(dp_data_dict)
    np.testing.assert_allclose(dp_data.forces, ref.forces)
    dp_data = mydp.get_data(ref.to_dataset())

    np.testing.assert_allclose(dp_data.boxes, ref.boxes)
    dp_data = mydp.get_data(ref.to_dataset().data)
    np.testing.assert_allclose(dp_data.energies, ref.energies)
    dp_data.data_dir = './dp_data_test2'
    mydp.dump_data(dp_data)
    # dp_data.dump_data(format='standard', data_dir= './dp_data_test2')
    dp_data = mydp.get_data('./dp_data_test2', load=True)
    np.testing.assert_allclose(dp_data.coordinates, ref.coordinates)
    dp_data.dump_data(format='standard', data_dir='./dp_data_test3')


def test_compress_slurm():
    """Test compressing model in SLURM environment."""
    comp_settings = hyset.RemoteArch(
        target='dummy',
        host='dummy',
        progress_bar=False,
        data_dir_local=os.getcwd(),
        work_dir_local=os.getcwd(),
        submit_dir_remote=os.getcwd(),
        data_dir_remote=os.getcwd(),
        work_dir_remote=os.getcwd(),
        force_recompute=False,
        overwrite_files=True,
        job_time=timedelta(hours=1),
        slurm_account='dummy',
        memory_per_cpu=None,
        ntasks=128,
        cpus_per_task=1,
        user='dummy',
    )

    # setup a model_compression
    mydp = DeepMD({'check_version': False}, compute_settings=comp_settings)

    # make a dummy class object
    class DummyCls:
        def __init__(self):
            self.model = 'dummy'
            self.model = 'dummy_model'

    dummy = DummyCls()
    settings = mydp.setup_compress(mydp.run_settings, dummy)
    assert settings.launcher == 'srun -n1'


def test_integration():
    """Test deepmd."""
    v = mydp.check_version()
    assert v is not None
    out_train = mydp.train(['./dp_data_test2', './dp_data_test3'])
    assert pathlib.Path(out_train['model']).exists()
    out_test = mydp.test('./dp_data_test2', model=out_train['model'])
    assert out_test['energy_rmse'] > -1e-4
    out_infer = mydp.infer(
        data='./dp_data_test2', model=out_train['model_compressed']
    )

    np.testing.assert_allclose(out_infer['energies'], energies, rtol=5e-2)
    out_infer = mydp.infer(
        data='./dp_data_test2', model=out_train['model'], units=Units('atomic')
    )
    np.testing.assert_allclose(
        out_infer['energies'] * 27.2114, energies, rtol=5e-2
    )

    out_model_devi = mydp.model_deviation(
        data='./dp_data_test2', models=[out_train['model']]
    )
    assert out_model_devi['max_devi_v'] is not None


def test_deepmd_start_train_from_dataset():
    """Test deepmd."""
    vasp = VASP(method={})
    parser = vasp.OutputParser

    # Get dataset directly from file
    ds_out = DataSet(
        os.path.dirname(os.path.realpath(__file__))
        + '/../../../quantumchemistry/vasp/test/test.OUTCAR',
        parser=parser,
        parse=True,
    )

    deepmd_engine = DeepMD({'debug': False, 'check_version': False})

    try:
        train, graph, compressed = deepmd_engine.train(
            data=ds_out, program_opt={'numb_steps': 5000, 'seed': 1}
        )
    except ValueError as e:
        print(e)
        raise

    # When the "no data to train!" error has been resolved, this test should
    # catch the first error that pertains to the executable not being present,
    # or something like that
