import json
from pathlib import Path
from typing import Union

import numpy as np
from hyobj import DataSet

# from ....utils import is_file


class InputParser:
    """DeepMD input parser."""

    def parse(self, input: Union[str, Path]) -> dict:
        """Read Deepmd input."""
        # if Path(input).exists():
        #     with open(input, 'r') as f:
        #         return json.load(f)
        # else:
        #     return json.loads(str(input))
        with open(input, 'r') as f:
            return json.load(f)

    @classmethod
    def get_xyz(
        cls,
        fn_xyz: Union[Path, str],
    ) -> DataSet:
        """Read Deepmd xyz file."""
        # Assuming constant number of atoms and constant keywords
        data_dict: dict = {}
        i = 0
        with open(fn_xyz, 'r') as f:
            while True:
                try:
                    num_atoms = int(f.readline())
                except ValueError:
                    break
                comment_line = f.readline()

                keywords = comment_line.split(' ')
                keywords = [key for key in keywords if '=' in key]
                keywords = [key.split('=')[0] for key in keywords]
                values = comment_line.split('=')
                for i in range(len(values)):
                    for key in keywords:
                        values[i] = values[i].replace(key, '')
                values = [value for value in values if value != '']
                values = [value.replace('\n', '') for value in values]

                for key, value in zip(keywords, values):
                    if key not in data_dict.keys():
                        data_dict[key] = []
                    if 'Properties' in key:
                        prop_dict: dict = {}
                        val_split = value.split(':')
                        for i, k in enumerate(val_split[::3]):
                            prop_dict[k] = {}
                            prop_dict[k] = {
                                'val_typ': {
                                    'R': float,
                                    'S': str,
                                    'I': int
                                }[val_split[i * 3 + 1]],
                                'val_num': int(val_split[i * 3 + 2])
                            }
                    elif '"' in value:
                        value = value.replace('"', '')

                        if 'T' in value:
                            value = value.replace('T', '1').replace('F', '0')
                            data_dict[key] += [
                                np.fromstring(value, dtype=bool, sep=' ')
                            ]
                        else:
                            data_dict[key] += [
                                np.fromstring(value, dtype=float, sep=' ')
                            ]
                    else:
                        data_dict[key] += [[float(value)]]

                # Read atom info
                prop_data = {}
                for key in prop_dict.keys():
                    prop_data[key] = np.zeros(
                        (num_atoms * int(prop_dict[key]['val_num'])),
                        dtype=prop_dict[key]['val_typ'])
                for i in range(num_atoms):
                    line = f.readline()
                    line_split = line.split()
                    start = 0
                    for key in prop_dict.keys():
                        ival = int(prop_dict[key]['val_num'])
                        string = ' '.join(
                            line_split[start:start + ival])
                        prop_data[key][i * ival:(i + 1) *
                                       ival] = np.array(
                                           string.split(' '),
                                           dtype=prop_dict[key]['val_typ'])
                        start += ival
                data_dict.pop('Properties')
                # Add to data_dict:
                for key in prop_data.keys():
                    if key in data_dict.keys():
                        data_dict[key].append(prop_data[key])
                    else:
                        data_dict[key] = [prop_data[key]]

        for key in list(data_dict.keys()):
            if key in ['positions', 'forces', 'velocities']:
                data_dict[key.replace('s', '')] = data_dict.pop(key)
            elif key in ['species']:
                data_dict['atom'] = data_dict.pop(key)
            elif key in ['cell', 'Lattice']:
                data_dict['box'] = data_dict.pop(key)
            elif key in ['pos']:
                data_dict['position'] = data_dict.pop(key)
            elif key in ['TotEnergy']:
                data_dict['energy'] = data_dict.pop(key)

        for key in data_dict.keys():
            data_dict[key] = np.vstack(data_dict[key])

        if 'stress' in data_dict.keys():
            # TODO: Check if this is correct
            vols = np.array([
                np.linalg.det(box.reshape((3, 3))) for box in data_dict['box']
            ])
            data_dict['virial'] = -data_dict.pop('stress') * vols[:, None]

        return DataSet(data_dict)

    # @classmethod
    # def get_data_ds(cls, data: Union[str, Path, dict, DataSet],
    #              fill_zeros: Optional[bool] = False) -> DataSet:
    #     """Get DeepMD input data."""
    #     if isinstance(data, (str, Path)):
    #         if not is_file(data):
    #             raise FileNotFoundError('could not find DeepMD data ' +
    #                                     f'folder {data}')

    #         data_dir = Path(data)
    #         if not data_dir.is_dir():
    #             raise FileNotFoundError('could not find DeepMD data ' +
    #                                     f'folder {data_dir}')

    #         # ' find setname'
    #         files = [f.name for f in data_dir.iterdir()]
    #         for f in ['type.raw', 'type_map.raw']:
    #             if f not in files:
    #                 raise FileNotFoundError(f'could not find file {f} in ' +
    #                                         f'folder {data_dir}')

    #         set_dir_str: str = ''
    #         for f in files:
    #             if f.startswith('set.'):
    #                 set_dir_str = f
    #                 break
    #         if set_dir_str == '':
    #             raise FileNotFoundError('could not find file <set.XYZ> in ' +
    #                                     f'folder {data_dir}')
    #         set_dir = data_dir / set_dir_str
    #         if not set_dir.is_dir():
    #             raise FileNotFoundError('could not find DeepMD data ' +
    #                                     f'folder {set_dir}')
    #         files = [f.name for f in set_dir.iterdir()]
    #         for f in ['coord.npy', 'box.npy']:
    #             if f not in files:
    #                 raise FileNotFoundError(f'could not find file {f} in ' +
    #                                         f'folder {set_dir}')
    #         coord = np.load(set_dir / 'coord.npy')
    #         box = np.load(set_dir / 'box.npy')

    #         f = str(set_dir/'energy.npy')
    #         if is_file(f):
    #             energy = np.load(f)
    #         else:
    #             if fill_zeros:
    #                 energy = np.zeros(coord.shape[0])

    #             else:
    #                 raise FileNotFoundError(f'could not find file {f} in ' +
    #                                         f'folder {set_dir}')

    #         f = str(set_dir/'force.npy')
    #         if is_file(f):
    #             force = np.load(f)
    #         else:
    #             if fill_zeros:
    #                 force = np.zeros_like(coord)

    #             else:
    #                 raise FileNotFoundError(f'could not find file {f} in ' +
    #                                         f'folder {set_dir}')

    #         ds = DataSet({'coord': coord, 'box': box,
    #                       'force': force}, dimension='multi')
    #         ds1 = DataSet({'energy': energy})

    #         return ds + ds1

    #     elif isinstance(data, DataSet):
    #         for k in ['coord', 'box']:
    #             if k not in list(data.data.columns):
    #                 raise KeyError(f'could not find colum {k} in dataframe')

    #         return data

    #     elif isinstance(data, pd.DataFrame):
    #         for k in ['coord', 'box']:
    #             if k not in list(data.columns):
    #                 raise KeyError(f'could not find colum {k} in dataframe')
    #         return DataSet(data)

    #     elif isinstance(data, pd.Series):
    #         df = data.to_frame().T
    #         for k in ['coord', 'box']:
    #             if k not in list(df.columns):
    #                 raise KeyError(f'could not find colum {k} in dataframe')
    #         return DataSet(df)

    #     elif isinstance(data, dict):
    #         for k in ['coord', 'box']:
    #             if k not in data.keys():
    #                 raise KeyError(f'could not find colum {k} in dataframe')
    #         return DataSet(data)
    #     else:
    #         raise TypeError('allowed input types: str/path, ' +
    #                         'dict and hylleraas.DataSet')
