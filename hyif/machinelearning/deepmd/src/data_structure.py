import copy
from dataclasses import dataclass
from pathlib import Path
from typing import List, Optional, Tuple, Union

import numpy as np
from hyobj import DataSet, Units
from qcelemental import periodictable as pt

# from dataclasses import replace
from .units import DPUnits


@dataclass
class DeepMDData:
    """DeepMD data class."""

    format: Optional[str] = None
    coordinates: Optional[np.ndarray] = None
    boxes: Optional[np.ndarray] = None
    energies: Optional[np.ndarray] = None
    forces: Optional[np.ndarray] = None
    virials: Optional[np.ndarray] = None
    atoms: Optional[list] = None
    natoms: Optional[int] = None
    type_raw: Optional[List[int]] = None
    type_map_raw: Optional[List[str]] = None
    real_atom_types: Optional[np.ndarray] = None
    output_file: Optional[Union[str, Path]] = None
    model: Optional[Union[str, Path]] = None
    model_compressed: Optional[Union[str, Path]] = None
    data_dir: Optional[Union[str, Path]] = None
    dump: Optional[bool] = None
    load: Optional[bool] = None
    nopbc: Optional[bool] = None
    units: Optional[Units] = None
    frames: Optional[list] = None
    nframes: Optional[int] = None

    def __post_init__(self):
        """Post init."""
        if self.format is None:
            self.format = 'standard'

        if self.format not in ['standard', 'mixed_type']:
            raise ValueError('format must be standard or mixed_type')

        if self.load:
            self.load_data(self.format)

        # move....
        self.nframes = self._set_frames()

        if self.atoms:
            self.type_raw, self.type_map_raw = self._atoms_to_types(self.atoms)
            self.natoms = len(self.atoms)

        if self.format == 'mixed_type':
            self.real_atom_types, self.type_raw = self._set_real_atom_types()
            self.atoms = None
        else:
            if self.real_atom_types is not None:
                self.type_raw, self.type_map_raw = self._atoms_to_types(
                    self.real_atom_types[0]
                )

        # self._fix_units()
        if self.units is not None:
            self._fix_input_units(self.units)
            self.units = DPUnits

        if self.dump:
            self.dump_data()

        self._fix_dimensions()

    def _fix_dimensions(self):
        """Fix array shapes."""
        n_data = -1
        if self.coordinates is not None:
            self.coordinates = np.array(self.coordinates)
            n_data = self.coordinates.shape[0]
            self.coordinates = self.coordinates.reshape((n_data, -1))
        if n_data == -1:
            return
        if self.forces is not None:
            self.forces = np.array(self.forces).reshape((n_data, -1))
        if self.virials is not None:
            self.virials = np.array(self.virials).reshape((n_data, 9))
        if self.boxes is not None:
            self.boxes = np.array(self.boxes).reshape((n_data, 9))

    def _set_real_atom_types(self):
        """Set real atom types."""
        if self.real_atom_types is None:
            # nframes = len(self.energies)
            real_atom_types = np.array(
                [self.type_raw for _ in range(self.nframes)]
            )
        else:
            real_atom_types = self.real_atom_types
        type_raw = [0 for _ in self.type_raw]

        return real_atom_types, type_raw

    def _fix_input_units(self, input_units: Units):
        """Fix input units."""
        output_units = DPUnits

        fac_length = output_units.length[1] / input_units.length[1]
        fac_energy = output_units.energy[1] / input_units.energy[1]

        if self.coordinates is not None:
            self.coordinates *= fac_length
        if self.boxes is not None:
            self.boxes *= fac_length
        if self.energies is not None:
            self.energies *= fac_energy
        if self.forces is not None:
            self.forces *= fac_energy / fac_length
        if self.virials is not None:
            self.virials *= fac_energy

    def _set_frames(self):
        """Set frames to be used."""
        frames = set(self.frames) if self.frames is not None else None
        if self.frames is not None:
            frames = list(set(self.frames))

        if self.energies is None:
            return 0

        if frames is None:
            frames = list(range(len(self.energies)))

        if len(frames) == 0:
            raise ValueError('frames is empty')

        self.energies = np.take(self.energies, frames, 0)

        if self.forces is not None:
            self.forces = np.take(self.forces, frames, 0)
        if self.virials is not None:
            self.virials = np.take(self.virials, frames, 0)
        if self.coordinates is not None:
            self.coordinates = np.take(self.coordinates, frames, 0)
        if self.boxes is not None:
            self.boxes = np.take(self.boxes, frames, 0)
        if self.real_atom_types is not None:
            self.real_atom_types = np.take(self.real_atom_types, frames, 0)

        return len(frames)

    def load_data(self, format: Optional[str] = 'standard'):
        """Load DeepMD data."""
        # if data_dir.suppfix == npz...
        if self.coordinates is not None:
            return

        if self.data_dir is None:
            raise ValueError('data_dir is not set')
        if not isinstance(self.data_dir, Path):
            self.data_dir = Path(self.data_dir)
        if not self.data_dir.is_dir():
            raise FileNotFoundError(
                'could not find DeepMD data ' + f'folder {self.data_dir}'
            )

        # ' find setname'
        files = [f.name for f in self.data_dir.iterdir()]
        for f in ['type.raw', 'type_map.raw']:
            if f not in files:
                raise FileNotFoundError(
                    f'could not find file {f} in ' + f'folder {self.data_dir}'
                )

        set_dirs = []
        # set_dir_str: str = ''
        for f in files:
            if f.startswith('set.'):
                set_dirs.append(f)

        if len(set_dirs) == 0:
            raise FileNotFoundError(
                'could not find file <set.XYZ> in ' + f'folder {self.data_dir}'
            )

        with open(self.data_dir / 'type.raw', 'rt') as ff:
            self.type_raw: List[int] = [int(i) for i in ff.read().split()]
            self.natoms = len(self.type_raw)
            # if self.mixed_type:
            #     self.type_raw = [0 for _ in self.type_raw]
        with open(self.data_dir / 'type_map.raw', 'rt') as ff:
            self.type_map_raw = ff.read().split()

        if 'nopbc' in list(self.data_dir.iterdir()):
            self.nopbc = True

        self.energies = []
        self.forces = []
        self.virials = []
        self.coordinates = []
        self.boxes = []
        self.real_atom_types = []

        for set_dir_str in set_dirs:
            set_dir = self.data_dir / set_dir_str
            # print('reading data from ', set_dir)
            if not set_dir.is_dir():
                raise FileNotFoundError(
                    'could not find DeepMD data ' + f'folder {set_dir}'
                )
            files = [f.name for f in set_dir.iterdir()]
            for f in ['coord.npy', 'box.npy']:
                if f not in files:
                    raise FileNotFoundError(
                        f'could not find file {f} in ' + f'folder {set_dir}'
                    )
            coordinates = np.load(set_dir / 'coord.npy', allow_pickle=True)
            n_data = coordinates.shape[0]
            coordinates = coordinates.reshape((n_data, -1))

            if 'energy.npy' in files:
                energies = np.load(set_dir / 'energy.npy', allow_pickle=True)
                if energies.shape[0] != n_data:
                    raise ValueError(
                        'number of atoms in coord.npy and '
                        + 'energy.npy does not match'
                    )
            else:
                energies = np.zeros(n_data)

            if 'force.npy' in files:
                forces = np.load(
                    set_dir / 'force.npy', allow_pickle=True
                ).reshape((n_data, -1))
                if coordinates.shape[1] != forces.shape[1]:
                    raise ValueError(
                        'number of atoms in coord.npy and '
                        + 'force.npy does not match'
                    )
            else:
                forces = np.zeros(coordinates.shape)

            if 'box.npy' in files:
                boxes = np.load(
                    set_dir / 'box.npy', allow_pickle=True
                ).reshape((n_data, 9))
            else:
                boxes = np.zeros((n_data, 9))

            if 'virial.npy' in files:
                virials = np.load(
                    set_dir / 'virial.npy', allow_pickle=True
                ).reshape((n_data, 9))
            else:
                virials = np.zeros((n_data, 9))

            if format == 'mixed_type':
                # try:
                real_atom_types = np.load(
                    set_dir / 'real_atom_types.npy', allow_pickle=True
                )
                # except FileNotFoundError:
                #     raise FileNotFoundError('could not find file ' +
                #                             'real_atom_types.npy in ' +
                #                             f'folder {set_dir}')
                # else:
                self.real_atom_types.append(real_atom_types)

            self.energies.append(energies)
            self.forces.append(forces)
            self.virials.append(virials)
            self.coordinates.append(coordinates)
            self.boxes.append(boxes)

        if format == 'standard':
            self.atoms = [self.type_map_raw[int(i)] for i in self.type_raw]

        self.energies = np.concatenate(self.energies)
        self.forces = np.concatenate(self.forces)
        self.virials = np.concatenate(self.virials)
        self.coordinates = np.concatenate(self.coordinates)
        self.boxes = np.concatenate(self.boxes)

        if format == 'mixed_type' and len(self.real_atom_types) > 0:
            self.real_atom_types = np.concatenate(self.real_atom_types)
        else:
            self.real_atom_types = None

    def gen_real_atom_types(self):
        """Generate real atom types."""
        return np.array(
            [self.type_raw for _ in range(self.coordinates.shape[0])]
        )

    def pad(self, npad: int, real_atom_types, type_raw):
        """Pad data."""
        # if not self.mixed_type:
        #     raise ValueError('cannot pad data without mixed_type=True')
        if npad == 0:
            return
        if npad < 0:
            raise ValueError('npad must be positive')
        nold = len(type_raw)
        type_raw = [0 for _ in range(nold + npad)]

        if real_atom_types is None:
            raise ValueError('real_atom_types is None')
        real_atom_types = np.pad(
            real_atom_types,
            ((0, 0), (0, npad)),
            'constant',
            constant_values=-1,
        )
        return type_raw, real_atom_types

    def dump_data(self, **kwargs):
        """Dump DeepMD data."""
        path = kwargs.get('data_dir', self.data_dir)
        format = kwargs.get('format', 'standard')
        nopbc = kwargs.get('nopbc', self.nopbc)
        npad: int = kwargs.get('pad', None)
        overwrite: bool = kwargs.get('overwrite', False)
        units = kwargs.get('units', None)
        if units is not None:
            fac_e = units.energy[1] / DPUnits.energy[1]
            fac_l = units.length[1] / DPUnits.length[1]
        else:
            fac_e = 1.0
            fac_l = 1.0

        if format == 'mixed_type':
            if self.real_atom_types is None:
                real_atom_types, type_raw = self._set_real_atom_types()
            else:
                real_atom_types = self.real_atom_types
                type_raw = self.type_raw
        else:
            if self.type_raw is None and self.atoms is not None:
                type_raw, type_map_raw = self._atoms_to_types(self.atoms)
            else:
                type_raw = self.type_raw.copy()

        type_map_raw = self.type_map_raw.copy()
        # natoms = len(type_raw)

        if npad is not None:
            type_raw, real_atom_types = self.pad(
                npad, real_atom_types, type_raw
            )

        if not isinstance(path, Path):
            path = Path(path)
        if not path.is_dir():
            path.mkdir(parents=True)

        if nopbc:
            with open(path / 'nopbc', 'wt') as f:
                f.write('')

        # check if set.xyz exists, if not create set.(xyz+1) zb set.001 set.002
        set_dir_str: str = ''
        files = [f.name for f in path.iterdir()]
        num = 0

        for f in files:
            if f.startswith('set.'):
                set_dir_str = f
                num = max(num, int(f.split('.')[1]))
                set_dir_str = f'set.{num:03}'

            if f == 'type.raw':
                with open(path / f, 'rt') as fr:
                    type_raw_new = [int(i) for i in fr.read().split()]
                if len(type_raw_new) != len(type_raw):
                    raise ValueError(
                        'number of atoms in type.raw is '
                        + 'different from previous sets',
                        type_raw_new,
                        type_raw,
                    )
            if f == 'type_map.raw':
                with open(path / f, 'rt') as fr:
                    type_map_raw_new = fr.read().split()
                if len(type_map_raw_new) != len(type_map_raw):
                    raise ValueError(
                        'number of atoms in type_map.raw is '
                        + 'different from previous sets',
                        type_map_raw_new,
                        type_map_raw,
                    )

        if set_dir_str == '':
            set_dir_str = 'set.000'
        else:
            set_dir_str = set_dir_str.split('.')[1]
            if overwrite:
                set_dir_str = f'set.{int(set_dir_str):03}'
                # for f in files:
                #     if f.startswith('set.'):
                #         if f != set_dir_str:
                #             (path / f).unlink()
                try:
                    for f in Path(set_dir_str).iterdir():
                        f.unlink()
                except FileNotFoundError:
                    pass
            else:
                raise ValueError(
                    'set_dir_str is not empty, set overwrite=True to overwrite'
                )
                # set_dir_str = f'set.{int(set_dir_str)+1:03}'

        set_dir = path / set_dir_str
        if not set_dir.exists():
            set_dir.mkdir()

        if set_dir_str == 'set.000':
            with open(path / 'type.raw', 'wt') as f:
                f.write(' '.join([str(i) for i in type_raw]))
            with open(path / 'type_map.raw', 'wt') as f:
                f.write(' '.join(self.type_map_raw))

        np.save(set_dir / 'coord.npy', self.coordinates * fac_l)

        # Add support for non-pbc
        if self.boxes is not None:
            np.save(set_dir / 'box.npy', self.boxes * fac_l)
        else:
            np.save(set_dir / 'box.npy',
                    np.array([100, 0, 0,
                              0, 100, 0, 0, 0, 100]*len(self.energies)))
        if self.energies is not None:
            np.save(set_dir / 'energy.npy', self.energies * fac_e)
        # Check that forces is a member of self
        if self.forces is not None:
            np.save(set_dir / 'force.npy', self.forces * fac_e / fac_l)
        if self.virials is not None:
            np.save(set_dir / 'virial.npy', self.virials * fac_e)
        if format == 'mixed_type':
            np.save(set_dir / 'real_atom_types.npy', real_atom_types)

    def _atoms_to_types(self, atoms) -> Tuple[List[int], List[str]]:
        """Convert atoms to types."""
        types = copy.deepcopy(atoms)
        types_map: List[str] = []
        # iatom = -1
        if isinstance(atoms[0], (list, np.ndarray)):
            return self._atoms_to_types(atoms[0])
        for i, atom in enumerate(atoms):
            if atom not in pt.E:
                raise ValueError(f'atom {atom} not in periodic table')
            if atom not in types_map:
                types_map.append(atom)
                # iatom += 1
            types[i] = types_map.index(atom)
            # for j in range(i + 1, len(atoms)):
            #     types[j] = int(iatom) if types[j] == atom else types[j]
        return types, types_map

    def to_dataset(self, keys: Optional[List[str]] = None):
        """Convert DeepMD data to DataSet."""
        if self.real_atom_types is not None:
            self.atom_types = self.real_atom_types
        else:
            if self.atoms is None:
                if any([i is None for i in [self.type_raw,
                                            self.type_map_raw]]):
                    raise ValueError(
                        'type_raw and type_map_raw must be set'
                        + ' to convert to DataSet'
                        + f' {self.type_raw} {self.type_map_raw}'
                    )
                self.atom_types = [
                    [self.type_map_raw[int(i)] for i in self.type_raw]
                    for _ in range(self.coordinates.shape[0])
                ]
                self.atoms = [
                    [self.type_map_raw[int(i)] for i in self.type_raw]
                    for _ in range(self.coordinates.shape[0])
                ]
        if keys is None:
            keys = [
                'energies',
                'forces',
                'virials',
                'coordinates',
                'boxes',
                'atoms',
            ]
        d = {
            k: np.array(getattr(self, k))
            for k in keys
            if getattr(self, k) is not None
        }
        return DataSet(d)

    def to_nequip(self) -> str:
        """Convert DeepMD data to NequIP format."""
        nequip_str = ''
        dump_list = [
            self.energies,
            self.coordinates,
            self.boxes,
            self.forces,
        ]
        if self.virials is not None and self.virials.all() != 0.0:
            dump_list.append(self.virials)
        else:
            dump_list.append([None] * len(self.energies))

        for e, c, b, f, v in zip(*dump_list):
            nequip_str += f'{len(self.type_raw)}\n'
            nequip_str += f'Lattice="{b[0]} {b[1]} {b[2]} {b[3]} {b[4]}'
            nequip_str += f' {b[5]} {b[6]} {b[7]} {b[8]}"'
            if v is not None:
                nequip_str += f' virial={v[0]} {v[1]} {v[2]} {v[3]} {v[4]}'
                nequip_str += f' {v[5]} {v[6]} {v[7]} {v[8]}"'
            if self.nopbc:
                nequip_str += ' pbc="F F F"'
            else:
                nequip_str += ' pbc="T T T"'
            nequip_str += ' Properties=species:S:1:pos:R:3:forces:R:3'
            nequip_str += f' energy={e}\n'
            c = c.reshape(-1, 3)
            f = f.reshape(-1, 3)
            for a, c, f in zip(self.type_raw, c, f):
                a_type = self.type_map_raw[int(a)]
                nequip_str += (
                    f'{a_type} {c[0]} {c[1]} {c[2]} {f[0]} {f[1]} {f[2]}\n'
                )

        return nequip_str
