from copy import deepcopy
from typing import List

import hyset
import MDAnalysis
import numpy as np
import pytest
from hyobj import PeriodicSystem

from hyif import Gromacs


def test_create_gro_file():
    """Test creation of gro file string."""
    my_gromacs = Gromacs({'check_version': False})
    system = PeriodicSystem(
        {
            'atoms': ['OW', 'HW1', 'HW2'],
            'coordinates': [0, 0, 0, 1, 0, 0, 0, 0, 1],
            'pbc': [10, 0, 0, 0, 10, 0, 0, 0, 10],
        },
        units='metal',
    )
    system_no_properties = deepcopy(system)
    system.properties.update(
        {'residue_name': ['SOL', 'SOL', 'SOL'], 'residue_idx': [1, 1, 1]}
    )
    gro_str = my_gromacs.create_gro_file(system)

    with open('tmp.gro', 'w') as fp:
        fp.write(gro_str)

    u = MDAnalysis.Universe('tmp.gro')
    assert len(u.atoms) == 3
    assert np.allclose(u.dimensions[:3], [10, 10, 10])
    assert np.allclose(u.dimensions[3:], [90, 90, 90])

    with pytest.raises(ValueError):
        my_gromacs.create_gro_file(system_no_properties)


def test_read_topology():
    """Test reading of topology file."""
    my_gromacs = Gromacs()
    system = PeriodicSystem(
        {
            'atoms': ['OW', 'HW1', 'HW2'],
            'coordinates': [0, 0, 0, 1, 0, 0, 0, 0, 1],
            'pbc': [10, 0, 0, 0, 10, 0, 0, 0, 10],
        }
    )
    system.properties.update(
        {'residue_name': ['wat', 'wat', 'wat'], 'residue_idx': [1, 1, 1]}
    )

    # Check that no topology causes error
    with pytest.raises(ValueError):
        my_gromacs.setup(system)

    # Check reading the topology
    topology_str, itp_files1 = my_gromacs.read_topology('data/example.top')
    assert isinstance(topology_str, str)
    assert len(itp_files1) == 0

    # Check reading topology and itp files
    topology_str, itp_files2 = my_gromacs.read_topology(
        ['data/example.top', 'data/forcefield.itp']
    )
    assert isinstance(topology_str, str)
    assert len(itp_files2) == 1

    # Check reading opology from string
    new_topology_str, itp_files3 = my_gromacs.read_topology(topology_str)
    assert isinstance(new_topology_str, str)
    assert len(itp_files3) == 0


def test_read_template():
    """Test reading of template file."""
    my_gromacs = Gromacs()

    template, extra_template = my_gromacs.read_template('data/example.mdp')
    assert isinstance(template, str)
    assert isinstance(extra_template, List)


def test_gmx_run():
    """Test gmx_run."""
    comp_set = hyset.create_compute_settings(print_level='DEBUG')
    gmx_ana = Gromacs({'check_version': False}, compute_settings=comp_set)
    output_edr = 'data/gmx_run/cb271c4438efb47e8ca1473e8737e7eb73698aa1.edr'

    gmx_energy = gmx_ana.gmx_run(
        'energy',
        input_files=['-f', output_edr],
        output_files=['-o', 'tmp-energy.xvg'],
        commandline_options=' '.join([str(i) for i in range(1, 25)]),
        run_opt=dict(),
    )

    assert 'Temperature' in gmx_energy['output'].keys()
