import os
from datetime import timedelta

import hyset
import numpy as np
from hyobj import PeriodicSystem, Units

from hyif import Gromacs


def test_gromacs_locally():
    """Test gromacs locally."""
    my_gromacs = Gromacs(
        {'check_version': False}, hyset.create_compute_settings()
    )
    system = PeriodicSystem(
        {
            'atoms': ['OW', 'HW1', 'HW2'],
            'coordinates': [0, 0, 0, 1, 0, 0, 0, 0, 1],
            'pbc': [10, 0, 0, 0, 10, 0, 0, 0, 10],
        }
    )
    system.properties.update(
        {'residue_name': ['SOL', 'SOL', 'SOL'], 'residue_idx': [1, 1, 1]}
    )
    kwargs = {
        'template': 'data/example.mdp',
        'topology': [
            'data/example2.top',
            'data/forcefield.itp',
            'data/SOL.itp',
        ],
    }
    result = my_gromacs.run(system, run_opt=dict(sub_dir='tmp'), **kwargs)
    assert isinstance(result, dict)


def test_template_alteration():
    """Test template alteration."""
    my_gromacs = Gromacs(
        {'check_version': False}, hyset.create_compute_settings()
    )
    system = PeriodicSystem(
        {
            'atoms': ['OW', 'HW1', 'HW2'],
            'coordinates': [0, 0, 0, 1, 0, 0, 0, 0, 1],
            'pbc': [10, 0, 0, 0, 10, 0, 0, 0, 10],
        }
    )
    system.properties.update(
        {'residue_name': ['SOL', 'SOL', 'SOL'], 'residue_idx': [1, 1, 1]}
    )
    system_options = {'ref-t': '310'}
    kwargs = {
        'topology': [
            'data/example2.top',
            'data/forcefield.itp',
            'data/SOL.itp',
        ],
        'program_opt': system_options,
    }
    result = my_gromacs.run(system, run_opt=dict(sub_dir='tmp2'), **kwargs)
    assert isinstance(result, dict)


def test_result():
    """Test result parsing."""
    my_gromacs = Gromacs(
        {'check_version': False}, hyset.create_compute_settings()
    )
    system = PeriodicSystem(
        {
            'atoms': ['OW', 'HW1', 'HW2'],
            'coordinates': [0, 0, 0, 1, 0, 0, 0, 0, 1],
            'pbc': [10, 0, 0, 0, 10, 0, 0, 0, 10],
            'units': 'md',
        }
    )
    system.properties.update(
        {'residue_name': ['SOL', 'SOL', 'SOL'], 'residue_idx': [1, 1, 1]}
    )
    system_options = {
        'nstxout': '1000',
        'nstvout': '1000',
        'nstfout': '1000',
    }
    kwargs = {
        'topology': [
            'data/example2.top',
            'data/forcefield.itp',
            'data/SOL.itp',
        ],
        'program_opt': system_options,
    }
    result = my_gromacs.run(system, run_opt=dict(sub_dir='tmp3'), **kwargs)
    assert 'coordinates' in result
    assert 'box' in result
    assert 'atom_names' in result
    assert 'atom_types' in result
    assert 'forces' in result
    assert 'resnames' in result
    assert 'residues' in result


def test_reading_gro():
    """Test reading gro file."""
    my_gromacs = Gromacs(
        {'check_version': False}, hyset.create_compute_settings()
    )
    system = PeriodicSystem(my_gromacs.get_input_molecule('data/example2.gro'))
    np.testing.assert_allclose(
        system.coordinates,
        np.array(
            [
                [0.240, 3.840, 3.426],
                [0.245, 3.780, 3.352],
                [0.179, 3.908, 3.398],
            ]
        ),
    )
    np.testing.assert_allclose(
        system.pbc,
        np.array(
            [
                [5.57965, 0.0, 0.0],
                [0.0, 5.57965, 0.0],
                [0.0, 0.0, 5.57965],
            ]
        ),
    )
    assert np.array_equal(
        system.properties['residue_name'], np.array(['SOL', 'SOL', 'SOL'])
    )
    assert np.array_equal(
        system.properties['residue_idx'], np.array([1, 1, 1])
    )
    assert np.array_equal(system.units, Units('md'))
    assert np.array_equal(system.atoms, np.array(['OW', 'HW1', 'HW2']))


def test_units():
    """Test units."""
    my_gromacs = Gromacs(
        {'check_version': False}, hyset.create_compute_settings()
    )
    system = PeriodicSystem(my_gromacs.get_input_molecule('data/example.gro'))

    kwargs = {
        'template': 'data/example.mdp',
        'topology': [
            'data/example.top',
            'data/forcefield.itp',
            'data/SOL.itp',
        ],
    }
    result = my_gromacs.run(system, run_opt=dict(sub_dir='tmp4'), **kwargs)
    np.testing.assert_allclose(
        result['box'][0],
        np.array(
            [5.57965, 0.0, 0.0, 0.0, 5.57965, 0.0, 0.0, 0.0, 5.57965],
        ),
    )
    np.testing.assert_allclose(result['coordinates'][0][0], np.array([0.240]))
    assert result['atom_names'][0][0] == 'OW'
    assert result['atom_types'][0][0] == 'O'
    np.testing.assert_allclose(result['forces'][0][0], np.array([-100.210266]))


def test_gromacs_remote():
    """Test gromacs on betzy."""
    env_cp2k_slurm = hyset.create_compute_settings(
        'betzy',
        user='manuelln',
        slurm_account='nn4654k',
        job_time=timedelta(minutes=10),
        ntasks=1,
        cpus_per_task=1,
        max_connect_attempts=100,
        auto_connect=True,
        progress_bar=False,
        env=['module load GROMACS/2023.1-foss-2022a'],
        force_recompute=False,
        overwrite_files=True,
        slurm_extra=['--nodes=1', '--partition=preproc', '--mem-per-cpu=4G'],
        data_dir_local=os.getcwd(),
        work_dir_local=os.getcwd(),
        submit_dir_remote='/cluster/work/users/manuelln/test_gromacs',
        data_dir_remote='/cluster/work/users/manuelln/test_gromacs',
        work_dir_remote='/cluster/work/users/manuelln/test_gromacs',
    )
    my_gromacs = Gromacs({'check_version': False}, env_cp2k_slurm)
    system = PeriodicSystem(my_gromacs.get_input_molecule('data/example.gro'))

    kwargs = {
        'template': 'data/example.mdp',
        'topology': [
            'data/example.top',
            'data/forcefield.itp',
            'data/SOL.itp',
        ],
    }
    result = my_gromacs.run(system, run_opt=dict(sub_dir='tmp5'), **kwargs)
    assert isinstance(result, dict)


def test_trjconv():
    """Test trjconv."""
    gromacs_compute_settings = hyset.create_compute_settings(
        force_recompute=False,  # print_level="DEBUG"
    )
    gromacs_interface = Gromacs(
        {'check_version': False}, compute_settings=gromacs_compute_settings
    )
    system = PeriodicSystem(
        gromacs_interface.get_input_molecule('data/example.gro')
    )
    kwargs = {
        'template': 'data/example.mdp',
        'topology': [
            'data/example.top',
            'data/forcefield.itp',
            'data/SOL.itp',
        ],
        'index_file': 'data/index.ndx',
        'trjconv_trr': {'commandline_options': '3'},
        'trjconv_gro': {'commandline_options': '3'},
    }
    result = gromacs_interface.run(
        system, run_opt=dict(sub_dir='tmp'), **kwargs
    )
    assert isinstance(result, dict)
    assert 'files' in result
    assert 'constrained.trr' in result['files']
    assert 'constrained.gro' in result['files']
    assert len(result['resnames']) == 11
    assert len(result['coordinates']) == 11
    assert len(result['coordinates'][0]) == 3
