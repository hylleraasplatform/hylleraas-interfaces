from pathlib import Path
from typing import Union

try:
    import MDAnalysis as Mda
except ImportError:
    Mda = None
import numpy as np

from ...abc import Parser

# from qcelemental import PhysicalConstantsContext, periodictable
# constants = PhysicalConstantsContext('CODATA2018')


class OutputParser(Parser):
    """Gromacs OutputParser."""

    @classmethod
    def parse_stderr(cls, stderr: Union[str, Path]) -> str:
        """Parse Gromacs stderr."""
        return stderr.read_text() if isinstance(stderr, Path) else stderr

    @classmethod
    def parse_xtc(cls, xtc_file: Union[str, Path]) -> dict:
        """Parse Gromacs trajectory file."""
        boxlist = []
        coords = []
        atypes = []
        a_names = []
        forces = []
        resnames = []
        residues = []
        if_coords = True
        if Mda is None:
            raise ImportError('MDAnalysis is not installed.')

        results = {}

        xtc_file = Path(xtc_file)

        xtc_name = xtc_file.name
        xtc_path = xtc_file.parent
        hash_str = xtc_name.split('.')[0]
        gro_file = xtc_path / (hash_str + '.gro')
        trr_file = xtc_path / (hash_str + '.trr')
        trr_path = Path(trr_file)
        constrained_gro_file = xtc_path / (hash_str + '.constrained.gro')
        constrained_gro_path = Path(constrained_gro_file)

        try:
            if constrained_gro_path.exists():
                u = Mda.Universe(str(constrained_gro_file), str(trr_file))
                for _ in u.trajectory:
                    boxlist.append(
                        Mda.lib.mdamath.triclinic_vectors(u.dimensions).ravel()
                        * 0.1
                    )
                    try:
                        coords.append(u.atoms.positions.ravel() * 0.1)
                    except AttributeError:
                        if_coords = False
                    atypes.append(u.atoms.types)
                    a_names.append(u.atoms.names)
                    forces.append(u.atoms.forces.ravel())
                    resnames.append(u.atoms.resnames)
                    residues.append(u.atoms.resids)

                if not if_coords:
                    u = Mda.Universe(str(constrained_gro_file))
                    coords.append(u.atoms.positions.ravel() * 0.1)

                num_frames = len(boxlist)
                traj = {
                    'atom_names': a_names,
                    'atom_types': atypes,
                    'coordinates': np.array(coords),
                    'box': np.array(boxlist).reshape(num_frames, -1),
                    'forces': np.array(forces).reshape(num_frames, -1) * 10.0,
                    'resnames': resnames,
                    'residues': residues,
                }
                results.update(traj)
            else:
                if trr_path.exists():
                    u = Mda.Universe(str(gro_file), str(trr_file))
                    for _ in u.trajectory:
                        boxlist.append(
                            Mda.lib.mdamath.triclinic_vectors(
                                u.dimensions
                            ).ravel()
                            * 0.1
                        )
                        try:
                            coords.append(u.atoms.positions.ravel() * 0.1)
                        except AttributeError:
                            if_coords = False
                        atypes.append(u.atoms.types)
                        a_names.append(u.atoms.names)
                        forces.append(u.atoms.forces.ravel())
                        resnames.append(u.atoms.resnames)
                        residues.append(u.atoms.resids)

                    if not if_coords:
                        u = Mda.Universe(str(gro_file), str(xtc_file))
                        for _ in u.trajectory:
                            coords.append(u.atoms.positions.ravel() * 0.1)

                    num_frames = len(boxlist)
                    traj = {
                        'atom_names': a_names,
                        'atom_types': atypes,
                        'coordinates': np.array(coords).reshape(
                            num_frames, -1
                        ),
                        'box': np.array(boxlist).reshape(num_frames, -1),
                        'forces': np.array(forces).reshape(num_frames, -1)
                        * 10.0,
                        'resnames': resnames,
                        'residues': residues,
                    }
                    results.update(traj)
                else:
                    u = Mda.Universe(str(gro_file), str(xtc_file))
                    for _ in u.trajectory:
                        boxlist.append(
                            Mda.lib.mdamath.triclinic_vectors(
                                u.dimensions
                            ).ravel()
                            * 0.1
                        )
                        coords.append(u.atoms.positions.ravel() * 0.1)
                        atypes.append(u.atoms.types)
                        a_names.append(u.atoms.names)
                        residues.append(u.atoms.residues.resnames)
                    num_frames = len(boxlist)
                    traj = {
                        'atom_names': a_names,
                        'atom_types': atypes,
                        'coordinates': np.array(coords).reshape(
                            num_frames, -1
                        ),
                        'box': np.array(boxlist).reshape(num_frames, -1),
                        'residues': residues,
                    }
                    results.update(traj)

        except Exception as e:
            results['error'] = 'Could not parse Gromacs trajectory: ' + str(e)
            results['is_converged'] = False

        return results

    @classmethod
    def parse_stdout(cls, stdout: Union[str, Path]) -> dict:
        """Parse Gromacs stdout."""
        output = {}

        content = stdout.read_text() if isinstance(stdout, Path) else stdout

        for line in content.split('\n'):
            if 'GROMACS' in line:
                output['version'] = line
            if 'OpenMP threads' in line:
                output['OpenMP_threads'] = line.split()[1]
            if 'Total wall time' in line:
                output['Time [s]:'] = line.split()[2]
            if 'Error' in line:
                output['error'] = line

        return output
