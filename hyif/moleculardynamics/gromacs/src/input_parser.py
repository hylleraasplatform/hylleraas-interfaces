from hyobj import Units

from ...molecule import Molecule


class InputParser:
    """Gromacs input class."""

    @classmethod
    def read_molecule_from_file(cls, filename: str) -> Molecule:
        """Read molecule from file.

        Parameters
        ----------
        filename: :obj:`pathlib.Path`
            path to input file

        Returns
        -------
        Molecule
            Hylleraas-compatible Molecule object

        """
        try:
            from ase.io.gromacs import read_gromacs
        except ImportError:
            raise ImportError('could not find package ase')
        atoms = read_gromacs(filename)

        try:
            gromacs_atomtypes = atoms.get_array('atomtypes')
        except KeyError:
            gromacs_atomtypes = atoms.get_chemical_symbols()

        coordinates = atoms.get_positions() / 10.0

        # Check if a unit cell is defined
        if atoms.get_pbc().any():
            cell = atoms.get_cell()[:]
            return Molecule(
                atoms=gromacs_atomtypes,
                coordinates=coordinates,
                properties={
                    'residue_name': atoms.get_array('residuenames'),
                    'residue_idx': atoms.get_array('residuenumbers'),
                    'unit_cell': cell/10.0,
                },
                units=Units('md'),
            )

        else:
            return Molecule(
                atoms=gromacs_atomtypes,
                coordinates=coordinates,
                properties={
                    'residue_name': atoms.get_array('residuenames'),
                    'residue_idx': atoms.get_array('residuenumbers'),
                },
                units=Units('md'),
            )
