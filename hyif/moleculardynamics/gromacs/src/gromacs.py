import copy
from copy import deepcopy
from dataclasses import replace
from io import StringIO
from pathlib import Path
from typing import Any, Callable, Dict, List, Optional, Tuple, Union

import numpy as np
import pandas as pd
from hyobj import Constants, MoleculeLike, PeriodicSystem, Units
from hyset import (ComputeResult, ComputeSettings, File, RemoteRunSettings,
                   RunSettings, create_compute_settings, run_sequentially)
from hytools.logger import get_logger

from ....manager import NewRun
# from ....base import HylleraasInterfaceBase
# from ....manager import acompute, compute
from ....utils import unique_filename
from ...abc import HylleraasMDInterface, Parser, Runner
from ...molecule import Molecule
from .input_parser import InputParser
from .output_parser import OutputParser

# from qcelemental import periodictable as pt

DEFAULT_INPUT = """integrator               = {{integrator}}
dt                       = {{timestep}}
nsteps                   = {{nsteps}}
nstlog                   = {{nstlog}}
nstenergy                = {{nstenergy}}
nstxout-compressed       = {{nstxout-compressed}}
nstxout                  = {{nstxout}}
nstvout                  = {{nstvout}}
nstfout                  = {{nstfout}}
compressed-x-grps        = {{compressed-x-grps}}
constraints              = {{constraints}}
constraint-algorithm     = {{constraint-algorithm}}
cutoff-scheme            = {{cutoff-scheme}}
rlist                    = {{rlist}}
nstlist                  = {{nstlist}}
coulombtype              = {{coulombtype}}
rcoulomb                 = {{rcoulomb}}
vdwtype                  = {{vdwtype}}
rvdw                     = {{rvdw}}
tcoupl                   = {{tcoupl}}
tc_grps                  = {{tc_grps}}
tau-t                    = {{tau-t}}
ref-t                    = {{ref-t}}
pcoupl                   = {{pcoupl}}
tau-p                    = {{tau-p}}
compressibility          = {{compressibility}}
ref-p                    = {{ref-p}}
pcoupltype               = {{pcoupltype}}
gen_vel                  = {{gen_vel}}
gen_temp                 = {{gen_temp}}
"""


class Gromacs(HylleraasMDInterface, NewRun):
    """Gromacs interface."""

    def __init__(
        self,
        method: dict = {},
        compute_settings: Optional[ComputeSettings] = None,
        log_file: Optional[str] = 'gromacs.log',
        logging_level: Optional[str] = 'INFO',
        **kwargs,
    ):
        # initialize logger
        config_file = kwargs.pop(
            'config_file', Path(__file__).parent / 'logger_config.yml'
        )
        config_file = config_file if Path(str(config_file)).exists() else None
        self.logger = get_logger(
            logger_name='gromacs',
            log_file=log_file,
            logging_level=logging_level,
            config_file=config_file,
        )
        self.logger.debug('Initializing Gromacs Interface')
        if compute_settings is None:
            self.compute_settings = create_compute_settings()
        else:
            self.compute_settings = compute_settings

        self.Runner: Runner = self.compute_settings.Runner
        self.program = kwargs.pop('program', 'gmx')
        self.run_settings = self.compute_settings.run_settings

        self.OutputParser: Parser = OutputParser  # type: ignore
        self.output_file_suffix = '.xtc'

        self.method = method
        self.template, extra_template_files = self.read_template(
            method.get('template', DEFAULT_INPUT)
        )
        self.extra_template_files = extra_template_files

        #     self.version: str = 0
        #      (
        #           self.check_version() if method.pop('check_version', True)
        #                                 else None
        #        )

        self.arch_type = getattr(self.compute_settings, 'arch_type', 'remote')
        self.units = Units('md')

        self.additional_args_grompp = method.get('additional_args_grompp', [])
        self.additional_args_mdrun = method.get('additional_args_mdrun', [])

        if isinstance(self.units, str):
            self.units = Units(self.units)

    @run_sequentially
    def setup(
        self, molecule: Union[MoleculeLike, str, Path], **kwargs
    ) -> RunSettings:
        """Set up the calculation.

        Parameters
        ----------
        molecule : MoleculeLike
            Molecule to be calculated.
        **kwargs
            Additional keyword arguments.

        Returns
        -------
        RunSettings
            RunSettings object.

        """
        run_settings = deepcopy(self.run_settings)
        if 'run_opt' in kwargs.keys():
            run_settings = replace(run_settings, **kwargs.pop('run_opt'))

        program_opt = kwargs.pop('program_opt', {})
        if program_opt is None:
            self.logger.warning(
                'program_opt is None, define it as an empty dict.'
            )
            program_opt = {}

        trjconv_trr = kwargs.pop('trjconv_trr', None)
        trjconv_gro = kwargs.pop('trjconv_gro', None)
        trjconv_xtc = kwargs.pop('trjconv_xtc', None)

        if isinstance(molecule, PeriodicSystem):
            mol_str = self.create_gro_file(molecule)
            molecule_obj = copy.deepcopy(molecule)
        elif isinstance(molecule, (str, Path)):
            if '\n' in str(molecule):
                mol_str = str(molecule)
                # make a tmp file
                with open('tmp.gro', 'w') as f:
                    f.write(mol_str)
                molecule_obj = PeriodicSystem('tmp.gro')
                # remove the tmp file
                Path('tmp.gro').unlink()

            else:
                if isinstance(molecule, str):
                    molecule = Path(molecule)
                    molecule_file = str(molecule.resolve())
                    with open(molecule_file, 'r') as f:
                        mol_str = str(f.read())
                elif isinstance(molecule, Path):
                    with open(molecule, 'r') as f:
                        mol_str = str(f.read())
                molecule_obj = PeriodicSystem(str(molecule))
        else:
            raise TypeError('Cannot use Molecule based system.')

        if 'topology' not in kwargs.keys():
            raise ValueError('Topology not provided.')
        else:
            topology_str, extra_topology_files = self.read_topology(
                kwargs['topology']
            )
            if '{{molecules}}' in topology_str:
                resids_by_name = dict()  # type: ignore
                for resid, resname in zip(
                    molecule_obj.properties['residue_idx'],  # type: ignore
                    molecule_obj.properties['residue_name'],  # type: ignore
                ):
                    if resname not in resids_by_name:
                        resids_by_name[resname] = []
                    resids_by_name[resname].append(resid)  # type: ignore
                molecules_str = ''
                for resname, resids in resids_by_name.items():
                    molecules_str += f'{resname}     {len(set(resids))}\n'
                topology_str = topology_str.replace(
                    '{{molecules}}', molecules_str
                )

        if 'index_file' in kwargs.keys():
            index_file = self.read_index(kwargs['index_file'])
        else:
            index_file = None

        if 'template' not in kwargs.keys():
            template = self.template
            # extra_template_files = self.extra_template_files
        else:
            template, _ = self.read_template(kwargs['template'])

        # if run_settings.program is not None:
        #     warnings.warn('program is set in run_settings, ignoring plugin')
        #     program = run_settings.program
        # else:

        if 'program' in kwargs.keys():
            program = kwargs['program']
        else:
            program = self.program

        setup_dict_tpr: Dict[str, Any] = {'program': program}
        setup_dict_mdrun: Dict[str, Any] = {'program': program}
        if trjconv_trr is not None:
            setup_dict_trjconv_trr: Dict[str, Any] = {'program': program}
        if trjconv_gro is not None:
            setup_dict_trjconv_gro: Dict[str, Any] = {'program': program}
        if trjconv_xtc is not None:
            setup_dict_trjconv_xtc: Dict[str, Any] = {'program': program}

        input_str = template

        # Use program_opt to
        options = {
            'integrator': 'md',
            'timestep': 0.002,
            'nsteps': 5000,
            'nstlog': 1000,
            'nstenergy': 1000,
            'nstxout-compressed': 1000,
            'nstxout': 0,
            'nstvout': 0,
            'nstfout': 0,
            'compressed-x-grps': 'System',
            'constraints': 'h-bonds',
            'constraint-algorithm': 'LINCS',
            'cutoff-scheme': 'Verlet',
            'rlist': 1.2,
            'nstlist': 10,
            'coulombtype': 'pme',
            'rcoulomb': 1.2,
            'vdwtype': 'Cut-off',
            'rvdw': 1.2,
            'tcoupl': 'v-rescale',
            'tc_grps': 'System',
            'tau-t': 0.4,
            'ref-t': 300,
            'pcoupl': 'C-rescale',
            'tau-p': 5.0,
            'compressibility': 4.5e-5,
            'ref-p': 1.0,
            'pcoupltype': 'isotropic',
            'gen_vel': 'yes',
            'gen_temp': 300,
        }
        options.update(program_opt)
        for key, value in options.items():
            # Check whether key is defined in input_str
            str_to_be_replaced = '{{' + key + '}}'
            if str_to_be_replaced not in input_str:
                if key in program_opt.keys():
                    lines = input_str.split('\n')
                    lines.append(f'{key} = {program_opt[key]}')
                    input_str = '\n'.join(lines)
                    # raise KeyError(
                    #    f'{key} = {program_opt[key]} cannot be'
                    #    + 'formatted to string.'
                    # )
            else:
                input_str = input_str.replace(str_to_be_replaced, str(value))
        hash_str = program_opt.pop(
            'hash_str',
            unique_filename(
                mol_str.split('\n')
                + input_str.split('\n')
                + topology_str.split('\n')
                + ([index_file] if index_file else [])
                + [file.content for file in extra_topology_files]
            ),
        )
        input_filename = hash_str + '.pre.mdp'
        mol_filename = hash_str + '.gro'
        topology_filename = hash_str + '.top'
        tpr_filename = hash_str + '.tpr'
        # output_filename = hash_str + '.xtc'
        grompp_filename = hash_str + '.grompp_stdout'
        mdrun_filename = hash_str + '.mdrun_stdout'
        mdout_filename = hash_str + '.mdp'
        index_filename = hash_str + '.ndx'
        outgro_filename = hash_str + '.out.gro'

        if trjconv_trr is not None:
            trjconv_1_filename = hash_str + '.trr'
        if trjconv_gro is not None:
            trjconv_2_filename = hash_str + '.constrained.gro'
        if trjconv_xtc is not None:
            trjconv_3_filename = hash_str + '.xtc'

        # Check that are is nothing more to replace
        if '{{' in input_str:
            list_lines = []
            for line in input_str.split('\n'):
                if '{{' in line:
                    list_lines.append(line)
            raise ValueError(
                'Input string is not properly formatted, '
                + 'missing values for: '
                + ','.join(list_lines)
            )

        files_to_write = [
            File(name=mol_filename, content=mol_str),
            File(name=topology_filename, content=topology_str),
        ]
        files_to_write.append(File(name=input_filename, content=input_str))
        files_to_write += extra_topology_files
        if index_file is not None:
            files_to_write.append(
                File(name=index_filename, content=index_file)
            )

        if isinstance(run_settings, RemoteRunSettings):
            launcher = ''
        else:
            launcher = []  # type: ignore
        additional_args_grompp = kwargs.pop(
            'additional_args_grompp', self.additional_args_grompp
        )
        additional_args_grompp = ' '.join(additional_args_grompp)

        args = [
            'grompp',
            '-f',
            str(input_filename),
            '-c',
            str(mol_filename),
            '-p',
            str(topology_filename),
            '-o',
            tpr_filename,
            '-po',
            mdout_filename,
            additional_args_grompp,
        ]

        if index_file is not None:
            args.extend(['-n', str(index_filename)])

        setup_dict_tpr.update(
            {
                'launcher': launcher,
                'files_to_write': files_to_write,
                'output_file': File(name=tpr_filename),
                'stdout_file': File(name=grompp_filename),
                'stderr_file': File(name=grompp_filename),
                'args': args,
            }
        )

        additional_args_mdrun = kwargs.pop(
            'additional_args_mdrun', self.additional_args_mdrun
        )
        additional_args_mdrun = ' '.join(additional_args_mdrun)
        args = ['mdrun', '-deffnm', hash_str, '-c', outgro_filename]
        if additional_args_mdrun:
            args.append(additional_args_mdrun)

        setup_dict_mdrun.update(
            {
                'output_file': File(name=outgro_filename),
                'stdout_file': File(name=mdrun_filename),
                'stderr_file': File(name=mdrun_filename),
                'args': args,
            }
        )
        if trjconv_trr is not None:
            args = [
                'trjconv',
                '-s',
                str(tpr_filename),
                '-f',
                str(hash_str + '.trr'),
                '-o',
                str(trjconv_1_filename),
                '-force',
            ]
            if index_file is not None:
                args.extend(['-n', str(index_filename)])
            setup_dict_trjconv_trr.update(
                {
                    'launcher': launcher,
                    'output_file': File(name=trjconv_1_filename),
                    'stdout_file': File(name=hash_str + '.trjconv1_stdout'),
                    'stderr_file': File(name=hash_str + '.trjconv1_stderr'),
                    'args': args,
                }
            )
            if 'commandline_options' in trjconv_trr.keys():
                setup_dict_trjconv_trr.update(
                    {'pre_cmd': f'echo {trjconv_trr["commandline_options"]} |'}
                )

        if trjconv_gro is not None:
            args = [
                'trjconv',
                '-s',
                str(tpr_filename),
                '-f',
                str(hash_str + '.gro'),
                '-o',
                str(trjconv_2_filename),
            ]
            if index_file is not None:
                args.extend(['-n', str(index_filename)])
            setup_dict_trjconv_gro.update(
                {
                    'launcher': launcher,
                    'output_file': File(name=trjconv_2_filename),
                    'stdout_file': File(name=hash_str + '.trjconv2_stdout'),
                    'stderr_file': File(name=hash_str + '.trjconv2_stderr'),
                    'args': args,
                }
            )
            if 'commandline_options' in trjconv_gro.keys():
                setup_dict_trjconv_gro.update(
                    {'pre_cmd': f'echo {trjconv_gro["commandline_options"]} |'}
                )

        if trjconv_xtc is not None:
            args = [
                'trjconv',
                '-s',
                str(tpr_filename),
                '-f',
                str(hash_str + '.xtc'),
                '-o',
                str(trjconv_3_filename),
            ]
            if index_file is not None:
                args.extend(['-n', str(index_filename)])
            setup_dict_trjconv_xtc.update(
                {
                    'launcher': launcher,
                    'output_file': File(name=trjconv_3_filename),
                    'stdout_file': File(name=hash_str + '.trjconv3_stdout'),
                    'stderr_file': File(name=hash_str + '.trjconv3_stderr'),
                    'args': args,
                }
            )
            if 'commandline_options' in trjconv_xtc.keys():
                setup_dict_trjconv_xtc.update(
                    {'pre_cmd': f'echo {trjconv_xtc["commandline_options"]} |'}
                )

        output = []
        output.append(replace(deepcopy(run_settings), **setup_dict_tpr))
        output.append(replace(deepcopy(run_settings), **setup_dict_mdrun))
        if trjconv_trr is not None:
            output.append(
                replace(deepcopy(run_settings), **setup_dict_trjconv_trr),
            )
        if trjconv_gro is not None:
            output.append(
                replace(deepcopy(run_settings), **setup_dict_trjconv_gro),
            )
        if trjconv_xtc is not None:
            output.append(
                replace(deepcopy(run_settings), **setup_dict_trjconv_xtc),
            )

        return output

    def create_gro_file(self, system: PeriodicSystem) -> str:
        """Turn a periodic system into a gro file string."""
        if not (
            'residue_idx' in system.properties.keys()
            and 'residue_name' in system.properties.keys()
        ):
            raise ValueError(
                'Gromacs interface requires specification of residue_idx and '
                'residue_name for every atom in the periodic system object.'
            )

        atoms = system.atoms

        mol_units = system.units.length[0]
        if mol_units is None:
            raise ValueError('unit not set in molecule')

        if mol_units == 'bohr':
            fac = self.units.length[1]
        elif mol_units.lower() in 'angstroms':
            fac = self.units.length[1] / Constants.bohr2angstroms
        elif mol_units.lower() in 'nm':
            fac = 1.0
        else:
            raise ValueError(f'unknown units {mol_units} in molecule {system}')
        coordinates = np.reshape(system.coordinates, (-1, 3)) * fac
        cell = np.array(system.pbc.flatten()) * fac

        res_ids = system.properties['residue_idx']
        res_names = system.properties['residue_name']

        gro_cell = cell[[0, 4, 8, 1, 2, 3, 5, 6, 7]]
        system_str = 'A Gromacs structure file written from molecule by HSP \n'
        system_str += f'{len(atoms)} \n'

        for i, (atom_name, coordinate, res_id, res_name) in enumerate(
            zip(atoms, coordinates, res_ids, res_names)
        ):
            system_str += (
                '{:>5d}{:<5s}{:>5s}{:>5d}{:>8.3f}{:>8.3f}{:>8.3f}\n'.format(
                    res_id,
                    res_name,
                    atom_name,
                    i + 1,
                    coordinate[0],
                    coordinate[1],
                    coordinate[2],
                )
            )
        system_str += ''.join(['{:10.5f}'.format(x) for x in gro_cell])
        system_str += '\n'

        return system_str

    def get_input_molecule(
        self, input_file: Union[str, Path], parser: Parser = None
    ) -> Molecule:
        """Get molecule from input file.

        Parameters
        ----------
        input_file: :obj:`pathlib.Path`
            path to input file
        parser: :obj:`Parser`, optional
            parser for reading inputfile (must provide method 'parse')

        Returns
        -------
        :obj:`Molecule`
            Hylleraas compatible Molecule object

        """
        parse: Callable
        if parser is None:
            parse = InputParser.read_molecule_from_file  # type: ignore
        else:
            parse = parser.parse

        return parse(Path(input_file))  # type: ignore

    @run_sequentially
    def parse(self, output: ComputeResult):
        """Parse the output."""
        if str(output.output_file).endswith('.tpr'):
            return  # Quit the parse function if output_file ends with '.tpr'

        output_dict = {}

        if isinstance(output.output_file, File):
            prefix = str(output.output_file.name).split('.')[0]
            output_path = Path(output.output_file)
        elif isinstance(output.output_file, str):
            prefix = str(Path(output.output_file).name).split('.')[0]
            output_path = Path(output.output_file)
        elif isinstance(output.output_file, Path):
            prefix = str(output.output_file.name).split('.')[0]
            output_path = Path(output.output_file)
        else:
            raise ValueError('output_file is not a valid type.')

        # Look for files with same hash -xtc
        files = {}
        for fn in output_path.parent.glob(f'{prefix}*'):
            key = fn.name.replace(prefix, '')
            if key[0] == '.' or key[0] == '_':
                key = key[1:]
            files[key] = str(fn)
            if key == 'xtc':
                output_dict.update(
                    self.OutputParser.parse_xtc(fn)  # type: ignore
                )
        output_dict.update({'files': files})

        if output.stdout is not None:
            output_dict.update(
                self.OutputParser.parse_stdout(output.stdout)  # type: ignore
            )  # type: ignore

        if output.stderr is not None:
            # warnings.warn(output.stderr)
            err_str = self.OutputParser.parse_stderr(  # type: ignore
                output.stderr
            )
            self.logger.error(err_str)
            output_dict['stderr'] = err_str

        # if output.add_to_results is not None:
        #     output_dict.update(output.add_to_results)

        return output_dict

    def read_topology(
        self, topology: Union[str, Path, List]
    ) -> Tuple[str, List[File]]:
        """Read topology file."""
        if isinstance(topology, str):
            if '\n' in str(topology):
                return str(topology), []
            else:
                topology = Path(topology)
                topology_file = str(topology.resolve())
                with open(topology_file, 'r') as f:
                    ret_topology = f.read()
                return ret_topology, []

        elif isinstance(topology, Path):
            with open(topology_file, 'r') as f:
                ret_topology = f.read()
            return ret_topology, []

        elif isinstance(topology, List):
            ret_topology = None
            itp_files = []

            for file_name in topology:
                if file_name.endswith('.top'):
                    with open(file_name, 'r') as f:
                        ret_topology = f.read()
                elif file_name.endswith('.itp'):
                    itp_path = Path(file_name)
                    with open(file_name, 'r') as f:
                        itp_content = f.read()
                    itp_file = File(name=itp_path.name, content=itp_content)
                    itp_files.append(itp_file)

            if ret_topology is None:
                raise ValueError(
                    'No topology (.top) file found in the list provided.'
                )
            else:
                return ret_topology, itp_files

        else:
            raise TypeError(
                'Not valid topology format, should be either a string, '
                'a Path or a List of files.'
            )

    def read_template(
        self, template: Union[str, Path]
    ) -> Tuple[str, List[Path]]:
        """Read template file (.mdp)."""
        if '\n' in str(template):
            return str(template), []
        else:
            if isinstance(template, str):
                template = Path(template)
                template_file = str(template.resolve())
                with open(template_file, 'r') as f:
                    ret_template = f.read()
                return ret_template, []
            elif isinstance(template, Path):
                with open(template, 'r') as f:
                    ret_template = f.read()
                return ret_template, []
            else:
                raise TypeError(
                    'Not valid template format, should be either '
                    'a string or a Path.'
                )

    def read_index(self, index_file: Union[str, Path]) -> str:
        """Read index file (.ndx)."""
        if '\n' in str(index_file):
            return str(index_file)
        else:
            if isinstance(index_file, str):
                index_file = Path(index_file)
                index_file_file = str(index_file.resolve())
                with open(index_file_file, 'r') as f:
                    ret_index_file = f.read()
                return ret_index_file
            elif isinstance(index_file, Path):
                with open(index_file, 'r') as f:
                    ret_index_file = f.read()
                return ret_index_file
            else:
                raise TypeError(
                    'Not valid template format, should be either '
                    'a string or a Path.'
                )

    def author(self):
        """Get interface authors email adress."""
        return 'manuelna@kjemi.uio.no, s.l.bore@kjemi.uio.no'

    @run_sequentially
    def gmx_setup(self, arguments, **kwargs):
        """Use gmx functions to calculate properties.

        Parameters
        ----------
        arguments : list
            Arguments to be passed to gmx function,
            e.g. ['solvate', '-box', '5', '5', '5']
        input_files : list
            List of input files with specifications
            e.g. ['-c', 'structure.gro', '-p', 'topology.top']
        output_files : list
            List of output files with specifications
            e.g. ['-o', 'output.gro']
        commandline_options : str
            Commandline options that are passed to avoid interactive mode

        Returns
        -------
        RunSettings
            RunSettings object

        """
        input_files = kwargs.get('input_files', [])
        output_files = kwargs.get('output_files', [])
        commandline_options = kwargs.get('commandline_options', [])
        if isinstance(arguments, str):
            arguments = arguments.split()

        run_settings = deepcopy(self.run_settings)
        if 'run_opt' in kwargs.keys():
            run_settings = replace(run_settings, **kwargs.pop('run_opt'))

        if 'program' in kwargs.keys():
            program = kwargs.pop('program')
        else:
            program = 'gmx'
        hash_str = kwargs.pop(
            'hash', unique_filename(input_files + output_files)
        )
        data_files = []

        input_files_tmp = []
        for i, f in enumerate(input_files):
            # check that not an argument, such as '-c'
            input_files_tmp.append(f)
            if f.startswith('-'):
                continue
            else:
                input_file = File(
                    name=Path(f), handler=run_settings.file_handler
                )
                # this is a data file, we need to use absolute path
                if self.arch_type == 'remote':
                    input_files_tmp[i] = str(input_file.data_path_remote)
                else:
                    # use absolute path
                    input_files_tmp[i] = str(input_file.data_path_local)
                data_files.append(input_file)
        files_to_parse = []
        output_file = None
        for i, f in enumerate(output_files):
            # check that not an argument, such as '-c'
            if f.startswith('-'):
                continue
            else:
                f = f.replace('{hash}', hash_str)
                files_to_parse.append(File(name=Path(f)))
                output_files[i] = f
                if output_files[i - 1] == '-o':
                    # remove from files_to_parse
                    files_to_parse.pop()

                    output_file = File(name=Path(f))
        setup_dict = {}
        setup_dict.update(
            {
                'program': program,
                'files_to_write': [],
                'files_to_parse': files_to_parse,
                'output_file': output_file,
                'stdout_file': File(
                    name=f'{hash_str}.{arguments[0]}.gmx_stdout'
                ),
                'stderr_file': File(
                    name=f'{hash_str}.{arguments[0]}.gmx_stderr'
                ),
                'data_files': data_files,
                'args': arguments + input_files_tmp + output_files,
            }
        )
        if commandline_options:
            setup_dict.update({'pre_cmd': f'echo {commandline_options} |'})
        return replace(deepcopy(run_settings), **setup_dict)

    @run_sequentially
    def parse_gmx(self, unparsed_output, **kwargs) -> dict:
        """Parse gmx output."""
        out_dict = {}
        files = {}
        for fn in unparsed_output.files_to_parse:
            if str(fn).endswith('.xvg'):
                key = fn.name
                key = key.replace('.xvg', '')
                key = '.'.join(key.split('.')[1:])
                out_dict[key] = self.read_xvg(str(fn))
                files[key] = str(fn)
        out_dict['files'] = files
        try:
            out_dict['output'] = self.read_xvg(
                str(unparsed_output.output_file)
            )
        except Exception:
            out_dict['output'] = {'file': str(unparsed_output.output_file)}

        return out_dict

    @run_sequentially
    async def gmx_arun(self, arguments, **kwargs):
        """Run calculation context-based and asynchronous."""
        setup_tmp = self.gmx_setup(arguments, **kwargs)
        unparsed_output = await self.compute_settings.arun(setup_tmp)
        return self.parse_gmx(unparsed_output, **kwargs)

    @run_sequentially
    def gmx_run(self, arguments, **kwargs):
        """Run calculation context-based."""
        setup_tmp = self.gmx_setup(arguments, **kwargs)
        unparsed_output = self.compute_settings.run(setup_tmp)
        return self.parse_gmx(unparsed_output, **kwargs)

    def check_version(self):
        """Check lammps version."""
        # running_dict = {
        #     'program':
        #     self._set_parallel(self.compute_settings.ntasks, self.plugin)
        # }
        # if self.launcher is not None:
        #     running_dict.update({'launcher': self.launcher})

        # out = self.runner.run(running_dict)
        out = self.run(None)
        # print(out)
        # version = None
        # for line in out.split('\n'):
        #     if 'LAMMPS' in line:
        #         version = line
        try:
            version = out['version']
        except KeyError:
            raise ValueError(f'Could not find GROMACS version in {out}')

        # if version not in VERSIONS_SUPPORTED:
        #   raise NotImplementedError(
        #      f' LAMMPS version {version} not supported.',
        #     f' Please contact {self.author} ',
        # )
        return version

    def read_xvg(self, file_path, **kwargs):
        """Read an XVG file and extracts the data into a pandas DataFrame.

        Parameters
        ----------
        file_path : str
            The path to the XVG file to be read.

        Returns
        -------
        pandas.DataFrame
            A DataFrame containing the data from the XVG file. The first column
            is assumed to be 'Time', and subsequent columns are named according
            to the legends found in the file. The DataFrame also has an
            attribute 'comments' which contains the header comments from the
            file.

        Notes
        -----
        - Lines starting with '#' are considered comments and are stored in the
          'comments' attribute.
        - Lines starting with '@' are used to extract legend information
          for the columns.
        - Data lines are assumed to start after the first non-comment,
          non-legend line.

        """
        with open(file_path, 'r', encoding='utf-8') as file:
            lines = file.readlines()

        # Extract header comments and legend information
        comments = []
        legends = []
        data_lines = []
        first_data_found = False

        for line in lines:
            if line.startswith('#'):
                comments.append(line.strip())
            # look for axis labels
            if 'axis' in line:  # such as @    xaxis  label "r (nm)"
                # use regex to extract the label
                xaxis_label = line.split('"')[1]
                xaxis_label = xaxis_label.split()[0].strip()
                legends.append(xaxis_label)

            if line.startswith('@ s') and ' legend' in line:
                legend_label = line.split('"')[
                    1
                ]  # Get the legend text between the quotes
                legends.append(legend_label)
            elif not line.startswith(('@', '#')):
                if not first_data_found:
                    first_data_found = True
                data_lines.append(line)

        # Create the data string for pandas
        data_str = ''.join(data_lines)

        # Read the data into a DataFrame
        data = pd.read_csv(
            StringIO(data_str),
            delim_whitespace=True,
            header=None,
            # names=columns,
        )
        # Create column names list (assuming Time is always the first column)
        if len(legends) > 2:
            # remove second element
            legends.pop(1)
        columns = legends
        data.columns = columns
        # Attach comments as attributes to the DataFrame
        data.attrs['comments'] = comments

        return data

    def run(self, data, *args, **kwargs):
        """Run calculation context-based and asyncronous."""
        setup_tmp = self.setup(data, **kwargs)
        results = self.compute_settings.run(setup_tmp)
        return self.parse(results[-1])

    async def arun(self, data, *args, **kwargs):
        """Run calculation context-based and asyncronous."""
        program_opt = kwargs.pop('program_opt', {})
        if program_opt is None:
            self.logger.warning(
                'program_opt is None, define it as an empty dict.'
            )
            program_opt = {}
        setup_tmp = self.setup(data, **program_opt, **kwargs)
        results = await self.compute_settings.arun(setup_tmp)
        return self.parse(results[-1])

    # async def arun(self, data, *args, **kwargs):
    #     """Run calculation context-based and asyncronous."""
    #     async with acompute(self, data, *args, **kwargs) as r:
    #         return r[1]
