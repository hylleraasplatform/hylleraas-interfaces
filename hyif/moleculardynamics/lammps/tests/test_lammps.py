# import copy

# import numpy as np
# import pytest
# from hyobj import Constants, Molecule, PeriodicSystem, Units
# from hyset import File, RunSettings, create_compute_settings

# from hyif import Lammps

# work_dir = '/Users/tilmann/Documents/work/hylleraas/hyif/hyif/hyif/'
# work_dir += 'moleculardynamics/lammps/tests/'
# wdir = '/root/scratch/'

# myenv_lammps = create_compute_settings('docker',
#                                        image='active_learning',
#                                        work_dir=work_dir,
#                                        container_work_dir=work_dir,
#                                        debug=True,
#                                        launcher='conda run -n lammps',
#                                        force_recompute=True)

# mymol = Molecule('O')
# mymol.properties = {
#     'unit_cell': [20, 0, 0, 0, 20, 0, 0, 0, 20],
#     'unit': 'angstrom'
# }
# mymol2 = PeriodicSystem(mymol,
#                         pbc=np.array([[20, 0, 0], [0, 20, 0], [0, 0, 20]]))
# # print(mymol2)
# mymol3 = copy.deepcopy(mymol2)
# mymol3.properties['unit'] = 'bohr'
# mymol3.coordinates = mymol3.coordinates * 1.88973
# mymol3.pbc = np.array(mymol3.pbc) * 1.88973

# mylammps = Lammps({})


# def test_defaults():
#     """Test LAMMPS defaults."""
#     defaults = mylammps.template
#     assert 'MOLECULE_FILE' in defaults


# def test_version_check():
#     """Test LAMMPS version check."""
#     version = mylammps.check_version()
#     assert 'LAMMPS' in version


# def test_units():
#     """Test LAMMPS units."""
#     # mol_a = Molecule('O', optimize_smiles = 'mmff')
#     # print(mol_a.distance(0,1))
#     # mol_b = copy.deepcopy(mol_a)
#     # mol_b.coordinates /= Constants.bohr2angstroms
#     # mol_b.units = 'bohr'
#     # print(mol_b.distance(0,1))
#     # print(mol_b, mol_a)
#     # påokk
#     # mymol_bohr = PeriodicSystem(Molecule('O'), pbc = np.eye(3)*20,
#     # unit='bohr')
#     # mymol_b
#     # print('working', mymol2)
#     # print('not working', mymol_bohr)
#     # a = mylammps.get_trajectories(mymol2)
#     # print('working', a)
#     # a = mylammps.get_trajectories(mymol_bohr)
#     # print('notworking', a)

#     # mymol_angstrom = copy.deepcopy(mymol_bohr)
#     # mymol_angstrom.properties['unit'] = 'angstrom'
#     # mymol_angstrom._coordinates *= Constants.bohr2angstroms
#     # mymol_angstrom._pbc *= Constants.bohr2angstroms

#     # print('mymol:bohr')
#     # print(mymol_bohr)
#     # print('mymol wokring')
#     # print(mymol2)
#     # jiji
#     # a1 = mylammps.run(mymol2)
#     # print(np.array(a1['trajectories']).shape)
#     # a1 = mylammps.run(mymol2)
#     # print(np.array(a1['trajectories']).shape)

#     # fg
#     t1 = mylammps.get_trajectories(mymol2)
#     t2 = mylammps.get_trajectories(mymol2, units=Units('real'))
#     t3 = mylammps.get_trajectories(mymol2, units=Units('metal'))
#     t4 = mylammps.get_trajectories(mymol2, units=Units('atomic'))
#     t1 = t1.trajectories[-10:, :]
#     t2 = t2.trajectories[-10:, :]
#     t3 = t3.trajectories[-10:, :]
#     t4 = t4.trajectories[-10:, :]

#     np.testing.assert_allclose(t1, t2)
#     np.testing.assert_allclose(t1, t3)
#     np.testing.assert_allclose(t1,
#                                t4 * Constants.bohr2angstroms,
#                                rtol=1e-6,
#                                atol=1e-5)
#     print('testing sucessful')


# def test_setup():
#     """Test LAMMP setup function."""
#     setup = mylammps.setup(mymol2)
#     assert (setup.output_file.name ==
#             'f574c239f14e3c71f3bd0df2fc5e082e25ab3280.out')
#     with pytest.raises(ValueError):
#         mylammps.setup(44)


# def test_run():
#     """Test LAMMP run function."""
#     mylammps = Lammps({}, compute_settings=myenv_lammps)
#     out = mylammps.run(mymol2)
#     assert out['trajectories'].shape[1] == 9
#     assert out['boxes'].shape[1] == 9


# def test_async():
#     """Test LAMMP async run function."""
#     import asyncio

#     rr = mylammps.run(mymol2)
#     print(rr)
#     r0 = asyncio.run(mylammps.arun(mymol2))
#     print(r0)
#     # r1 = mylammps.get_trajectories(mymol2)
#     # print(r1, '\n')
#     # r = asyncio.run(mylammps.aget_trajectories(mymol2))
#     # print(r)


# if __name__ == '__main__':
#     test_units()
from datetime import timedelta

import hyset
import pytest
from hyobj import Molecule, PeriodicSystem

from hyif import Lammps

INPUT = """variable	T equal {{temperature}}
variable	P equal {{pressure}}
variable	dt equal {{timestep}}
variable	nsteps equal {{nsteps}}
variable	dump_freq equal {{dump_freq}}
variable	thermo_freq equal ${dump_freq}
units	metal
boundary	p p p
atom_style	atomic
neighbor	2.0 bin
neigh_modify	every 25 delay 0 check no
box tilt large
read_data	{{hash}}.data
pair_style	lj/cut 2.5
pair_coeff	* * 1 1
velocity	all create ${T} 19949 rot yes dist gaussian
velocity	all zero linear
velocity	all zero angular
{{npt_str}}
timestep	${dt}
"""
INPUT += 'thermo_style	custom step etotal pe ke enthalpy density '
INPUT += 'lx ly lz vol pxx pyy pzz press\n'
INPUT += """thermo	${thermo_freq}
thermo_modify	flush yes
"""
INPUT += 'fix	thermo_print all print ${dump_freq} "$(step) $(time) $(temp) '
INPUT += '$(etotal) $(pe) $(ke) $(enthalpy) $(density) $(lx) $(ly) '
INPUT += '$(lz) $(vol) $(pxx) $(pyy) $(pzz) $(press)" append '
INPUT += '{{hash}}.thermo.out screen no title "# step time temp etotal pe ke '
INPUT += 'enthalpy density lx ly lz vol pxx pyy pzz press"\n'
INPUT += """dump	1 all atom ${dump_freq} {{hash}}.lammpstrj
dump_modify	1 sort id append yes
restart	10000 {{hash}}.restart.1 {{hash}}.restart.2
timer timeout {{timeout}} 100
run	${nsteps} upto
write_restart	{{hash}}.restart.1
write_data	{{hash}}.datafile_final nocoeff
"""


def test_setup():
    """Test LAMMP setup function."""
    mylammps = Lammps({'check_version': False})
    molecule = Molecule('O')
    molecule.properties = {
        'unit_cell': [20, 0, 0, 0, 20, 0, 0, 0, 20],
        'unit': 'angstrom',
    }

    npt_str = 'fix	npt all npt temp ${T} ${T} 0.1 iso ${P} ${P} 0.1'
    # Verify that no error is raised when running a single molecule
    mylammps.setup(molecule)

    # Check that error is raised if npt_str is not in template
    with pytest.raises(ValueError):
        mylammps.setup(molecule, template=INPUT)

    # Check that error is raised if npt_str is not in template
    with pytest.raises(KeyError):
        mylammps.setup(molecule, program_opt=dict(npt_str=npt_str))

    # Check that no error is raised if npt_str is in template
    mylammps.setup(molecule, program_opt=dict(npt_str=npt_str), template=INPUT)

    out = mylammps.setup(
        molecule,
        program_opt=dict(npt_str=npt_str, restart='dummy.restart'),
        template=INPUT,
    )
    assert 'read_restart' in out.files_to_write[-1].content
    assert 'read_data' not in out.files_to_write[-1].content


def test_gen_system_str_from_molecule():
    """Test LAMMP gen_system_str_from_molecule function."""
    mylammps = Lammps({'check_version': False})
    #
    molecule = PeriodicSystem(
        {
            'atoms': ['O', 'H', 'H'],
            'coordinates': [[10, 10, 10], [10, 11, 10], [10, 10, 11]],
            'pbc': [[20, 0, 0], [0, 20, 0], [0, 0, 20]],
        }
    )

    # Verify that no error is raised when running a single molecule
    str1 = mylammps.gen_system_str_from_molecule(molecule)
    str2 = mylammps.gen_system_str_from_molecule(molecule, label_atoms=False)

    # Check that the two strings are different
    assert str1 != str2

    mylammps_no_labels = Lammps({'check_version': False, 'label_atoms': False})
    # make a setup
    setup = mylammps_no_labels.setup(molecule)
    no_labels_input = setup.files_to_write[0].content
    # Check that the two strings are the same
    assert no_labels_input == str2

    # make another setup
    setup = mylammps.setup(molecule)
    labels_input = setup.files_to_write[0].content
    # Check that the two strings are the same
    assert labels_input == str1

    # Check that overide works
    str3 = mylammps.gen_system_str_from_molecule(molecule, label_atoms=False)
    assert str3 == str2

    # Test that velocities are added
    molecule2 = PeriodicSystem(
        {
            'atoms': ['O', 'H', 'H'],
            'coordinates': [[10, 10, 10], [10, 11, 10], [10, 10, 11]],
            'pbc': [[20, 0, 0], [0, 20, 0], [0, 0, 20]],
        }
    )
    molecule2.properties['velocities'] = [[1, 0, 0], [0, 0, 0], [0, 0, 0]]
    str4 = mylammps.gen_system_str_from_molecule(molecule2)
    assert 'Velocities' in str4
    # write to file and try to read it back with ase
    with open('test.lmp', 'w') as f:
        f.write(str4)
    from ase.io import read

    # check that the file can be read by ase
    read('test.lmp', format='lammps-data', style='atomic', units='metal')


def test_setting_program():
    """Test setting lammps executable."""
    molecule = PeriodicSystem(
        {
            'atoms': ['O', 'H', 'H'],
            'coordinates': [[10, 10, 10], [10, 11, 10], [10, 10, 11]],
            'pbc': [[20, 0, 0], [0, 20, 0], [0, 0, 20]],
        }
    )
    mylammps = Lammps({'check_version': False})
    assert mylammps.program == 'lmp'

    mylammps = Lammps({'check_version': False, 'program': 'lmp_mpi'})
    assert mylammps.program == 'lmp_mpi'

    setup = mylammps.setup(molecule)
    print(setup)
    assert setup.program == 'lmp_mpi'

    setup = mylammps.setup(molecule, program='lmp')
    assert setup.program == 'lmp'


def test_timeout():
    """Test setting lammps executable."""
    molecule = PeriodicSystem(
        {
            'atoms': ['O', 'H', 'H'],
            'coordinates': [[10, 10, 10], [10, 11, 10], [10, 10, 11]],
            'pbc': [[20, 0, 0], [0, 20, 0], [0, 0, 20]],
        }
    )
    mylammps = Lammps({'check_version': False})
    # verify that there is no timer line in the input
    setup = mylammps.setup(molecule)
    assert 'timer' not in setup.files_to_write[-1].content

    env_deepmd = hyset.RemoteArch(
        target='lumi',
        host='lumi',
        user='dummy',
        slurm_account='dummy',
        job_time=timedelta(hours=48),
    )

    mylammps = Lammps({'check_version': False}, compute_settings=env_deepmd)
    # verify that there is a timer line in the input
    setup = mylammps.setup(
        molecule, template=INPUT, program_opt=dict(npt_str='dummy')
    )
    assert 'timer' in setup.files_to_write[-1].content

    # for small job_time of 100 seconds, the timeout should be 90 seconds
    env_deepmd = hyset.RemoteArch(
        target='lumi',
        host='lumi',
        user='dummy',
        slurm_account='dummy',
        job_time=timedelta(seconds=100),
    )
    env_deepmd.job_time = timedelta(seconds=100)
    mylammps = Lammps({'check_version': False}, compute_settings=env_deepmd)
    assert mylammps.timeout == '0:1:30'
