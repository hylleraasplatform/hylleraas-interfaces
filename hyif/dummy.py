from .importme import HylleraasInterface


class Dummy(HylleraasInterface):
    """Dummy class."""

    def __init__(self, *args):
        """Initialize."""

    def restart(self, *args):
        """Prepare restart for computation class."""

    @classmethod
    def get_input_molecule(cls, *args):
        """Get molecule input."""
        return 'Hy', [0, 0, 0]

    @property
    def version(self):
        """Set version."""
        return '0.0'

    @property
    def author(self):
        """Set authors name or email adress."""
        return 'unknown'


class Dummy2(HylleraasInterface):
    """Another Dummy class."""

    def __init__(self):
        """Initialize."""

    @property
    def version(self):
        """Set version."""
        return '0.0'

    @property
    def author(self):
        """Set authors name or email adress."""
        return 'unknown'
