# from .dummy import Dummy, Dummy2
# from .quantumchemistry import (Dalton, CP2K, London, LSDalton, MRChem,
#                                Orca, Xtb)
from typing import Union

from .generic import Generic
from .machinelearning import DeepMD, HylleraasMLInterface, NequIP
from .moleculardynamics import Gromacs, HylleraasMDInterface, Lammps
from .quantumchemistry import (CP2K, VASP, Cfour, Crest, Dalton,
                               HylleraasQMInterface, HyQD, London, LSDalton,
                               MRChem, Orca, Respect, Xtb)

HylleraasInterface = Union[
    HylleraasQMInterface, HylleraasMDInterface, HylleraasMLInterface
]
PROGRAMS: dict = {
    'dummy': 'Dummy',
    'dummy2': 'Dummy2',
    'dalton': 'Dalton',
    'london': 'London',
    'lsdalton': 'LSDalton',
    'mrchem': 'MRChem',
    'orca': 'Orca',
    'xtb': 'Xtb',
    'cp2k': 'CP2K',
    'deepmd': 'DeepMD',
    'lammps': 'Lammps',
    'python': 'Generic',
    'bash': 'Generic',
    'nequip': 'NequIP',
    'vasp': 'VASP',
    'crest': 'Crest',
    'respect': 'Respect',
    'hyqd': 'HyQD',
    'gromacs': 'Gromacs',
    'cfour': 'Cfour',
}


__all__ = [
    'DeepMD',
    'Lammps',
    'CP2K',
    'Dalton',
    'MRChem',
    'Orca',
    'Xtb',
    'HylleraasInterface',
    'LSDalton',
    'London',
    'Generic',
    'NequIP',
    'VASP',
    'Crest',
    'Respect',
    'HyQD',
    'Gromacs',
    'Cfour'
]
