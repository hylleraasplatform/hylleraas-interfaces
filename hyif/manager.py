from contextlib import asynccontextmanager, contextmanager
from copy import deepcopy
from dataclasses import replace
from typing import Callable, Union

from hyobj import HylleraasObject
from hyset import ComputeResult, File, FileHandler, RunSettings

__all__ = [
    'compute',
    'acompute',
    'NewRun',
    'Run',
    'PreSetup',
    'File',
    'FileHandler',
]

# from .machinelearning import HylleraasMLInterface
# from .moleculardynamics import HylleraasMDInterface
# from .quantumchemistry import HylleraasQMInterface

# HylleraasInterface = Union[HylleraasQMInterface,
#                            HylleraasMDInterface,
#                            HylleraasMLInterface]


@contextmanager
def compute(
    method: Callable, object: Union[HylleraasObject, None], *args, **kwargs
):
    """Generate context for computations.

    Parameters
    ----------
    method: :class:`hyif.Method` instance
        Method.
    object: :class:`hyobj.*`
        Object.
    args: tuple
        Arguments.
    kwargs: dict
        Keyword arguments.

    Yields
    ------
    dict
        parsed Result.

    """
    run_settings: RunSettings
    run_settings = method.setup(object, *args, **kwargs)  # type: ignore
    result: ComputeResult
    result = method.compute_settings.run(run_settings)  # type: ignore
    try:
        yield method.parse(result)  # type: ignore
    finally:
        method.compute_settings.teardown(run_settings)  # type: ignore


@asynccontextmanager
async def acompute(method: Callable, object: HylleraasObject, *args, **kwargs):
    """Async context for computations."""
    run_settings: RunSettings
    run_settings = method.setup(object, *args, **kwargs)  # type: ignore
    result: ComputeResult
    result = await method.compute_settings.arun(run_settings)  # type: ignore
    try:
        yield method.parse(result)  # type: ignore
    finally:
        await method.compute_settings.ateardown(run_settings)  # type: ignore


class NewRun:
    """New run functions."""

    def run(self, object: HylleraasObject, *args, **kwargs):
        """Run calculation context-based."""
        with compute(self, object, *args, **kwargs) as r:  # type: ignore
            return r

    async def arun(self, object: HylleraasObject, *args, **kwargs):
        """Run calculation context-based."""
        async with acompute(
            self, object, *args, **kwargs  # type: ignore
        ) as r:  # type: ignore
            return r


class Run(NewRun):
    """Run functions."""


class PreSetup:
    """Pre setup."""

    def __init__(self):
        """Initialize."""

    def pre_setup(self, **kwargs):
        """Pre setup."""
        update_dict: dict = {}
        run_settings = deepcopy(self.compute_settings.run_settings)
        if hasattr(self, 'default_run_settings'):
            update_dict.update(self.default_run_settings)
        if 'run_opt' in kwargs.keys():
            update_dict.update(kwargs.pop('run_opt'))

        return replace(run_settings, **update_dict)
