from __future__ import annotations

from typing import Dict, List, Optional

import numpy as np
from hyset import ComputeSettings, create_compute_settings

# from ..utils import get_val_from_arglist, key_in_arglist
from ...abc import HylleraasQMInterface
from ...molecule import Molecule

DEFAULT_KW: List[str] = ['basis', 'qcmethod', 'properties']
IGNORE_KW: List[str] = ['program', 'molecule', '_type', 'compute_settings']

try:
    import daltonproject as dp
    dp_found = True
except Exception:
    dp_found = False


class Dalton(HylleraasQMInterface):
    """Dalton interface (via daltonproject)."""

    def __init__(self,
                 method: dict,
                 compute_settings: Optional[ComputeSettings] = None):

        if not dp_found:
            raise ImportError('daltonproject not installed')

        if compute_settings is None:
            self.compute_settings = create_compute_settings()
        else:
            self.compute_settings = compute_settings

        self.method = method

        self.generate_input(self.method, self.compute_settings)
        self.force_recompute = self.compute_settings.force_recompute

    def check_version(self):
        """Check performed by daltonproject."""
        pass

    @classmethod
    def gen_molecule(cls, molecule: Molecule):
        """Generate daltonproject Molecule instance."""
        units = molecule.units  # type: ignore
        coords = np.array(molecule.coordinates)
        if units.length[0] == 'bohr':
            raise Exception('--temporary hack: only angstrom units allowed')
            coords = np.array(molecule.coordinates) / units.length[1]
        mol_string = ''
        for i, atom in enumerate(molecule.atoms):
            mol_string += atom
            mol_string += ' {:.12f}'.format(coords[i, 0])
            mol_string += ' {:.12f}'.format(coords[i, 1])
            mol_string += ' {:.12f}'.format(coords[i, 2])
            mol_string += '\n'

        return dp.Molecule(atoms=mol_string)

    def generate_input(self, method: dict, compute_settings: ComputeSettings
                       ) -> None:
        """Generate instances for daltonproject Compute class."""
        self.dp_input = self.set_default_options(method)
        self.dp_basis = dp.Basis(self.dp_input['basis'])
        if isinstance(self.dp_input['qcmethod'], str):
            self.dp_method = dp.QCMethod(
                                qc_method=self.dp_input['qcmethod'][0]
                                        )
        elif len(self.dp_input['qcmethod']) == 2:
            self.dp_method = dp.QCMethod(
                qc_method=self.dp_input['qcmethod'][0],
                xc_functional=self.dp_input['qcmethod'][1])

        self.dp_properties = dp.Property(
                energy=('energy' in self.dp_input['properties']),
                gradients=(any([kw in self.dp_input['properties'] for kw in
                                ['analytical_gradient', 'gradient']])),
                two_photon_absorption=('two_photon_absorption' in
                                       self.dp_input['properties']),
                geometry_optimization=('geometry_optimization' in
                                       self.dp_input['properties']),
                excitation_energies=('excitation_energies' in
                                     self.dp_input['properties']),
                hessian=(any([kw in self.dp_input['properties'] for kw in
                              ['analytical_hessian',
                               'vibrational_modes',
                               'normal_modes',
                               'vibrational_frequencies']])),
                dipole_gradients=('dipole_gradients' in
                                  self.dp_input['properties']),
                polarizability_gradients=(
                    self.dp_input['properties']['polarizability_gradients']
                    if 'polarizability_gradients' in
                    self.dp_input['properties'] else False),
                nmr_shieldings=('nmr_shieldings' in
                                self.dp_input['properties']),
                spin_spin_couplings=('spin_spin_couplings' in
                                     self.dp_input['properties']),
                polarizabilities=(
                    self.dp_input['properties']['polarizabilities']
                    if 'polarizabilities' in self.dp_input['properties']
                    else False)
        )

        if compute_settings.name == 'local':
            self.dp_settings = dp.ComputeSettings(
                mpi_num_procs=compute_settings.ntasks,
                launcher=compute_settings.launcher,
                omp_num_threads=compute_settings.cpus_per_task,
                work_dir=compute_settings.work_dir_local,
                scratch_dir=compute_settings.scratch_dir_local)
        else:
            self.dp_settings = None

    def set_default_options(self, input_dict: dict) -> dict:
        """Set default options for daltonproject."""
        if input_dict is None:
            input_kw = []
        else:
            input_kw = [
                item.lower()
                for item in input_dict
                if item.lower() not in IGNORE_KW
            ]
        output_dict: dict = {}
        output_dict['basis'] = 'pcseg-1'
        output_dict['qcmethod'] = ['DFT', 'B3LYP']
        output_dict['properties'] = ['energy']

        for key in input_kw:
            if key in DEFAULT_KW:
                output_dict[key] = input_dict[key]
            else:
                raise NotImplementedError(
                    'could not generate interface '
                    f'{self.__class__.__name__} for keyword {key} '
                    f'available keywords are {DEFAULT_KW}')
        return output_dict

    # @classmethod
    # def get_input_molecule(cls, *args):
    #     """Generate input, in this unnecessary."""
    #     pass

    # @classmethod
    # def get_input_method(cls, *args):
    #     """Generate input, in this unnecessary."""
    #     pass

    # def update_coordinates(self, coordinates) -> None:
    #     """Update coordinates on interface side."""
    #     self.dp_molecule.coordinates = coordinates.reshape(-1, 3)
    #     return

    def run(self, molecule: Molecule, options: Optional[dict] = {}):
        """Run a computation with DALTON.

        Parameter
        ---------
        molecule : hyobj.Molecule
            molecule for that computation should be performed
        options: dict
            additional options

        Returns
        -------
        dict
            results

        """
        self.dp_molecule = Dalton.gen_molecule(molecule)
        properties = options.get('properties', self.dp_properties)

        self.result = dp.dalton.compute(
            molecule=self.dp_molecule,
            basis=self.dp_basis,
            qc_method=self.dp_method,
            properties=properties,
            compute_settings=self.dp_settings,
            force_recompute=self.force_recompute
        )
        # TODO: return self.result.__dict__
        if any(kw in properties for kw in ['normal_modes',
                                           'vibrational_modes',
                                           'vibrational_frequencies']):
            try:
                from daltonproject import vibrational_analysis
            except ImportError:
                raise ImportError('vibrational_analysis from daltonproject ' +
                                  'not available')
            else:
                spectrum = vibrational_analysis(molecule=self.dp_molecule,
                                                hessian=self.result.hessian)
                return {'frequencies': spectrum.frequencies,
                        'normal_modes': spectrum.cartesian_displacements}
        return self.result

    def get_energy(self, molecule: Molecule) -> float:
        """Compute energy."""
        result = self.run(molecule)
        return result.energy

    def get_gradient(self, molecule: Molecule) -> np.ndarray:
        """Compute gradient."""
        properties = dp.Property(gradients=True)
        result = self.run(molecule, options={'properties': properties})
        return np.ndarray(result.gradients)

    def get_hessian(self, molecule: Molecule):
        """Read hessian. TODO."""
        pass

    def get_summary(self) -> Dict:
        """Extract information from Dalton outputfile."""
        summary_dict: dict = {}
        # could not find this information in output-file
        summary_dict['thresh_energy'] = None
        summary_dict['steps'] = -1
        energies = []
        gradnorms = []
        with open(f'{self.result.filename}.out', 'r') as output_file:
            for line in output_file:
                if 'BFGS update' in line:
                    summary_dict['method'] = 'BFGS'
                if 'Convergence threshold for gradient set to' in line:
                    summary_dict['rms_grad'] = float(line.split()[7].replace(
                        'D', 'E'))
                    summary_dict['max_grad'] = summary_dict['rms_grad']
                if 'Convergence threshold for energy set to' in line:
                    summary_dict['thresh_energy'] = float(
                        line.split()[7].replace('D', 'E'))
                if 'Convergence threshold for step set to' in line:
                    summary_dict['rms_step'] = float(line.split()[7].replace(
                        'D', 'E'))
                    summary_dict['max_step'] = summary_dict['rms_step']
                if 'Geometry converged in ' in line:
                    summary_dict['steps'] = int(line.split()[4])

                if (all(['Iter     Energy        Change       GradNorm'
                        in line,
                        'Index   StepLen    TrustRad #Rej' in line]
                        )):
                    output_file.readline()
                    # workaround for reading the optimization table summary
                    check_iter = True
                    check_en = True
                    check_line = 9
                    while check_iter and check_en and check_line == 9:
                        split_line = output_file.readline().split()
                        try:
                            int(split_line[1])
                        except Exception:
                            check_iter = False
                        else:
                            check_iter = True

                        try:
                            float(split_line[2])
                        except Exception:
                            check_en = False
                        else:
                            check_en = True

                        check_line = len(split_line)

                        if check_line == 9:
                            energies.append(float(split_line[2]))
                            gradnorms.append(float(split_line[4]))

        if len(energies) != summary_dict['steps']:
            print(summary_dict['steps'])
            print(energies)
            raise Exception('Dalton geometry output not consistent')

        if len(energies) > 0:
            summary_dict['energies'] = energies
            summary_dict['gradient_norms'] = gradnorms
            summary_dict['final_energy'] = energies[-1]
            summary_dict['final_gradient_norm'] = gradnorms[-1]
            if len(energies) > 1:
                summary_dict['final_energy_change'] = energies[-2] - \
                    energies[-1]

        return summary_dict

    @property
    def version(self):
        """Set interface version."""
        return '0.0'

    @property
    def author(self):
        """Set author's name or email adress."""
        return 'tilmann.bodenstein@kjemi.uio.no'
