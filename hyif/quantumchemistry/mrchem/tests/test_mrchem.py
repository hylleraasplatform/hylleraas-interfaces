import numpy as np
# import pytest
from hyobj import Molecule
from hyset import create_compute_settings

from hyif import MRChem

seqenv = create_compute_settings(
    'local',
    ntasks=1,
    debug=False
)

parenv = create_compute_settings(
    'local',
    ntasks=4,
    launcher='mpirun --bind-to none',
    debug=False
)

hydrogen_fluoride = Molecule({
    'atoms': ['F', 'H'],
    'coordinates': [
        [0.0, 0.0, 0.0],
        [0.0, 0.0, 0.917]
    ]},
    units='angstrom'
)

# Reference results were obtained with world_prec as 1.0e-8
world_prec = 1.0e-4

energy_dict = {
    'qcmethod': 'hf',
    'specific_input': {
        'world_prec': world_prec,
        'SCF': {
            'guess_prec': 1.0e-3,
            'guess_type': 'sad_gto',
            'guess_screen': 12.0
        }
    },
}

gradient_dict = {
    'qcmethod': 'hf',
    'specific_input': {
        'world_prec': world_prec,
        'Properties': {'geometric_derivative': True},
        'SCF': {
            'guess_prec': 1.0e-3,
            'guess_type': 'sad_gto',
            'guess_screen': 12.0
        }
    },
}

nmr_dict = {
    'qcmethod': 'pbe',
    'specific_input': {
        'world_prec': world_prec,
        'Properties': {'nmr_shielding': True},
        'SCF': {
            'guess_prec': 1.0e-3,
            'guess_type': 'sad_gto',
            'guess_screen': 12.0
        },
        'NMRShielding': {
            'nuclear_specific': False,
            'nucleus_k': [0, 1]
        }
    },
}


def test_energy():
    """Test energy calculation."""
    mymrchem = MRChem(energy_dict, compute_settings=seqenv)
    energy = mymrchem.get_energy(hydrogen_fluoride)
    np.testing.assert_allclose(energy, -100.07045453814068, atol=5*world_prec)
    # Test without specifying compute settings
    mymrchem = MRChem(energy_dict)
    energy = mymrchem.get_energy(hydrogen_fluoride)
    np.testing.assert_allclose(energy, -100.07045453814068, atol=5*world_prec)


def test_energy_parallel():
    """Test energy calculation in parallel."""
    mymrchem = MRChem(energy_dict, compute_settings=parenv)
    energy = mymrchem.get_energy(hydrogen_fluoride)
    np.testing.assert_allclose(energy, -100.07045453814068, atol=5*world_prec)


def test_gradient():
    """Test gradient calculation."""
    mymrchem = MRChem(gradient_dict, compute_settings=seqenv)
    gradient = mymrchem.get_gradient(hydrogen_fluoride)
    np.testing.assert_allclose(
        gradient,
        [[
            1.3774828532349496e-08,
            6.105105429262065e-08,
            -0.025496815780835735
         ],
         [
            3.279867140256371e-09,
            1.427786025225816e-08,
            0.02549770785239014
        ]],
        atol=20*world_prec
    )


def test_gradient_parallel():
    """Test gradient calculation in parallel."""
    mymrchem = MRChem(gradient_dict, compute_settings=parenv)
    gradient = mymrchem.get_gradient(hydrogen_fluoride)
    np.testing.assert_allclose(
        gradient,
        [[
            1.3774828532349496e-08,
            6.105105429262065e-08,
            -0.025496815780835735
         ],
         [
            3.279867140256371e-09,
            1.427786025225816e-08,
            0.02549770785239014
        ]],
        # rtol=world_prec,
        atol=20*world_prec
    )


def test_nmr_shielding():
    """Test NMR calculation."""
    mymrchem = MRChem(nmr_dict, compute_settings=seqenv)
    tensor = mymrchem.get_nmr_shielding(hydrogen_fluoride)
    # Bin Gao, this test is missing some content. Adding one line to get the
    # linting through
    print(tensor)
