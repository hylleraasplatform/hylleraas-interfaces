import json
import re
from pathlib import Path
from typing import Dict, Union

import numpy as np
from hyobj import Constants

from ...abc import Parser
from ...molecule import Molecule

bohr2angstrom = Constants.bohr2angstroms


class MRChemInput:
    """Prepare MRChem input File."""

    @classmethod
    def add_mandatory_kw(cls, options: dict) -> dict:
        """Add mandatory options required by MRChem.

        Parameters
        ----------
        options : :obj:`dict`
            user-defined options

        Returns
        -------
        :obj:`dict`
            full (default + user-defined) options dictionary for MRChem

        """
        _options = options.get('specific_input', {})
        if not isinstance(_options, dict):
            raise ValueError(f'Invalid specific_input {_options} for MRChem')

        # For MRChem, the overall precision 'world_prec' and the method
        # 'WaveFunction.method' must be provided
        if 'world_prec' not in _options:
            _options['world_prec'] = 1.0e-4
        # Available wavefunction methods, see
        # https://mrchem.readthedocs.io/en/latest/users/user_ref.html
        qcmethod = str(options.get('qcmethod', '')).lower()
        if qcmethod == '':
            # We use Hartree-Fock (HF) as default wavefunction method
            if 'WaveFunction' in _options:
                if 'method' not in _options['WaveFunction']:
                    _options['WaveFunction']['method'] = 'hf'
            else:
                _options['WaveFunction'] = {'method': 'hf'}
        else:
            # qcmethod will overwrite that specified by 'WaveFunction.method'
            if 'WaveFunction' in _options:
                _options['WaveFunction']['method'] = qcmethod
            else:
                _options['WaveFunction'] = {'method': qcmethod}

        return _options

    @classmethod
    def gen_mrchem_input(cls, options: dict, molecule: Molecule) -> str:
        """Generate input for MRChem.

        Parameters
        ----------
        options : dict
            MRChem options dictionary
        molecule : :obj:`Hylleraas.Molecule`
            molecule input

        Returns
        -------
        str
            input string

        """
        _options = MRChemInput.add_mandatory_kw(options)

        try:
            unit = molecule.units.length[0]
        except KeyError:
            _options['world_unit'] = 'bohr'
        else:
            _options['world_unit'] = 'bohr' if unit == 'bohr' else 'angstrom'

        if 'charge' in molecule.properties:
            charge = molecule.properties['charge']
        else:
            charge = 0

        if 'multiplicity' in molecule.properties:
            multiplicity = molecule.properties['multiplicity']
        else:
            multiplicity = 1

        mol_string = ''
        for i in range(0, len(molecule.atoms)):
            mol_string += molecule.atoms[i]
            for j in range(3):
                mol_string += ' ' + str(molecule.coordinates[i][j])
            mol_string += '\n'

        _options['Molecule'] = {'multiplicity': multiplicity,
                                'charge': charge,
                                'coords': mol_string}

        return json.dumps(_options)


class MRChemOutput(Parser):
    """Parse MRChem output file (json format)."""

    @classmethod
    def parse(cls, output: Union[str, Path]) -> dict:
        """Parse MRChem output.

        Parameter
        ---------
        output: str or :obj:`pathlib.Path`
            output str or path to output file

        Returns
        -------
        dict
            parsed results

        """
        try:
            outfile = Path(str(output))
        except (OSError, TypeError):
            raise Exception(f'OSError or TypeError for output {str(output)}')
        except Exception as e:
            raise e
        else:
            if outfile.exists():
                with open(outfile, 'r') as f:
                    data = json.load(f)
                result: dict = {}
                result['energy'] = (
                    data['output']['properties']['scf_energy']['E_tot']
                )

                try:
                    result['gradient'] = MRChemOutput.extract_gradient(data)
                # KeyError means there is no required key existing for
                # gradients
                except KeyError:
                    pass
                except Exception as e:
                    raise e

                try:
                    result['nmr_shielding'] = (
                        MRChemOutput.extract_nmr_shielding(data)
                    )
                except KeyError:
                    pass
                except Exception as e:
                    raise e

                # FIXME: check if MRChem output file to get warnings and errors
                result['errors'] = []
                result['warnings'] = []

                return result
            else:
                raise FileNotFoundError(f'could not find output {outfile}')

    @classmethod
    def extract_gradient(cls, data: dict) -> np.ndarray:
        """Extract gradient from JSON object.

        Returns
        -------
        :obj:`np.array`
            gradient

        """
        num_atoms = len(data['input']['molecule']['coords'])
        gradient = np.zeros((num_atoms, 3))
        k = 0
        for i in range(num_atoms):
            for j in range(3):
                # FIXME: probably we do not need to multiply with
                # `bohr2angstrom`?
                gradient[i][j] = bohr2angstrom * (
                    data['output']['properties']['geometric_derivative']
                        ['geom-1']['total'][k]
                )
                k += 1
        return gradient

    @classmethod
    def extract_nmr_shielding(
        cls, data: dict
    ) -> Dict[int, Dict[str, np.array]]:
        """Extract NMR shielding tensors from JSON object.

        Returns
        -------
        :obj:`dict[int, dict[str, np.array]]`
            NMR shielding tensors

        """
        tensor = {}
        result = data['output']['properties']['nmr_shielding']
        for key in result:
            atom_index = int(re.search(r'\d+', key).group(0))
            tensor[atom_index] = {
                'total': np.array(result[key]['tensor']),
                'dia': np.array(result[key]['tensor_dia']),
                'para': np.array(result[key]['tensor_para'])
            }
        return tensor
