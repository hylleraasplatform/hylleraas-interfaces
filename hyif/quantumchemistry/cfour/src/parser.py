from dataclasses import dataclass

from ...abc import Parser as BaseParser

# from .input_parser import CFOURinput
# from .output_parser import CFOURoutput


@dataclass
class Parser:
    """Interface to Parser."""

    # InputParser: CFOURinput
    InputParser: BaseParser
    OutputParser: BaseParser
    # OutputParser: CFOURoutput
