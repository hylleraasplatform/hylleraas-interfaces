# =====================================================================
#                         input_parser_test.py
# =====================================================================
# This code parses a cfour input file which is always named "ZMAT"
# ZMAT-file is always structured as: title, geometry, list of keywords
#
# Contains the following routines:
# --------------------------------
# - read_molecule_from_file
# - read_keywords_from_file
# - gen_cfour_input
#
# General structure of a CFOUR input file:
# -----------------------------------------
# title (arbitrary)
# Atom1 x y z   -.
# Atom2 x y z    |-  geometry
# ...           -'
#          (empty line)
# *CFOUR(METHOD=SCF,    -.
# BASIS=cc-pVDZ,         |-  keywords
# ...)                  -'
#          (empty line)
# =====================================================================

from pathlib import Path
from typing import List

import numpy as np
from hyobj import Constants, Units

from ...molecule import Molecule


########################################################
#             Define the class CFOURInput              #
########################################################
class CFOURinput:
    """CFOUR input class."""

    # A class method is declared in this class and can be called as
    # CFOURInput.name-of-function(arguments)

    # ===========================================
    #           Read molecule from file
    # ===========================================
    @classmethod
    def read_molecule_from_file(cls, filename: Path) -> Molecule:
        """Read molecule from file.

        Parameters
        ----------
        filename: :obj:`pathlib.Path`
            path to input file

        Returns
        -------
        Molecule
            Hylleraas-compatible Molecule object
            Contains atoms, coordinates, properties, and units

        """
        # Check if the file exists
        try:
            if not filename.exists():
                raise FileNotFoundError(
                    'could not find ' f'input file {filename}'
                )
        except TypeError:
            raise TypeError(f'expected {type(Path())}, got {type(filename)}')

        unit = 'angstrom'  # Set unit default to angstrom
        charge = 0  # If not specified, default charge is 0
        atoms = []  # List of atoms
        coordinates = []  # List of coordinates

        # Open the file and read the lines
        with open(filename, 'r') as f:
            lines = f.readlines()

        # -------------------------------------------
        #           Extract geometry
        # -------------------------------------------
        i = 1  # Start after the title
        while i < len(lines) and lines[i].strip():
            parts = lines[i].split()
            if len(parts) == 4:
                atoms.append(parts[0])
                x, y, z = map(float, parts[1:4])
                coordinates.append((x, y, z))
            i += 1

        # Skip the empty line between geometry and keywords
        while i < len(lines) and not lines[i].strip():
            i += 1

        # -------------------------------------------
        # Extract keywords concerning charge & units
        # -------------------------------------------
        # Make a loop to read the keywords
        while i < len(lines) and lines[i].strip():
            line = lines[i].strip()
            # If we find the keyword UNITS=BOHR...
            if 'UNITS=BOHR' in line.upper():
                # ... change the units to bohr
                unit = 'bohr'
                print('Units: Bohr')
            # If the keyword CHARGE=x, set the charge to x
            if 'CHARGE=' in line.upper():
                charge = int(line.split('=')[1])
            # Repeat
            i += 1

        # If the unit is angstrom, convert the coordinates to bohr
        if unit == 'angstrom':
            print('Converting coordinates to bohr')
            coordinates = np.array(coordinates) / Constants.bohr2angstroms

        # Hand over the parsed data to the Molecule object
        return Molecule(
            atoms=atoms,
            coordinates=np.array(coordinates),
            properties={'charge': int(charge)},
            units=Units('atomic'),
        )

    # ===========================================
    #       Read keyword section from file
    # ===========================================
    @classmethod
    def read_keywords_from_file(cls, filename: Path) -> List:
        """Read keyword section from file.

        Parameters
        ----------
        filename: :obj:`pathlib.Path`
            path to inputfile

        Returns
        -------
        List
            The method, basis set information & further
            specifications from the keyword section

        """
        try:
            if not filename.exists():
                raise FileNotFoundError(
                    'could not find ' f'input file {filename}'
                )
        except TypeError:
            raise TypeError(f'expected {type(Path())}, got {type(filename)}')

        # Initialize the list of keywords
        keywords = []

        # Open the file and read the lines
        with open(filename, 'r') as f:
            lines = f.readlines()

        # Skip to the line starting with "*" because the
        # keyword section always starts with "*CFOUR()"
        i = 0
        while i < len(lines) and not lines[i].strip().startswith('*'):
            i += 1

        # Read the keywords from the lines starting with "*"
        while i < len(lines):
            # End if we reach an empty line
            if lines[i].strip() == '':
                break
            # For the first keyword, remove "*CFOUR(" and read the keyword
            if '*' in lines[i]:
                keywords.append(lines[i].strip().replace('*CFOUR(', '', 1))
            # For the other keywords...
            else:
                # ... if it's the last keyword, remove the closing parenthesis
                if ')' in lines[i]:
                    keywords.append(lines[i].strip().split(')')[0])
                # ... nothing special happens
                else:
                    keywords.append(lines[i].strip())
            i += 1

        return keywords

    # ===========================================
    #       Generate ZMAT file for CFOUR
    # ===========================================
    @classmethod
    def gen_cfour_input(cls, keywords: list, molecule: Molecule) -> str:
        """Generate input file (ZMAT) for CFOUR.

        Arguments:
        ----------
        keywords: list
            List of keywords
        molecule: obj:Molecule
            (hylleraas-compatible) Molecule object

        Returns
        -------
        input string

        """
        # Initialize the input string and start with the title
        # \n is a newline character
        input_str = 'Generated ZMAT file\n'

        try:
            # hasattr (has attribute) checks if the object has the
            # listed attributes
            if hasattr(molecule, 'atoms'):
                atoms = molecule.atoms
                coords = molecule.coordinates
                # reshape(-1,3): -1 means "as many rows as needed",
                # 3 means 3 columns (x,y,z),
                # -1 tells numpy to figure out the number of rows based
                # on the number of elements in the array
                coords = np.array(coords).reshape(-1, 3)

        except TypeError as e:
            print(f'could not read Molecule from {type(molecule)}, {e}')

        for i in range(len(atoms)):
            # in "{}" we define the width of the string
            # {:<2} means left-aligned with a width of 2 for the character atom
            # {:20.10f} means width of 20 and 10 decimal places
            input_str += ('{:<2}{:20.10f}{:20.10f}{:20.10f}\n').format(
                atoms[i], coords[i, 0], coords[i, 1], coords[i, 2]
            )

        # Add an empty line (this marks the end of geometry section)
        input_str += '\n'

        # The keyword section starts with "*CFOUR("
        input_str += '*CFOUR('
        # Add the keywords to the input string
        for x in keywords:
            # If x is the last keyword....
            if x == keywords[-1]:
                input_str += f'{x}'
                # ... end keyword section with a closing parenthesis
                input_str += ')\n'
            else:
                input_str += f'{x}\n'

        # Add an empty line (end of ZMAT, very important since it actually does
        # not work without it)
        input_str += '\n'

        return input_str


# =====================================================================
#                         Debugging stuff
# =====================================================================

# Execute function only if the script is run directly and not imported
# if __name__ == '__main__':
#     input_file = "ZMAT"
#     # Here the function is called; it returns a dictionary which is
#     # stored in input_data
#     input_data = parse_file(input_file)

# # Print the parsed quantities
# print("Title:", input_data['title'])
# print("Geometry section:")
# for coord in input_data['geometry']:
#     print(coord)
# print(" List of Keywords:")
# for kw in input_data['keywords']:
#     print(kw)
