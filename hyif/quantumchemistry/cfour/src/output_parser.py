# =====================================================================
#                         output_parser_test.py
# =====================================================================
# This code parses a CFOUR output file which is always named
# "jobname.out" where jobname is an arbitrary name
#
# Contains the following routines:
# --------------------------------
# - parse (main routine for parsing output file)
# - extract_gradients
# - extract_coordinates
#
# General structure of a CFOUR output file:
# -----------------------------------------
# - Input
# - List of all possible keywords
# - Coordinates & information about the molecule
# - 1 & 2 electron integrals
# - SCF procedure
# - If post-HF methods: AO -> MO transformation
# - If post-HF methods: MP2, CCSD, CCSD(T) energies/ amplitudes
# - Gradients, dipole momens, etc.
# - Final electronic energy
#
#
# Phia to do:
# - consider possible errors in output file
# =====================================================================

from pathlib import Path

import numpy as np


########################################################
#             Define the class CFOURInput              #
########################################################
class CFOURoutput:
    """CFOUR output class."""

    # A class method is declared in this class and can be called as
    # CFOURoutput.name-of-function(arguments)

    # ===========================================
    #           Parse output file
    # ===========================================
    @classmethod
    def parse(cls, output_file: Path, **kwargs) -> dict:
        """Parse output file."""
        # Performing a CFOUR calculation using DERIV_LEVEL=1 produces
        # a general output file (*.out) containing energy, gradients and
        # coordinates

        # Check if the output file exists
        # if not a Path object is passed, convert it to one
        output_file = Path(output_file)
        try:
            if not output_file.exists():
                raise FileNotFoundError(
                    'could not find ' f'input file {output_file}'
                )
        except TypeError:
            raise TypeError(
                f'expected {type(Path())}, got {type(output_file)}'
            )

        # Initialize result dictionary
        result: dict = {}
        result['output_file'] = str(output_file)
        # Read lines
        result['energy'] = cls.extract_energy(output_file)

        # ----------------------------------
        # Get coordinates and gradients
        # ----------------------------------
        gradient = cls.extract_gradient(output_file)
        result['forces'] = -np.array(gradient)

        # Use the same file (GRD) to extract coordinates
        if kwargs.get('parse_coordinates', True):
            coordinates = cls.extract_coordinates(output_file)
            result['coordinates'] = np.array(coordinates)

            # Use the same file (GRD) to extract atoms
            atoms = cls.extract_atoms(output_file)
            result['atoms'] = atoms

        return result

    # ===========================================
    #       Read gradients from GRD file
    # ===========================================
    @classmethod
    def extract_gradient(cls, output_file: Path) -> np.ndarray:
        """Parse GRD file and return coordinates & gradients."""
        gradient: list = []
        with open(output_file, 'r') as f:
            lines = f.read().split('\n')
        for idx, line in enumerate(lines):
            if (
                'current gradient vector' in line
                or 'Total energy gradient' in line
            ):
                start_index = lines.index(line) + 2
                i = start_index
                while i < len(lines):
                    parts = lines[i].split()
                    if len(parts) == 3:
                        gx, gy, gz = map(float, parts[0:3])
                        gradient.append((gx, gy, gz))
                    else:
                        break
                    i += 1
                break
            elif 'Molecular gradient' in line:
                start_index = lines.index(line) + 3
                i = start_index
                while len(lines[i].split()) == 5:
                    parts = lines[i].split()
                    if len(parts) == 5:
                        gx, gy, gz = map(float, parts[2:])
                        gradient.append((gx, gy, gz))
                    else:
                        break
                    i += 1
                break

        # ravel flattens the array (all dimensions are collapsed into one)
        # -> linear format
        # reshape(-1, 3) means to reshape the array to have 3 columns and as
        # many rows as
        # needed ("-1" means as many as needed)
        return np.array(gradient).ravel().reshape(-1, 3)

    # ===========================================
    #       Read coordinates from GRD file
    # ===========================================
    @classmethod
    def extract_coordinates(cls, output_file: Path) -> np.ndarray:
        """Parse GRD file and return coordinates."""
        coordinates: list = []
        with open(output_file, 'r') as f:
            lines = f.readlines()

        for line in lines:
            if 'current coordinates' in line:
                start_index = lines.index(line) + 1
                i = start_index
                while i < len(lines):
                    parts = lines[i].split()
                    if len(parts) == 3:
                        gx, gy, gz = map(float, parts)
                        coordinates.append((gx, gy, gz))
                    else:
                        break
                    i += 1
                return np.array(coordinates).ravel().reshape(-1, 3)

        for line in lines:
            if 'Input from ZMAT file' in line:
                start_index = lines.index(line) + 2
                i = start_index
                zmat_string = ''
                while i < len(lines):
                    if '**********' in lines[i]:
                        break
                    zmat_string += lines[i]
                    i += 1
                return cls.extract_zmat_properties(zmat_string)['coordinates']

                # return np.array(coordinates).ravel().reshape(-1, 3)
        # cast error if no coordinates are found
        raise ValueError('No coordinates found in GRD file')

    @classmethod
    def extract_zmat_properties(cls, zmat_string: str) -> dict:
        """Parse ZMAT file and return coordinates."""
        # first line is comment
        results = {}
        results['comment'] = zmat_string.split('\n')[0]
        # Second until empty is atoms followed by coordinates
        atoms = []
        coordinates = []
        for line in zmat_string.split('\n')[1:]:
            parts = line.split()
            if len(parts) == 0:
                break
            atom = parts[0]
            coords = parts[1:]
            coords1 = [float(x) for x in coords]
            atoms.append(atom)
            coordinates.append(coords1)
        results['atoms'] = atoms  # type: ignore
        results['coordinates'] = np.array(coordinates, dtype=float)

        # Next look for lines starting with units=
        units = 'angstrom'
        if 'UNITS=BOHR' in zmat_string.upper():
            units = 'bohr'
        if units == 'angstrom':
            results['coordinates'] *= 0.529177  # type: ignore
        return results

    @classmethod
    def extract_energy(cls, output_file: Path) -> float:
        """Parse GRD file and return energy."""
        with open(output_file, 'r') as f:
            lines = f.readlines()
        # --------------------------------
        # Extract electronic energy
        # --------------------------------
        for line in lines:
            if (
                'The final electronic energy' in line
                or 'Total energy is' in line
            ):
                return float(line.split()[-2])

        # cast error if no energy is found
        raise ValueError('No energy found in GRD file')

    @classmethod
    def extract_atoms(cls, output_file: Path) -> list:
        """Parse GRD file and return atom labels."""
        atoms = []
        with open(output_file, 'r') as f:
            lines = f.readlines()

        for line in lines:
            if 'Input from ZMAT file' in line:
                start_index = lines.index(line) + 3
                i = start_index
                while i < len(lines):
                    parts = lines[i].split()
                    if len(parts) == 4:
                        atom = parts[0]
                        atom = atom.strip()
                        atoms.append(atom)
                    else:
                        break
                    i += 1
                return atoms

        # cast error if no atoms are found
        raise ValueError('No atoms found in GRD file')
