import copy
import os
import re
from dataclasses import replace
from typing import Optional

from hyobj import Molecule, MoleculeLike, Units
from hyset import (ComputeResult, ComputeSettings, RunSettings,
                   create_compute_settings, run_sequentially)

from ....manager import File, NewRun, PreSetup
from ....utils import unique_filename
from ...abc import HylleraasQMInterface
from .input_parser import CFOURinput
from .output_parser import CFOURoutput
from .parser import Parser as AllParser


########################################################
#             Define the class Cfour                   #
########################################################
class Cfour(HylleraasQMInterface, NewRun, PreSetup):
    """CFOUR interface."""

    # ===========================================
    #           Constructor function
    # ===========================================
    # The task of constructors is to initialize(assign values) to the
    # data members of the class when an object of the class is created
    # For example "cfour_instance = Cfour(method, compute_settings)"
    # would call this constructor function
    def __init__(
        self, method=None, compute_settings: Optional[ComputeSettings] = None
    ):

        if isinstance(method, dict):
            self.method = method  # type: ignore
            # Add some default settings if not provided
            self.method.setdefault('SYM', 'OFF')  # type: ignore
            self.method.setdefault('FIXGEOM', 'ON')  # type: ignore
            self.method.setdefault('MEM_UNIT', 'GB')  # type: ignore
            self.method.setdefault('COORDS', 'CARTESIAN')  # type: ignore
            self.input_str = method.pop('input_str', None)

        elif method is None:
            self.method = {}  # type: ignore
            self.input_str = None
        else:
            raise ValueError('Method must be a dictionary')

        # Check is the settings are specified. If not, create them
        if compute_settings is None:
            self.compute_settings = create_compute_settings()
        else:
            self.compute_settings = compute_settings

        # if getattr(self.compute_settings, 'arch_type', 'remote') == 'remote':
        #     method['MEM'] = self.compute_settings.memory

        # self.Parser is set as an instance of the AllParser class, which
        # contains the CFOURinput and CFOURoutput classes
        self.InputParser = CFOURinput
        self.OutputParser = CFOURoutput
        self.output_file_suffix = '/CFOUR_OUTPUT'
        self.input_file_suffix = None
        self.run_settings = self.compute_settings.run_settings

        if compute_settings is None:
            self.compute_settings = create_compute_settings()
        else:
            self.compute_settings = compute_settings
        self.run_settings = self.compute_settings.run_settings

        if isinstance(method, dict):
            self.units = self.method.get('units', Units())  # type: ignore
        else:
            self.units = Units()

    @run_sequentially
    def setup(self, molecule: Molecule, **kwargs) -> RunSettings:
        """Set up calculation."""
        run_settings = copy.deepcopy(self.run_settings)
        if 'run_opt' in kwargs.keys():
            run_settings = replace(run_settings, **kwargs.get('run_opt'))
        program_opt = kwargs.pop('program_opt', {})

        hash_ = program_opt.get('hash', None)
        input_str = self.gen_input(molecule, method=program_opt, **kwargs)
        if hash_ is not None:
            folder_name = hash_
        else:
            folder_name = unique_filename(input_str.split('\n'))
        sub_dir = getattr(run_settings, 'sub_dir')
        if sub_dir is not None:
            run_settings = replace(
                run_settings,
                **{
                    'sub_dir': os.path.join(
                        getattr(run_settings, 'sub_dir'), folder_name
                    )
                },
            )
        else:
            run_settings = replace(run_settings, **{'sub_dir': folder_name})

        input_file = File(name='ZMAT', content=input_str)

        if molecule is None:
            return run_settings

        output_file = File(name='CFOUR_OUTPUT')
        files_to_write = [input_file]

        files_to_parse = [output_file] + [input_file]

        running_dict = {
            'args': '',
            'stdout_file': output_file,
            'output_file': output_file,
            'files_to_write': files_to_write,
            'files_to_parse': files_to_parse,
        }

        running_dict['program'] = kwargs.get('program', 'xcfour')
        return replace(run_settings, **running_dict)

    def gen_input(self, molecule: Molecule, method={}, **kwargs):
        """Generate CFOUR input file."""
        comment_str = kwargs.get('comment', 'CFOUR INPUT')
        method = copy.deepcopy(method)
        if not method:
            method = copy.deepcopy(self.method)

        input_str = method.pop('input_str', copy.deepcopy(self.input_str))

        if input_str is not None:
            method_str = copy.deepcopy(input_str)
            method_str = method_str.upper()
            method_str = re.sub(
                r'(?:^|,)UNITS=\w+',
                lambda match: (
                    f'{match.group()[:-len(match.group().split("=")[-1])]}'
                    f'{molecule.units.length.unit.upper()}'
                ),
                method_str,
                flags=re.MULTILINE,
            )
            # every line starting with % should have the line in lowercase
            method_str = '\n'.join(
                [
                    line if not line.startswith('%') else line.lower()
                    for line in method_str.split('\n')
                ]
            )
        else:
            method['UNITS'] = molecule.units.length.unit.upper()
            method_str = (
                '*CFOUR('
                + '\n'.join(
                    [
                        f'{k.upper()}={str(v).upper()}'
                        for k, v in method.items()
                    ]
                )
                + ')'
            )
            method['UNITS'] = molecule.units.length.unit.upper()
            if 'CHARGE' not in method:
                # check if is in molecule.properties
                if 'charge' in molecule.properties:
                    method['CHARGE'] = str(molecule.properties['charge'])
            if 'MULTIPLICITY' not in method:
                # check if is in molecule.properties
                if 'spin_multiplicity' in molecule.properties:
                    method['MULTIPLICITY'] = str(
                        molecule.properties['spin_multiplicity']
                    )
                method_str = (
                    '*CFOUR('
                    + '\n'.join(
                        [
                            f'{k.upper()}={str(v).upper()}'
                            for k, v in method.items()
                        ]
                    )
                    + ')'
                )

        molecule_str = '\n'.join(molecule.xyz_string.split('\n')[2:])
        input_str = f"""{comment_str}
{molecule_str}
{method_str}

"""
        return input_str

    def author(self):
        """Return author."""
        return 'Sophia Burger, soburger@uni-mainz.de.'

    def check_version(self):
        """Check the version of CFOUR."""
        return 1

    def get_energy(self, molecule: MoleculeLike, **kwargs):
        """Compute energy."""
        result = self.run(molecule, **kwargs)
        return result['energy']

    def get_gradient(self, molecule: MoleculeLike, **kwargs):
        """Compute gradient."""
        result = self.run(molecule, **kwargs)
        return result['gradient']

    def get_hessian(self, molecule):
        """Do basically nothing."""
        return None

    def _set_parser(self) -> AllParser:
        """Set parsers for input, output, errors.

        Parameter
        ---------
        compute_settings: :obj:`ComputeSettings`

        Returns
        -------
        :obj:`Parser`

        """
        # no space allowed but also there needs to be line before return

        return AllParser(
            InputParser=CFOURinput,  # type: ignore
            OutputParser=CFOURoutput,  # type: ignore
        )

    @run_sequentially
    def parse(self, output: ComputeResult, **kwargs) -> dict:
        """Parse output."""
        outfile = output.output_file
        result = {}
        result['stdout'] = output.stdout
        result['stderr'] = output.stderr

        if outfile is not None:
            parsed_output = self.OutputParser.parse(outfile)
            if isinstance(parsed_output, dict):
                result.update(parsed_output)
            else:
                raise TypeError('Parsed output is not a dictionary')
            result['output_file'] = str(outfile)

        # if output.files_to_parse:
        #     for f in output.files_to_parse:
        #         pp = self.OutputParser
        #         try:
        #             p = Path(f.work_path_local)
        #         except AttributeError:
        #             p = Path(f)
        # g = pp.extract_gradient(p)
        # result['gradient'] = g

        # e = pp.parse(p)
        # result['energy'] = e

        return result
