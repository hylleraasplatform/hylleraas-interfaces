# ========================================================================
#                  Test if I did everything the right way
# ========================================================================
# This file contains unit tests for the functions in input_parser_test.py,
# output_parser_test.py
#
# - Execute using "pytest test.py" (note for myself: conda activate hsp)
# - The files used for testing are in the "data" folder (ZMAT, GRD,
#   jobname.out)
# ========================================================================
from pathlib import Path

import numpy as np
from hyobj import Molecule, Units

from hyif import Cfour  # type: ignore
from hyif.quantumchemistry.cfour.src.input_parser import CFOURinput
from hyif.quantumchemistry.cfour.src.output_parser import CFOURoutput


# -------------------------------------------------------
#       Tests concerning input_parser_test.py
# -------------------------------------------------------
def test_read_molecule_from_file():
    """Test the read_molecule_from_file function.

    For this, the ZMAT in the folder 'data' is used.
    """
    zmat_file = Path('data/ZMAT')

    # Call the function to test
    molecule = CFOURinput.read_molecule_from_file(zmat_file)

    # Assertions to verify the correctness of the function
    assert molecule.atoms == ['O', 'H', 'H']
    # allclose: Returns True if all elements of a given array are equal within
    # a tolerance
    assert np.allclose(
        molecule.coordinates,
        np.array(
            [
                [0.0, 0.0, 0.1287460],
                [0.0, -1.4992208, -1.0216465],
                [0.0, 1.4992208, -1.0216465],
            ]
        ),
        atol=1e-6,
    )
    assert molecule.properties['charge'] == 0
    assert molecule.units == Units('atomic')


def test_read_method_from_file():
    """Test the read_method_from_file function.

    For this, the ZMAT in the folder 'data' is used.
    """
    zmat_file = Path('data/ZMAT')

    # Call the function to test
    keywords = CFOURinput.read_keywords_from_file(zmat_file)
    assert set(keywords) == {
        'CALC=MP2',
        'BASIS=3-21G',
        'COORDS=CARTESIAN',
        'UNITS=BOHR',
        'DERIV_LEVEL=1',
        'SYM=OFF',
        'MEMORY=1',
        'MEM_UNIT=GB',
    }


def test_gen_cfour_input():
    """Test if ZMAT can be read (DISPENSIBLE).

    For this, the ZMAT in the folder 'data' is used.
    """
    zmat_file = Path('data/ZMAT')

    # Call the function to test
    molecule = CFOURinput.read_molecule_from_file(zmat_file)
    keywords = CFOURinput.read_keywords_from_file(zmat_file)

    # Generate the input string
    input_str = CFOURinput.gen_cfour_input(keywords, molecule)

    # Expected output
    expected_output = (
        'Generated ZMAT file\n'
        'O         0.0000000000       -0.0000000000        0.1287460442\n'
        'H         0.0000000000       -1.4992208474       -1.0216465729\n'
        'H         0.0000000000        1.4992208474       -1.0216465729\n'
        '\n'
        '*CFOUR(CALC=MP2\n'
        'BASIS=3-21G\n'
        'UNITS=BOHR\n'
        'DERIV_LEVEL=1\n'
        'SYM=OFF\n'
        'MEMORY=1\n'
        'COORDS=CARTESIAN\n'
        'MEM_UNIT=GB)\n'
        '\n'
    )

    # Print the generated input string and the expected output
    print('Generated input string:\n', input_str)
    print('Expected output:\n', expected_output)

    # Assert the generated input string matches the expected output
    assert input_str == expected_output


# -------------------------------------------------------
#       Tests concerning output_parser_test.py
# -------------------------------------------------------
def test_output_parser():
    """Test the OutputParser.

    For this, the Test.out file in the folder 'data' is used.
    """
    # Use the existing Test.out file from the "data" folder
    output_file = Path('data/Test.out')

    # Call the function to test
    result = CFOURoutput.parse(output_file)

    # Assertions to verify the correctness of the function
    # islcose: Returns True if two arrays are element-wise equal within a
    #  tolerance
    # while allclose checks if the entire arrays are close as a whole
    # (internally uses isclose and then applies np.all to check if all
    # elements are True)
    assert np.isclose(result['energy'], -75.70943547, atol=1e-8)
    # assert np.allclose(
    #     result['coordinates'],
    #     np.array(
    #         [
    #             [0.0, 0.0, 0.128746],
    #             [0.0, -1.499220, -1.021646],
    #             [0.0, 1.499220, -1.021646],
    #         ]
    #     ),
    #     atol=1e-6,
    # )
    #
    assert np.allclose(
        result['gradient'],
        np.array(
            [
                [0.0, 0.0146124, 1.7870115],
                [0.0, -0.5868635, -0.4670839],
                [0.0, 0.5848694, -0.4672401],
            ]
        ),
        atol=1e-6,
    )


# -------------------------------------------------------
#       Tests concerning generating ZMAT
# -------------------------------------------------------
def test_input():
    """Test if the ZMAT is generated correctly."""
    # Define the method string
    method = {
        'CALC': 'MP2',
        'BASIS': '3-21G',
        'DERIV_LEVEL': '1',
        'SYM': 'OFF',
        'MEMORY': '1',
        'COORDS': 'CARTESIAN',
        'MEM_UNIT': 'GB',
    }

    # Is this necessary?
    # comp_setting = hyset.create_compute_settings()

    # Set up the Cfour object
    mycfour = Cfour(method=method)

    # Define the molecule
    thf = Molecule(
        """O         -1.64233        1.99050       -0.00655
C         -2.77453        1.16336        0.19389
C         -2.39387       -0.23397       -0.26027
C         -0.98898       -0.26492        0.24578
C         -0.54746        1.11467       -0.20751
H         -3.66398        1.53008       -0.36059
H         -2.99928        1.15652        1.28449
H         -2.37693       -0.26726       -1.37467
H         -3.05678       -1.04086        0.11792
H         -0.36205       -1.09984       -0.13293
H         -1.00737       -0.29813        1.36016
H         -0.32320        1.09867       -1.29812
H          0.35720        1.44172        0.34719""",
        units='metal',
    )

    input_str = mycfour.gen_input(thf, method=method)
    print(input_str)

    setup = mycfour.setup(thf)
    print(setup)


# -------------------------------------------------------
#       Test run for CFOUR calculation
# -------------------------------------------------------
def test_run():
    """Test the run function of the Cfour class."""
    # Define the method string
    method = {
        'CALC': 'MP2',
        'BASIS': '3-21G',
        'DERIV_LEVEL': '1',
        'CHOLESKY': 'ON',
        'TOL_CHOL': '5',
        'SYM': 'OFF',
        'MEMORY': '1',
        'COORDS': 'CARTESIAN',
        'MEM_UNIT': 'GB',
    }

    # Is this necessary?
    # comp_setting = hyset.create_compute_settings()

    # Set up the Cfour object
    mycfour = Cfour(method=method)

    # Define the molecule
    thf = Molecule(
        """O         -1.64233        1.99050       -0.00655
C         -2.77453        1.16336        0.19389
C         -2.39387       -0.23397       -0.26027
C         -0.98898       -0.26492        0.24578
C         -0.54746        1.11467       -0.20751
H         -3.66398        1.53008       -0.36059
H         -2.99928        1.15652        1.28449
H         -2.37693       -0.26726       -1.37467
H         -3.05678       -1.04086        0.11792
H         -0.36205       -1.09984       -0.13293
H         -1.00737       -0.29813        1.36016
H         -0.32320        1.09867       -1.29812
H          0.35720        1.44172        0.34719""",
        units='metal',
    )

    # mycfour.run(THF)

    # test run
    result = mycfour.run(thf)

    # Check correctness of the result
    assert np.allclose(
        result['gradient'],
        np.array(
            [
                [0.45883619, 0.15233707, -0.25771286],
                [-0.6561135, 2.35306167, 0.74560665],
                [-0.68890606, -0.79463828, -0.79838409],
                [0.66388994, -0.42943667, 0.61394962],
                [0.15201749, 0.22916879, -0.67234497],
                [-1.01139329, 0.34003964, -0.47225194],
                [-0.32535976, -0.06974792, 1.0240733],
                [-0.05704995, -0.0170923, -0.63134434],
                [-0.64304133, -0.77205312, 0.25355556],
                [0.56544736, -0.82668896, -0.18624548],
                [0.04713059, -0.06855647, 0.68769725],
                [0.07047774, 0.01469283, -0.8698355],
                [0.98840968, 0.20743557, 0.47167898],
            ]
        ),
        atol=1e-6,
    )
    # print('Received gradient:')
    # print(gradient)

    # print('Received energy:')
    # print(result['energy'])
    assert np.isclose(result['energy'], -230.168361823723529, atol=1e-8)

    # coordinates = result['coordinates']
    # print('Received coordinates in BOHR:')
    # print(coordinates)


def test_input2():
    """Test if the ZMAT is generated correctly."""
    # Define the method string
    method = """%geo

%scf_keywords
SCF_CONV=8,SCF_MAXCYC=5000
MEMORY=5000,MEM_UNITS=MB
SCF_DAMP=900
SCF_EXPORDER=2
DERIV=1
FIXGEOM=ON
UNITS=ANGSTROM
COORD=CARTESIAN

%ccsd(t)_keywords
SCF_CONV=8,SCF_MAXCYC=5000
CC_CONV=8,LINEQ_CONV=8
MEMORY=17000,MEM_UNITS=MB
FROZEN_CORE=ON
ABCDTYPE=AOBASIS
CC_PROG=NCC
SCF_DAMP=900
COORD=CARTESIAN
DERIV=1
FIXGEOM=ON
UNITS=ANGSTROM
SCF_EXPORDER=2

%keywords
CONV=8
MAXCYC=150
COORD=CARTESIAN
FIXGEOM=ON
DERIV=1
GEO_MAXSTEP=1
UNITS=ANGSTROM

%scf_extrap
3
PVDZ
PVTZ
PVQZ

%ccsd(t)_extrap
2
PVDZ
PVTZ
"""

    mycfour = Cfour(method={'input_str': method})

    # Define the molecule
    thf = Molecule(
        """O         -1.64233        1.99050       -0.00655
C         -2.77453        1.16336        0.19389
C         -2.39387       -0.23397       -0.26027
C         -0.98898       -0.26492        0.24578
C         -0.54746        1.11467       -0.20751
H         -3.66398        1.53008       -0.36059
H         -2.99928        1.15652        1.28449
H         -2.37693       -0.26726       -1.37467
H         -3.05678       -1.04086        0.11792
H         -0.36205       -1.09984       -0.13293
H         -1.00737       -0.29813        1.36016
H         -0.32320        1.09867       -1.29812
H          0.35720        1.44172        0.34719""",
        units='metal',
    )
    assert 'ANGSTROM' in mycfour.setup(thf).files_to_write[-1].content
    thf = Molecule(
        """O         -1.64233        1.99050       -0.00655
C         -2.77453        1.16336        0.19389
C         -2.39387       -0.23397       -0.26027
C         -0.98898       -0.26492        0.24578
C         -0.54746        1.11467       -0.20751
H         -3.66398        1.53008       -0.36059
H         -2.99928        1.15652        1.28449
H         -2.37693       -0.26726       -1.37467
H         -3.05678       -1.04086        0.11792
H         -0.36205       -1.09984       -0.13293
H         -1.00737       -0.29813        1.36016
H         -0.32320        1.09867       -1.29812
H          0.35720        1.44172        0.34719""",
        units='atomic',
    )

    assert 'BOHR' in mycfour.setup(thf).files_to_write[-1].content

    mycfour = Cfour()

    assert (
        '%ccsd(t)_keywords'
        in mycfour.setup(thf, program_opt={'input_str': method})
        .files_to_write[-1]
        .content
    )
