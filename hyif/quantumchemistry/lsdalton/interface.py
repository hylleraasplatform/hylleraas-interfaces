from __future__ import annotations

from typing import Dict, List, Optional

import numpy as np
from hyset import ComputeSettings, create_compute_settings

# from ..importme import HylleraasInterface

DEFAULT_KW: List[str] = ['basis', 'qcmethod', 'properties']
IGNORE_KW: List[str] = ['program', 'molecule', '_type', 'compute_settings']

try:
    import daltonproject as dp
    dp_found = True
except Exception:
    dp_found = False


class LSDalton:
    """Dalton interface (via daltonproject)."""

    def __init__(self, method,
                 compute_settings: Optional[ComputeSettings] = None):
        if not dp_found:
            raise ImportError('daltonproject not installed')

        if compute_settings is None:
            self.compute_settings = create_compute_settings()
        else:
            self.compute_settings = compute_settings

        try:
            dp_compute_settings = LSDalton.convert_settings(compute_settings)
        except Exception:
            dp_compute_settings = None

        self.dp_compute_settings = dp_compute_settings

        self.generate_input(method)

    @classmethod
    def convert_settings(cls, compute_settings: ComputeSettings
                         ) -> dp.ComputeSettings:
        """Convert hyset to dp.ComputeSettings."""
        work_dir = str(compute_settings.work_dir)
        scratch_dir = str(compute_settings.scratch_dir)
        node_list = None
        jobs_per_node = None
        if 'DP_MPI_COMMAND' in compute_settings.env:
            mpi_command = str(compute_settings.env['DP_MPI_COMMAND'])
        else:
            mpi_command = None
        if 'MPI_NUM_PROCS' in compute_settings.env:
            mpi_num_procs = str(compute_settings.env['MPI_NUM_PROCS'])
        else:
            mpi_num_procs = None
        if 'OMP_NUM_THREADS' in compute_settings.env:
            omp_num_threads = str(compute_settings.env['OMP_NUM_THREADS'])
        else:
            omp_num_threads = None
        memory = None
        comm_port = None
        if 'LSDALTON_LAUNCHER' in compute_settings.env:
            launcher = str(compute_settings.env['LSDALTON_LAUNCHER'])
        else:
            launcher = None

        return dp.ComputeSettings(work_dir=work_dir,
                                  scratch_dir=scratch_dir,
                                  node_list=node_list,
                                  jobs_per_node=jobs_per_node,
                                  mpi_command=mpi_command,
                                  mpi_num_procs=mpi_num_procs,
                                  omp_num_threads=omp_num_threads,
                                  memory=memory,
                                  comm_port=comm_port,
                                  launcher=launcher)

    @classmethod
    def gen_molecule(cls, molecule):
        """Generate daltonproject Molecule instance."""
        fac = molecule.units.length[1]
        mol_string = ''
        for i, atom in enumerate(molecule.atoms):
            coord = np.array(molecule.coordinates).ravel().reshape(-1, 3)/fac
            mol_string += atom
            mol_string += ' {:.12f}'.format(coord[i, 0])
            mol_string += ' {:.12f}'.format(coord[i, 1])
            mol_string += ' {:.12f}'.format(coord[i, 2])
            # mol_string += ' {:.12f}'.format(molecule.coordinates[i][0])
            # mol_string += ' {:.12f}'.format(molecule.coordinates[i][1])
            # mol_string += ' {:.12f}'.format(molecule.coordinates[i][2])
            mol_string += '\n'

        charge = 0
        if hasattr(molecule, 'properties'):
            charge = molecule.properties.setdefault('charge', 0)

        return dp.Molecule(atoms=mol_string, charge=charge)

    def generate_input(self, method) -> None:
        """Generate instances for daltonproject Compute class."""
        self.dp_input = self.set_default_options(method)
        method = {} if method is None else method

        self.dp_basis = dp.Basis(self.dp_input['basis'])
        if isinstance(self.dp_input['qcmethod'], str):
            self.dp_method = dp.QCMethod(
                                 qc_method=self.dp_input['qcmethod'][0]
                                        )
        elif len(self.dp_input['qcmethod']) == 2:
            self.dp_method = dp.QCMethod(
                qc_method=self.dp_input['qcmethod'][0],
                xc_functional=self.dp_input['qcmethod'][1])
        self.dp_properties = dp.Property(
            energy=('energy' in self.dp_input['properties']),
            gradients=('gradients' in self.dp_input['properties']))

    def set_default_options(self, input_dict: dict) -> dict:
        """Set default options for daltonproject."""
        input_kw = [
            item.lower()
            for item in input_dict
            if item.lower() not in IGNORE_KW
        ]
        output_dict: dict = {}
        output_dict['basis'] = 'pcseg-1'
        output_dict['qcmethod'] = ['DFT', 'B3LYP']
        output_dict['properties'] = ['energy', 'gradients']

        for key in input_kw:
            if key in DEFAULT_KW:
                output_dict[key] = input_dict[key]
            else:
                raise NotImplementedError(
                    'could not generate interface '
                    f'{self.__class__.__name__} for keyword {key} '
                    f'available keywords are {DEFAULT_KW}')
        return output_dict

    # @classmethod
    # def get_input_molecule(cls, *args):
    #     """Generate input, in this unnecessary."""
    #     pass

    # @classmethod
    # def get_input_method(cls, *args):
    #     """Generate input, in this unnecessary."""
    #     pass

    # def update_coordinates(self, molecule) -> None:
    #     """Update coordinates on interface side."""
    #     # setattr(self.dp_molecule,'coordinates',coordinates.reshape(-1, 3))
    #     # self.dp_molecule.coordinates = coordinates.reshape(-1, 3)
    #     return

    def get_energy(self, molecule):
        """Compute energy (and gradient)."""
        self.dp_molecule = LSDalton.gen_molecule(molecule)
        # self.update_coordinates(coordinates)
        self.result = dp.lsdalton.compute(
            molecule=self.dp_molecule,
            basis=self.dp_basis,
            qc_method=self.dp_method,
            properties=self.dp_properties,
            compute_settings=self.dp_compute_settings,
        )
        return self.result.energy

    def get_gradient(self, molecule):
        """Return gradient."""
        # self.update_coordinates(coordinates)
        self.dp_molecule = LSDalton.gen_molecule(molecule)

        if 'gradients' not in self.dp_input['properties']:
            raise Exception('Gradients are not computed')
        if not hasattr(self, 'gradients'):
            # print('re-computing gradient')
            self.result = dp.lsdalton.compute(
                molecule=self.dp_molecule,
                basis=self.dp_basis,
                qc_method=self.dp_method,
                properties=self.dp_properties,
                compute_settings=self.dp_compute_settings,
            )
        return self.result.gradients

    def get_summary(self) -> Dict:
        """Extract geo_opt information from LSDalton outputfile."""
        summary_dict: Dict = {}
        # could not find this information in output-file
        summary_dict['thresh_energy'] = None
        summary_dict['steps'] = -1
        energies = []
        gradnorms = []

        with open(f'{self.result.filename}.out', 'r') as output_file:
            for line in output_file:
                if 'BFGS update' in line:
                    summary_dict['method'] = 'BFGS'
                if 'Root-mean-square gradient threshold set to' in line:
                    summary_dict['rms_grad'] = float(line.split()[6].replace(
                        'D', 'E'))
                if 'Maximum gradient element threshold set to' in line:
                    summary_dict['max_grad'] = float(line.split()[7].replace(
                        'D', 'E'))
                if 'Root-mean-square step threshold set to' in line:
                    summary_dict['rms_step'] = float(line.split()[6].replace(
                        'D', 'E'))
                if 'Maximum step element threshold set to' in line:
                    summary_dict['max_step'] = float(line.split()[7].replace(
                        'D', 'E'))
                if 'Geometry converged in ' in line:
                    summary_dict['steps'] = int(line.split()[3])

                if (str('Iter      Energy        Change       GradNorm  ' +
                        'Index   StepLen   TrustRad #Rej') in line):
                    output_file.readline()
                    # workaround for reading the optimization table summary
                    check_iter = True
                    check_en = True
                    check_line = 8
                    while check_iter and check_en and check_line == 8:
                        split_line = output_file.readline().split()
                        try:
                            int(split_line[0])
                        except Exception:
                            check_iter = False
                        else:
                            check_iter = True

                        try:
                            float(split_line[1])
                        except Exception:
                            check_en = False
                        else:
                            check_en = True

                        check_line = len(split_line)

                        if check_line == 8:
                            energies.append(float(split_line[1]))
                            gradnorms.append(float(split_line[3]))

        if len(energies) != summary_dict['steps']:
            raise Exception('LSDalton geometry output not consistent')

        if len(energies) > 0:
            summary_dict['energies'] = energies
            summary_dict['gradient_norms'] = gradnorms
            summary_dict['final_energy'] = energies[-1]
            summary_dict['final_gradient_norm'] = gradnorms[-1]
            if len(energies) > 1:
                summary_dict['final_energy_change'] = energies[-2] - \
                    energies[-1]

        return summary_dict

    @property
    def version(self):
        """Set interface version."""
        return '0.0'

    @property
    def author(self):
        """Set author's name or email adress."""
        return 'tilmann.bodenstein@kjemi.uio.no'
