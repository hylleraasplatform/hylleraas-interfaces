from typing import Any, Dict

import numpy as np


class OutputParser:
    """Parser for crest output."""

    @classmethod
    def parse(cls, output: str) -> dict:
        """Parse crest output."""
        output_dict: dict = {}
        lines = output.split('\n')
        errors = []
        chemical_shieldings: Dict[int, Any] = {}

        for i, line in enumerate(lines):
            if 'Version' in line:
                version = line.split()[1].split(',')[0]
                output_dict.update({'version': version})

            if 'ERROR' in line:
                j = i
                error_str = line
                while True:
                    j += 1
                    line_split = lines[j].split()
                    if len(line_split) == 0:
                        break
                    else:
                        error_str += lines[j]
                errors.append(error_str)

            if 'Happy end' in line:
                output_dict.update({'success': True})

            if '--- Total energy             --- =' in line:
                energy = float(line.split()[5])
                output_dict.update({'energy': energy})

            # Simen: disable due to the change from ks to mdks.
            # if 'Energy of occupied orbitals' in line:
            if False:
                alpha_orbs = {}
                beta_orbs = {}
                j = i + 2
                while True:
                    j += 1
                    line = lines[j]
                    if 'Energy of virtual orbitals' in line:
                        break
                    if '(' in line:
                        orb_start, orb_end = line.split(
                            '(')[1].split(')')[0].split('-')
                        alpha_range = list(range(int(orb_start) - 1,
                                                 int(orb_end)))
                        alpha = line.split()[2:]
                        if len(alpha) != len(alpha_range):
                            raise ValueError('Alpha orbital parsing failed'
                                             f'{alpha} {alpha_range}'
                                             )
                        alpha_orbs.update({alpha_range[i]: float(alpha[i])
                                           for i in range(len(alpha))})
                        j += 1
                        line = lines[j]
                        if line.strip() == '':
                            continue
                        beta_range = list(range(int(orb_start) - 1,
                                          int(orb_end)))
                        beta = line.split()[1:]
                        if len(beta) != len(beta_range):
                            raise ValueError('Beta orbital parsing failed'
                                             f'{beta} {beta_range}')
                        beta_orbs.update({beta_range[i]: float(beta[i])
                                          for i in range(len(beta))})

                output_dict.update({'alpha_orbs': alpha_orbs})
                output_dict.update({'beta_orbs': beta_orbs})
            if 'Final isotropic values of electric dipole--electric' in line:
                frequencies = []
                frequencies_ev = []
                real_part = []
                imag_part = []
                eas = []
                j = i + 7
                while True:
                    line = lines[j]
                    if '---' in line:
                        break
                    if len(line.split()) < 6:
                        break
                    frequencies.append(float(line.split()[0]))
                    frequencies_ev.append(float(line.split()[1]))
                    real_part.append(float(line.split()[2]))
                    imag_part.append(float(line.split()[3]))
                    eas.append(float(line.split()[5]))
                    j += 1

                eas_spectrum = np.array(list(zip(frequencies, eas)))
                output_dict.update({'eas_spectrum': eas_spectrum})
                output_dict.update({'polarizabilities_real':
                                    np.array(real_part)})
                output_dict.update({'polarizabilities_imag':
                                    np.array(imag_part)})

            if 'CHEMICAL SHIELDING FOR NUCLEUS' in line:
                no = int(line.split()[5])
                chemical_shieldings[no] = {}
                atom = line.split()[7]
                chemical_shieldings[no].update({'atom': atom})
                principal_axes = []
                principal_values = []
                dia = 0.0
                para = 0.0
                total = 0.0
                j = i
                while True:
                    j = j + 1
                    if 'SUM' in lines[j]:
                        total = float(lines[j].split()[1])
                        break
                    if 'Principal Axes' in lines[j]:
                        for k in range(3):
                            line_split = lines[j+k+2].split()
                            principal_values.append(float(line_split[1]))
                            vals = [float(x) for x in line_split[2:]]
                            principal_axes.append(vals)
                    if 'DIA' in lines[j]:
                        dia = float(lines[j].split()[1])
                    if 'PARA' in lines[j]:
                        para = float(lines[j].split()[1])
                chemical_shieldings[no].update({'principal_axes':
                                                np.array(principal_axes)})
                chemical_shieldings[no].update({'principal_values':
                                                np.array(principal_values)})

                chemical_shieldings[no].update({'dia': dia})
                chemical_shieldings[no].update({'para': para})
                chemical_shieldings[no].update({'total': total})

                output_dict.update({'chemical_shieldings':
                                    chemical_shieldings})

            if 'J-COUPLING BETWEEN NUCLEI' in line:
                line_split = line.split()
                j_couplings = {}
                j_couplings.update({'atom1': int(line_split[4])})
                j_couplings.update({'atom2': int(line_split[7])})

                principal_axes = []
                principal_values = []
                j = i
                while True:
                    j = j + 1
                    if 'Principal Axes' in lines[j]:
                        for k in range(3):
                            line_split = lines[j+k+2].split()
                            principal_values.append(float(line_split[1]))
                            vals = [float(x) for x in line_split[2:]]
                            principal_axes.append(vals)
                        break

                j_couplings.update({'principal_axes':
                                    np.array(principal_axes)})
                j_couplings.update({'principal_values':
                                    np.array(principal_values)})
                output_dict.update({'j_couplings': j_couplings})

        output_dict.update({'errors': errors})

        return output_dict
