# import copy
from copy import deepcopy
from dataclasses import replace
from pathlib import Path
# from ....manager import File
# from ....utils import unique_filename
from typing import Dict, List, Optional

# import numpy as np
import packaging.version as pv
from hyobj import Units
from hyset import (ComputeResult, ComputeSettings, RunSettings,
                   create_compute_settings, run_sequentially)

from ....manager import File, NewRun, PreSetup, Run, compute
from ....utils import unique_filename
from ...abc import HylleraasQMInterface
from ...molecule import Molecule
from .input_parser import InputParser
from .output_parser import OutputParser

VERSIONS_SUPPORTED: List[str] = ['5.2.2']

MAIN_KW = ['scf', 'cpp', 'tdscf', 'cs', 'gt', 'sscc']

DEFAULT: dict = {
    'scf': {
        'method': 'mdks/pbe0',
        'cscale': 50.0,  # Temporary to mimic non-relativisitc limit
        'charge': 0,
        'multiplicity': 1,
        'maxiterations': 30,
        'nc-model': 'point',
        'convergence': 1e-07
    },
    'cpp': {
        'property': 'eas',
        'maxiterations': 30,
        'convergence': 1e-04,
        'units': 'ev',
        'damping': 0.1,
        'frequencies': '6.0 101x0.1'
    },
    'tdscf': {
        'spectroscopy': 'eas',
        'time-steps': ' 2000 x 0.5',
        'field': {
            'model': 'delta',
            'amplitude': 0.001,
            'direction': '0.0 1.0 0.0'
        }
    },
    'cs': {
        'gauge': 'giao',
        'maxiterations': 30,
        'convergence': 1.0e-5
    },
    'sscc': {
        'print-level': 'long',
        'maxiterations': 30,
        'convergence': 1.0e-5,
        'reference-atom': 1,
        'active-atoms': 1
    },
    'gt': {
        'gauge': 'center-of-mass'
    }
}


class Respect(HylleraasQMInterface, NewRun, PreSetup):
    """Main ResPect class."""

    def __init__(self,
                 method: dict,
                 compute_settings: Optional[ComputeSettings] = None):
        """Initialise ResPect interface.

        Parameter
        ---------
        method: dict
            main input
        compute_settings: :obj:`hyset.ComputeSettings`
            compute settings

        """
        # set up computing settings
        self.method = method
        if compute_settings is None:
            self.compute_settings = create_compute_settings()
        else:
            self.compute_settings = compute_settings
        self.debug = method.get('debug', False)

        # set up default run settings
        defaults = {
            'program': 'respect',
        }
        defaults.update(method.get('run_opt', {}))
        self.compute_settings.run_settings = replace(
            self.compute_settings.run_settings, **defaults)

        self.units = self.method.get('units', Units())
        self.input = DEFAULT.copy()
        self.input.update(method.get('input', {}))
        self.input.update(method.get('template', {}))
        if 'qcmethod' in method:
            qcmethod = method['qcmethod']
            if isinstance(qcmethod, list):
                if len(qcmethod) == 1:
                    qcmethod = qcmethod[0]
                elif len(qcmethod) == 2:
                    if qcmethod[0].upper() == 'DFT':
                        qcmethod[0] = 'mdks'
                    qcmethod = '/'.join(qcmethod)
                else:
                    raise ValueError(
                        f'qcmethod must be str or list of length 1 or 2, '
                        f'got {qcmethod}')

            self.input.update({'scf': {'method': qcmethod, 'cscale': 50.0}})

        self.basis = method.get('basis', 'ucc-pvdz')
        self.properties = self._translate_properties(
                                        method.get('properties', []))

        self.OutputParser = OutputParser
        self.InputParser = InputParser()

        self.version: pv.Version = (self.check_version() if self.method.get(
            'check_version', self.compute_settings.arch_type == 'local')
            else None)

        self.units = self.method.get('units', Units())
        self.run_settings = self.compute_settings.run_settings

    def _translate_properties(self, properties: List[str]) -> Dict[str, bool]:
        """Translate properties."""
        props = {
            'energy': False,
            'gradient': False,
            'hessian': False,
            'uvvis': False,
            'nmr': False,
            'ssc': False
        }

        for prop in properties:
            if prop in props:
                props[prop] = True
            else:
                # raise ValueError(f'Property {prop} not supported')
                print('\nWARNING: Property {prop} not supported')

        props_literal = {'shieldings': 'nmr',
                         'spin-spin-coupings': 'ssc'}
        for key, value in props_literal.items():
            if key in properties:
                props[value] = True

        return props

    def _fix_units(self, input: dict):
        """Fix units."""
        if input.get('cpp', {}).get('units', None) == 'ev':
            ref = Units('metal')
            self.units.energy = ref.energy

    def check_version(self) -> pv.Version:
        """Check ReSpect version."""
        version = None
        with compute(
                method=self,  # type: ignore
                object=None,  # type: ignore
                run_opt={'args': '--version'}) as r:
            print(r)
            version = pv.parse(r['version'])
        if version not in (pv.parse(v) for v in VERSIONS_SUPPORTED):
            raise NotImplementedError(
                f' ResPect version {version} not supported.',
                f' Please contact {self.author} ',
            )

        return version

    def setup_scf(self, molecule: Molecule, **kwargs) -> RunSettings:
        """Set up ResPect calculation.

        Parameter
        ---------
        molecule: :obj:`Molecule`
            hylleraas molecule instance
        kwargs: dict
            additional keyword arguments

        Returns
        -------
        :obj:`RunSettings`
            settings for running calculation


        Notes
        -----
        self.run_settings is automatically generated by @pre_setup

        """
        run_settings = self.pre_setup(**kwargs)
        input_dict = deepcopy(self.input['scf'])
        basis = self.basis
        if 'program_opt' in kwargs:
            input_dict.update(kwargs['program_opt'])
            if 'basis' in kwargs['program_opt'].get('scf', {}):
                basis = kwargs['program_opt']['scf']['basis']
        if basis in kwargs:
            basis = kwargs['basis']

        xyz_list = [
            f'{atom} {x} {y} {z}'
            for atom, (x, y, z) in zip(molecule.atoms, molecule.coordinates)
        ]
        xyz_str = ' '.join(xyz_list)
        input_dict['geometry'] = xyz_str
        input_dict['basis'] = self._set_basis(basis, molecule)
        input_str = self.InputParser.gen_input_str({'scf': input_dict})

        filename = unique_filename(input_str.split('\n'))
        input_file = File(name=filename + '.inp', content=input_str)
        output_file = File(name=filename + '.out_scf')
        files_to_write = [input_file]

        args = ['--scf', f'--inp={filename}',
                f'--scratch=${{SCRATCH}}']  # noqa: F541
        if run_settings.cpus_per_task > 1:
            args = ['--nt', str(run_settings.cpus_per_task)] + args

        running_dict = {
            'program': 'respect',
            'args': args,
            'output_file': output_file,
            # 'stdout_redirect': File(name=filename + '.stat'),
            'files_to_write': files_to_write,
        }
        return replace(run_settings, **running_dict)

    def setup_composite(self,
                        molecule: Molecule,
                        **kwargs) -> List[RunSettings]:
        """Set up ResPect calculation of uv/vis spectra."""
        # Todo: split kwargs into scf and cpp
        if self.debug:
            print(f'\nDEBUG: kwargs: {kwargs}')
        keys = MAIN_KW.copy()
        kw_found = [k for k in keys if k in kwargs]
        keys = [k for k in kw_found if kwargs[k]]

        if self.debug:
            print(f'\nDEBUG: calculations to be performed: {keys}')
        setups: Dict[str, RunSettings] = {}
        program_opt = kwargs.pop('program_opt', {})
        for key in keys:
            opts = kwargs.pop(key+'_opt', {})
            kwargs_copy = deepcopy(kwargs)
            program_opt_copy = deepcopy(program_opt)
            program_opt_copy.update(opts)
            kwargs_copy['program_opt'] = program_opt_copy
            if key == 'scf':
                setups[key] = self.setup_scf(molecule, **kwargs)
            else:
                setups[key] = self.setup_post_scf(calc_type=key, **kwargs_copy)

        if self.debug:
            print(f'\nDEBUG: setups: {setups}')

        # construct composite input file
        input_str = ''
        for rs in setups.values():
            input_str += rs.files_to_write[0].content
            input_str += '\n'
        filename = unique_filename(input_str.split('\n'))
        input_file = File(name=filename + '.inp', content=input_str)

        # update individual run settings
        composite_run_settings = []
        for kw, rs in setups.items():
            args = rs.args
            for i, arg in enumerate(args):
                if arg.startswith('--inp'):
                    args[i] = f'--inp={filename}'
            output_file = File(name=filename + '.out_' + kw)
            files_to_write = []
            if kw == 'scf':
                files_to_write = [input_file]
            run_settings = replace(rs,
                                   output_file=output_file,
                                   files_to_write=files_to_write,
                                   args=args)
            composite_run_settings.append(run_settings)

        # return list of run_settings for consecutive run
        return composite_run_settings

    def setup_uvvis(self,
                    molecule: Molecule,
                    **kwargs) -> List[RunSettings]:
        """Set up ResPect calculation of uv/vis spectra."""
        # Todo: split kwargs into scf and cpp
        for key in MAIN_KW:
            kwargs[key] = False
        kwargs['scf'] = True
        kwargs['cpp'] = True
        return self.setup_composite(molecule,
                                    cpp_opt={'property': 'eas'},
                                    **kwargs)

    def setup_nmr(self,
                  molecule: Molecule,
                  **kwargs) -> List[RunSettings]:
        """Set up ResPect calculation of uv/vis spectra."""
        # Todo: split kwargs into scf and cpp
        for key in MAIN_KW:
            kwargs[key] = False
        kwargs['scf'] = True
        kwargs['cs'] = True
        cs_opt = {'gauge': 'giao'}
        runtime_opt = kwargs.pop('cs_opt', {})
        cs_opt.update(runtime_opt)
        return self.setup_composite(molecule,
                                    cs_opt=cs_opt,
                                    **kwargs)

    def setup_ssc(self,
                  molecule: Molecule,
                  **kwargs) -> List[RunSettings]:
        """Set up ResPect calculation of NMR shielding."""
        # Todo: split kwargs into scf and cpp
        for key in MAIN_KW:
            kwargs[key] = False
        kwargs['scf'] = True
        kwargs['sscc'] = True
        return self.setup_composite(molecule,
                                    **kwargs)

    def setup_post_scf(self, calc_type: str = None, **kwargs) -> RunSettings:
        """Set up ResPect calculation.

        Parameter
        ---------
        molecule: :obj:`Molecule`
            hylleraas molecule instance
        kwargs: dict
            additional keyword arguments

        Returns
        -------
        :obj:`RunSettings`
            settings for running calculation


        Notes
        -----
        self.run_settings is automatically generated by @pre_setup

        """
        if calc_type not in MAIN_KW:
            raise ValueError(f'calc_type must be one of {MAIN_KW}, ' +
                             f'got {calc_type}')
        run_settings = self.pre_setup(**kwargs)
        input_dict = deepcopy(self.input[calc_type])
        if 'program_opt' in kwargs:
            input_dict.update(kwargs['program_opt'])

        self._fix_units(input_dict)
        input_str = self.InputParser.gen_input_str({calc_type: input_dict})
        filename = unique_filename(input_str.split('\n'))
        input_file = File(name=filename + '.inp', content=input_str)
        output_file = File(name=filename + '.out_' + calc_type)
        files_to_write = [input_file]

        args = ['--' + calc_type, f'--inp={filename}',
                f'--scratch=${{SCRATCH}}']  # noqa: F541
        if run_settings.cpus_per_task > 1:
            args = ['--nt', str(run_settings.cpus_per_task)] + args

        scf_input_file = File(name=filename + '.50')

        files_to_send = kwargs.get('files_to_send', [scf_input_file])
        for i, f in enumerate(files_to_send):
            try:
                path = f.work_path_local
            except AttributeError:
                path = f.name
            if not Path(path).exists():
                files_to_send.pop(i)
            #     raise FileNotFoundError(f'File {path} not found')

        running_dict = {
            'program': 'respect',
            'args': args,
            'output_file': output_file,
            'files_to_write': files_to_write,
            'files_to_send': files_to_send
        }
        return replace(run_settings, **running_dict)

    def _set_basis(self, basis, molecule):
        atoms = set(molecule.atoms)
        if isinstance(basis, str):
            if '\n' in basis:
                return basis
            else:
                return {atom: basis for atom in atoms}

        elif isinstance(basis, dict):
            return basis
        else:
            raise TypeError(f'basis must be str or dict, not {type(basis)}')

    @run_sequentially
    def setup(self, molecule: Molecule, **kwargs) -> RunSettings:
        """Set up ResPect calculation.

        Parameter
        ---------
        molecule: :obj:`Molecule`
            hylleraas molecule instance
        kwargs: dict
            additional keyword arguments

        Returns
        -------
        :obj:`RunSettings`
            settings for running calculation


        Notes
        -----
        self.run_settings is automatically generated by @pre_setup

        """
        run_settings = self.pre_setup(**kwargs)

        if molecule is None:
            return run_settings

        kws = MAIN_KW.copy() + ['uvvis', 'nmr', 'ssc']
        kw_found = [k for k in kws if k in kwargs]
        kw_true = [k for k in kw_found if kwargs[k]]

        if len(kw_true) > 1:
            raise ValueError(f'Only one of {kws} can be used, got {kw_true}')
        elif len(kw_true) == 1:
            kwargs.pop(kw_true[0])
        else:
            props = self.properties.copy()
            kw_found = [k for k in kws if k in props]
            kw_true = [k for k in kw_found if props[k]]

        if len(kw_true) == 0:
            raise ValueError(f'One of {kws} must be used, got {kw_true}')
        kw = kw_true[0]
        # kwargs.pop(kw)
        post_scf = MAIN_KW.copy()
        post_scf.remove('scf')
        if self.debug:
            print(f'\nDEBUG: calculation to be performed: {kw}')
        if kw == 'scf':
            return self.setup_scf(molecule, **kwargs)
        elif kw in post_scf:
            return self.setup_post_scf(calc_type=kw, **kwargs)
        else:
            return getattr(self, 'setup_' + kw)(molecule, **kwargs)

    @run_sequentially
    def parse(self, output: ComputeResult, **kwargs) -> dict:
        """Parse ResPect output.

        Parameter
        ---------
        output: `:obj:ComputeResult`
            result of the computation
        kwargs: dict
            additional keyword arguments

        Returns
        -------
        dict
            parsed results

        """
        result = {}
        if output.output_file:
            result.update({'output_file': output.output_file})
            try:
                path = Path(output.output_file)
            except (OSError, TypeError):
                text = output.output_file
            else:
                try:
                    text = path.read_text()
                except UnicodeDecodeError:
                    text = path.read_bytes()
                except AttributeError:
                    text = str(path)

            result.update(self.OutputParser.parse(text))

        if output.stdout:
            result.update({'stdout': output.stdout})
        if output.stderr:
            result.update({'stderr': output.stderr})

        return result

    @property
    def author(self):
        """Return authors email adress."""
        return 'tilmann.bodenstein@kjemi.uio.no'

    def get_energy(self):
        """Get energy."""
        raise NotImplementedError('get_energy not implemented')

    def get_gradient(self):
        """Get gradient."""
        raise NotImplementedError('get_gradient not implemented')

    def get_hessian(self):
        """Get hessian."""
        raise NotImplementedError('get_hessian not implemented')


class ResPect(HylleraasQMInterface, Run, PreSetup):
    """Main ResPect class."""

    def __init__(self, method, compute_settings):
        """Initialise ResPect interface."""
        self = Respect(method, compute_settings)  # # noqa: F841


# if __name__ == '__main__':

#     # c = Respect({})
#     # print(c.version)
#     from hyobj import Molecule
#     mol = Molecule('S')
#     c = Respect({'check_version': False})
#     a = c.setup_scf(mol)
#     print(a)
