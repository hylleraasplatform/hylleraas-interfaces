import os

import ase.io
import ase.io.vasp
import numpy as np
from hyobj import DataSet, Molecule, PeriodicSystem
from hyset import RemoteArch

from hyif import VASP, DeepMD


def test_launcher_hierarchy():
    """Test if executable and launcher are set properly."""
    my_vasp = VASP({})
    settings = my_vasp.compute_settings
    program, launcher = my_vasp._set_executable(settings)
    assert program == 'vasp_gpu' and launcher == 'mpirun -np 1'

    my_vasp = VASP({'run_opt': {'program': 'vasp_mpi'}})
    settings = my_vasp.compute_settings
    program, launcher = my_vasp._set_executable(settings)
    assert program == 'vasp_mpi' and launcher == 'mpirun -np 1'

    my_vasp = VASP({'run_opt': {'launcher': 'mpirun -np 4'}})
    settings = my_vasp.compute_settings
    program, launcher = my_vasp._set_executable(settings)
    assert program == 'vasp_gpu' and launcher == 'mpirun -np 4'

    my_vasp = VASP(
        {'run_opt': {'program': 'vasp_mpi', 'launcher': 'mpirun -np 4'}}
    )
    settings = my_vasp.compute_settings
    program, launcher = my_vasp._set_executable(settings)
    assert program == 'vasp_mpi' and launcher == 'mpirun -np 4'

    my_arch = RemoteArch(target='betzy', user='test')
    my_vasp = VASP(
        {'run_opt': {'program': 'vasp_mpi', 'launcher': 'mpirun -np 4'}},
        compute_settings=my_arch,
    )
    settings = my_vasp.compute_settings
    program, launcher = my_vasp._set_executable(settings)
    assert program == 'vasp_mpi' and launcher == 'mpirun -np 4'

    my_arch = RemoteArch(
        target='betzy', user='test', ntasks=8, launcher='mpirun -np 8'
    )
    my_vasp = VASP(
        {'run_opt': {'program': 'vasp_mpi'}}, compute_settings=my_arch
    )
    settings = my_vasp.compute_settings
    program, launcher = my_vasp._set_executable(settings)
    assert program == 'vasp_mpi' and launcher == 'mpirun -np 8'


def test_read_oszicar():
    """Test if OSZICAR can be read."""
    oszicar_path = (
        os.path.dirname(os.path.realpath(__file__))
        + '/files_test_is_converged/single_point/OSZICAR'
    )
    my_vasp = VASP({})
    my_vasp.OutputParser.parse_oszicar(oszicar_path)


def test_is_converged():
    """Test is convergence check can be done."""
    vasprun_path = (
        os.path.dirname(os.path.realpath(__file__))
        + '/files_test_is_converged/single_point/vasprun.xml'
    )
    my_vasp = VASP({})
    result = my_vasp.OutputParser.parse(vasprun_path)
    assert result['is_converged'][0]


def test_not_converged():
    """Test if non-convergence is picked up."""
    vasprun_path = (
        os.path.dirname(os.path.realpath(__file__))
        + '/files_test_not_converged/vasprun.xml'
    )
    my_vasp = VASP({})
    result = my_vasp.OutputParser.parse(vasprun_path)
    print(result)
    assert not result['is_converged'][0]


def test_wildcard_in_vasprun():
    """Check that energy becomes nan when vasprun full of stars."""
    vasprun_path = (
        os.path.dirname(os.path.realpath(__file__))
        + '/files_test_stars/vasprun.xml'
    )
    my_vasp = VASP({})
    result = my_vasp.OutputParser.parse(vasprun_path)
    print(result)
    assert False


def test_ase_vaspio():
    """Check if the parser reads the same from OUTCAR and vasprun.xml files."""
    path = os.path.dirname(os.path.realpath(__file__))

    data_xml = ase.io.read(path + '/outcar_vasprun_xml/vasprun.xml', index=':')
    data_outcar = ase.io.read(path + '/outcar_vasprun_xml/OUTCAR', index=':')

    for i in range(10):
        np.testing.assert_almost_equal(
            data_xml[i].get_positions(),
            data_outcar[i].get_positions(),
            decimal=5,
        )


def test_outcar_vs_vasprun_xml():
    """Test VASP output."""
    myvasp = VASP({'options': {'EDIFF': 1e-6, 'ALGO': 'Normal'}})
    path_outcar = (
        os.path.dirname(os.path.realpath(__file__))
        + '/outcar_vasprun_xml/OUTCAR'
    )
    path_xml = (
        os.path.dirname(os.path.realpath(__file__))
        + '/outcar_vasprun_xml/vasprun.xml'
    )
    results_outcar = myvasp.OutputParser.parse(path_outcar)
    results_xml = myvasp.OutputParser.parse(path_xml)

    ds_outcar = DataSet(results_outcar)
    ds_xml = DataSet(results_xml)

    for i in range(len(ds_outcar)):
        assert ds_outcar[i]['atoms'] == ds_xml[i]['atoms']
        np.testing.assert_almost_equal(
            ds_outcar[i]['coordinates'], ds_xml[i]['coordinates'], decimal=4
        )
        np.testing.assert_almost_equal(
            ds_outcar[i]['forces'], ds_xml[i]['forces'], decimal=4
        )
        np.testing.assert_almost_equal(
            ds_outcar[i]['box'], ds_xml[i]['box'], decimal=4
        )
        np.testing.assert_almost_equal(
            ds_outcar[i]['energy'], ds_xml[i]['energy'], decimal=4
        )
        np.testing.assert_almost_equal(
            ds_outcar[i]['virials'], ds_xml[i]['virials'], decimal=4
        )


def test_input():
    """Test VASP input."""
    _ = VASP({'options': {'EDIFF': 1e-6, 'ALGO': 'Normal'}})


def test_output():
    """Test VASP output."""
    myvasp = VASP({'options': {'EDIFF': 1e-6, 'ALGO': 'Normal'}})
    path = os.path.dirname(os.path.realpath(__file__)) + '/test.OUTCAR'
    results = myvasp.OutputParser.parse(path)
    np.testing.assert_almost_equal(results['energy'][0], -2210.71850908)
    np.testing.assert_almost_equal(results['forces'][0, -2], -2.109082)


def test_output_vasprun():
    """Test VASP output."""
    myvasp = VASP({'options': {'EDIFF': 1e-6, 'ALGO': 'Normal'}})
    path = os.path.dirname(os.path.realpath(__file__)) + '/test.vasprun.xml'
    results = myvasp.OutputParser.parse(path)
    print(results)
    np.testing.assert_almost_equal(results['energy'][0], -14.07479926)
    np.testing.assert_almost_equal(results['forces'][0, 3], -0.36075540)


def test_incar():
    """Test reading of VASP INCAR."""
    incar_path = os.path.dirname(os.path.realpath(__file__)) + '/test.INCAR'
    myvasp = VASP(
        {
            'incar_string': open(incar_path).read(),
            'incar_options': {'EDIFF': 1e-6, 'ALGO': 'Normal'},
            'kpoints': {
                'string': 'Regular 2 x 2 x 2 mesh centered at Gamma\n0\nGamma\n\
2 2 2'
            },
        }
    )

    correct_incar_dict = {
        'SYSTEM': 'mySystem',
        'ALGO': 'Normal',
        'IBRION': 1,
        'EDIFF': 1e-6,
    }
    correct_string = '\n'.join(
        [f'{key} = {value}' for key, value in correct_incar_dict.items()]
    )
    assert correct_string == myvasp.incar_string


def test_potcar():
    """Test POTCAR concatenation."""
    vasp_potcar_path = (
        os.path.dirname(os.path.realpath(__file__)) + '/potpaw_PBE.54'
    )

    myvasp = VASP(
        {
            'incar_options': {'EDIFF': 1e-6, 'ALGO': 'Normal'},
            'kpoints': {
                'string': 'Regular 2 x 2 x 2 mesh centered at Gamma\n0\nGamma\n\
2 2 2'
            },
            'potential_specification': {
                'path': vasp_potcar_path,
                'species_mapping': {'O': 'O_h'},
            },
        }
    )
    mol_str = """ O   0.0000   0.0000   0.0626
 H  -0.7920   0.0000  -0.4973
 H   0.7920   0.0000  -0.4973"""
    mymol1 = PeriodicSystem(
        Molecule(mol_str, units='angstroms'),
        pbc=[[5, 0, 0], [0, 5, 0], [0, 0, 5]],
    )
    potcar_string = myvasp._get_potcar_string(mymol1)
    print(potcar_string)


def test_poscar_string():
    """Test generation of POSCAR."""
    mol_str = """ O   1.0000   0.0000   1.0626
 H   0.2080   0.0000  0.50270
 H   1.7920   0.0000  0.50270"""
    mymol1 = PeriodicSystem(
        Molecule(mol_str, units='angstroms'),
        pbc=[[5, 0, 0], [0, 5, 0], [0, 0, 5]],
    )
    myvasp = VASP({'options': {'EDIFF': 1e-6, 'ALGO': 'Normal'}})
    result, outcar_reorder = myvasp._get_poscar_string(mymol1)
    expected_output = """Autogenerated by Hylleraas Software Platform \
VASP interface
1.0
5.0000000000000000 0.0000000000000000 0.0000000000000000
0.0000000000000000 5.0000000000000000 0.0000000000000000
0.0000000000000000 0.0000000000000000 5.0000000000000000
H O
2 1
Cartesian
0.20800000429153442 0.0 0.5026999711990356
1.7920000553131104 0.0 0.5026999711990356
1.0 0.0 1.062600016593933
"""
    assert result == expected_output


def test_kpoints():
    """Test VASP KPOINTS generation."""
    myvasp = VASP(
        {
            'incar_options': {'EDIFF': 1e-6, 'ALGO': 'Normal'},
            'kpoints': {
                'string': 'Regular 2 x 2 x 2 mesh centered at Gamma\n0\nGamma\n\
2 2 2'
            },
        }
    )
    assert (
        myvasp.kpoints_string
        == 'Regular 2 x 2 x 2 mesh centered at \
Gamma\n0\nGamma\n2 2 2'
    )


def test_kpoints_1():
    """Test VASP KPOINTS generation."""
    myvasp = VASP(
        {'incar_options': {'EDIFF': 1e-6, 'ALGO': 'Normal'}, 'kpoints': {}}
    )
    assert (
        myvasp.kpoints_string
        == 'Regular 1 x 1 x 1 mesh centered at \
Gamma\n0\nGamma\n1 1 1'
    )


def test_integration():
    """Test VASP caculation."""
    vasp_potcar_path = (
        os.path.dirname(os.path.realpath(__file__)) + '/potpaw_PBE.54'
    )

    myvasp = VASP(
        {
            'executable': 'vasp_gpu',
            'incar_options': {'EDIFF': 1e-6, 'ALGO': 'Normal'},
            'kpoints': {
                'string': 'Regular 1 x 1 x 1 mesh centered at Gamma\n0\nGamma\n\
1 1 1'
            },
            'potential_specification': {
                'path': vasp_potcar_path,
                'species_mapping': {'O': 'O_h'},
            },
        }
    )
    mol_str = """ O   0.0000   0.0000   0.0626
 H  -0.7920   0.0000  -0.4973
 H   0.7920   0.0000  -0.4973"""
    mymol1 = PeriodicSystem(
        Molecule(mol_str, properties={'unit': 'angstrom'}),
        pbc=[[5, 0, 0], [0, 5, 0], [0, 0, 5]],
    )

    mol_str = """ O   0.0000   1.0000   0.0626
 H  -0.7920   1.0000  -0.4973
 H   0.7920   1.0000  -0.4973"""
    mymol2 = PeriodicSystem(
        Molecule(mol_str, properties={'unit': 'angstrom'}),
        pbc=[[5, 0, 0], [0, 5, 0], [0, 0, 5]],
    )

    result1 = myvasp.run(mymol1, run_opt={'sub_dir': 'test_sub_dir'})
    result2 = myvasp.run(mymol2)

    np.testing.assert_almost_equal(
        result1['energy'][0], result2['energy'][0], decimal=3
    )


def test_particle_order():
    """Test VASP caculation."""
    vasp_potcar_path = (
        os.path.dirname(os.path.realpath(__file__)) + '/potpaw_PBE.54'
    )

    myvasp = VASP(
        {
            'executable': 'vasp_gpu',
            'incar_options': {'EDIFF': 1e-6, 'ALGO': 'Fast'},
            'kpoints': {
                'string': 'Regular 1 x 1 x 1 mesh centered at Gamma\n0\nGamma\n\
1 1 1'
            },
            'potential_specification': {
                'path': vasp_potcar_path,
                'species_mapping': {'O': 'O_h'},
            },
        }
    )
    mol_str = """ O   0.0000   0.0000   0.0626
 H  -0.7920   0.0000  -0.4973
 H   0.7920   0.0000  -0.4973"""
    mymol1 = PeriodicSystem(
        Molecule(mol_str, properties={'unit': 'angstrom'}),
        pbc=[[5, 0, 0], [0, 5, 0], [0, 0, 5]],
    )

    mol_str = """H  -0.7920   0.0000  -0.4973
 O   0.0000   0.0000   0.0626
 H   0.7920   0.0000  -0.4973"""
    mymol2 = PeriodicSystem(
        Molecule(mol_str, properties={'unit': 'angstrom'}),
        pbc=[[5, 0, 0], [0, 5, 0], [0, 0, 5]],
    )

    result1 = myvasp.run(mymol1)
    result2 = myvasp.run(mymol2)

    np.testing.assert_almost_equal(
        result1['energy'][0], result2['energy'][0], decimal=5
    )
    np.testing.assert_almost_equal(
        result1['forces'][0, 0:3], result2['forces'][0, 3:6], decimal=3
    )
    np.testing.assert_almost_equal(
        result2['forces'][0, 3:6], result1['forces'][0, 0:3], decimal=3
    )
    np.testing.assert_almost_equal(
        result1['forces'][0, 6:9], result2['forces'][0, 6:9], decimal=3
    )


def test_vasp_deepmd_data_integration():
    """Check compatibility vasp output with deepmd interface."""
    vasp_potcar_path = (
        os.path.dirname(os.path.realpath(__file__)) + '/potpaw_PBE.54'
    )

    myvasp = VASP(
        {
            'executable': 'vasp_gpu',
            'incar_options': {
                'EDIFF': 1e-4,
                'ALGO': 'Normal',
                'IBRION': 0,
                'ISIF': 2,
                'POTIM': 0.1,
                'NSW': 10,
            },
            'kpoints': {
                'string': 'Regular 1 x 1 x 1 mesh centered at Gamma\n0\nGamma\n\
1 1 1'
            },
            'potential_specification': {
                'path': vasp_potcar_path,
                'species_mapping': {'O': 'O_h'},
            },
        }
    )
    mol_str = """ O   0.0000   0.0000   0.0626
 H  -0.7920   0.0000  -0.4973
 H   0.7920   0.0000  -0.4973"""
    mymol = PeriodicSystem(
        Molecule(mol_str, properties={'unit': 'angstrom'}),
        pbc=[[5, 0, 0], [0, 5, 0], [0, 0, 5]],
    )

    result = myvasp.run(mymol, run_opt={'sub_dir': 'deepmd_sub_dir'})
    print(result)
    ds = DataSet(result)

    mydeepmd = DeepMD({'check_version': False})

    my_data = mydeepmd.get_data(ds)
    np.testing.assert_equal(my_data.nframes, 10)
