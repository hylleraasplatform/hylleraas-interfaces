import os
import pathlib
import re
from typing import Any, List, Union

import ase.io
import numpy as np

from ...abc import Parser

ERROR_EXCEPTIONS: list = []
WARNING_EXCEPTIONS: list = []


def float_or_none(number) -> Union[float, None]:
    """Convert to float or None."""
    if number is None:
        return None
    else:
        return float(number)


class OutputParser(Parser):
    """VASP output parser."""

    @classmethod
    def parse(cls, outpath, reorder_if_possible=True, stress=False):
        """Parse from OUTCAR file at path outpath."""
        outpath = pathlib.Path(outpath)
        result: dict = {}
        # errors: dict = {}
        # warnings: dict = {}

        if 'OUTCAR' in str(outpath) or '.xml' in str(outpath):
            try:
                frames = ase.io.read(outpath, index=':')
            except Exception as e:
                print(f'Failed to read VASP output file {outpath}: ', e)
                frames = []
        else:
            raise Exception(
                f'Filename of VASP output contains neither OUTCAR \
nor xml: {str(outpath)}'
            )

        outcar_reorder = None
        if reorder_if_possible:
            reorder_filename = os.path.join(
                os.path.dirname(outpath), 'outcar_reorder_indices.txt'
            )
            if os.path.isfile(reorder_filename):
                with open(reorder_filename, 'r') as ifile:
                    content = ifile.read()
                    content = content.replace('[', '').replace(']', '')
                    outcar_reorder = np.fromstring(content, dtype=int, sep=' ')

        forces = []
        virials = []
        boxes = []
        energy = []
        positions = []
        chemical_symbols = []
        for frame in frames:
            try:
                forces.append(frame.get_forces())
            except Exception as e:
                print(
                    'Could not get forces from VASP interface.'
                    + 'Will return empty VASP simulation from here on',
                    e,
                )
                break
            try:
                virials.append(
                    -frame.get_stress(voigt=False) * frame.get_volume()
                )  # Virial in eV
            except Exception as e:
                print(e)
            boxes.append(frame.get_cell(complete=True))
            energy.append(frame.get_potential_energy())
            positions.append(frame.get_positions())
            chemical_symbols.append(frame.get_chemical_symbols())

        has_virial = len(virials) > 0

        result['atoms'] = np.array(chemical_symbols)
        result['coordinates'] = np.array(positions)
        result['forces'] = np.array(forces)
        if has_virial:
            result['virial'] = np.array(virials)
        result['energy'] = np.array(energy)
        result['box'] = np.array(boxes)

        if outcar_reorder is not None:
            for i in range(len(result['forces'])):
                result['forces'][i, :, :] = result['forces'][
                    i, outcar_reorder, :
                ]
                result['coordinates'][i, :, :] = result['coordinates'][
                    i, outcar_reorder, :
                ]
                result['atoms'][i, :] = result['atoms'][i, outcar_reorder]

        num_frames = result['coordinates'].shape[0]

        if num_frames > 0:
            result['coordinates'] = result['coordinates'].reshape(
                (num_frames, -1)
            )
            result['forces'] = result['forces'].reshape((num_frames, -1))
            result['virial'] = result['virial'].reshape((num_frames, -1))
            result['box'] = result['box'].reshape((num_frames, -1))

        oszicar_path = outpath.with_name('OSZICAR')
        if os.path.isfile(oszicar_path):
            try:
                oszicar_contents = cls.parse_oszicar(oszicar_path)
            except Exception as e:
                print(e)
                result['is_converged'] = False
            incar_path = outpath.with_name('INCAR')
            if os.path.isfile(incar_path):
                incar_contents = cls.parse_incar(incar_path)
                if 'EDIFF' in incar_contents.keys():
                    ediff = float(incar_contents['EDIFF'])
                else:
                    ediff = 1e-4
                final_ediffs = np.array(
                    [entry['dE'][-1] for entry in oszicar_contents]
                )
                is_converged = np.abs(final_ediffs) < np.abs(ediff)
                result['is_converged'] = is_converged

        sum_len = sum([len(v) for v in result.values()])
        if sum_len == 0:
            for v in result.keys():
                result[v] = np.nan
            result['is_converged'] = False
        return result

    @classmethod
    def parse_stdout(cls, stdout) -> dict:
        """Parse stdout."""
        results = {}
        results['stdout'] = stdout
        return results

    @classmethod
    def parse_oszicar(cls, path) -> list:
        """Parse OSZICAR."""
        results = []
        # First find start of step
        with open(path, 'r') as ifile:
            lines = ifile.readlines()
        linecount = 0
        while True:
            if linecount >= len(lines):
                break
            metric_names = ['method'] + re.findall(
                r'\S+(?:\s\S+)?', lines[linecount]
            )

            data_types: List[Any] = []
            for name in metric_names:
                if name == 'method':
                    data_types.append(str)
                elif name in ['N', 'ncg']:
                    data_types.append(int)
                else:
                    data_types.append(float)

            step_convergence_metrics: dict[str, list] = {}
            for name in metric_names:
                step_convergence_metrics[name] = []

            while True:
                linecount += 1
                if linecount >= len(lines):
                    break

                def type_to_pattern(type):
                    str_pattern = r'(\S+)\s+'
                    int_pattern = r'([-+]?\d+)\s+'
                    float_pattern = r'([-+]?\d+\.\d+(?:[eE][-+]\d+)?)?\s*'
                    if type == int:
                        return int_pattern
                    elif type == str:
                        return str_pattern
                    elif type == float:
                        return float_pattern
                    else:
                        raise TypeError('only supports int, str and float')

                total_pattern = ''.join(map(type_to_pattern, data_types))
                match_groups = re.match(total_pattern, lines[linecount])
                if match_groups is None:
                    break

                for name, value, data_type in zip(
                    metric_names, match_groups.groups(), data_types
                ):
                    if value is not None:
                        step_convergence_metrics[name].append(data_type(value))
                    else:
                        step_convergence_metrics[name].append(value)

            # Look for the final line Two options, depending on MD or not
            data_types = [int, float, float, float]
            metric_names = ['Step', 'F', 'E0', 'dE_final']  # not MD
            if linecount >= len(lines):
                break

            match_groups = re.match(
                '\s*(\d+)\s+F=\s*([-+]?\d*\.?\d+(?:[eE][-+]?\d+))\s+E0\s*=\s*([-+]?\d*\.?\d+(?:[eE][-+]?\d+))\s+d\sE\s*=\s*([-+]?\d*\.?\d+(?:[eE][-+]?\d+))',  # noqa: W605 E501
                lines[linecount],
            )

            # Special match group for case where last dE has 3 digit exponent
            if match_groups is None:
                match_groups = re.match(
                    '\s*(\d+)\s+F=\s*([-+]?\d*\.?\d+(?:[eE][-+]?\d+))\s+E0\s*=\s*([-+]?\d*\.?\d+(?:[eE][-+]?\d+))\s+d\sE\s*=\s*([-+]?\d*\.?\d+(?:[-+]?\d+))',  # noqa: W605 E501
                    lines[linecount],
                )  # noqa: W605 E501

            if match_groups is None:
                data_types = [
                    int,
                    float,
                    float,
                    float,
                    float,
                    float,
                    float,
                    float,
                ]
                metric_names = ['Step', 'T', 'E', 'F', 'E0', 'EK', 'SP', 'SK']
                pattern = r'^\s*(\d+)\s+T=\s+(\d+)\.\s+E=\s+([-+]?\d*\.\d+E?[+-]?\d*)\s+F=\s+([-+]?\d*\.\d+E?[+-]?\d*)\s+E0=\s+([-+]?\d*\.\d+E?[+-]?\d*)\s+EK=\s+([-+]?\d*\.\d+E?[+-]?\d*)\s+SP=\s+([-+]?\d*\.\d+E?[+-]?\d*)\s+SK=\s+([-+]?\d*\.\d+E?[+-]?\d*).*'  # noqa: W605 E501
                match_groups = re.match(pattern, lines[linecount])

            if match_groups is None:
                break

            for name, value, data_type in zip(
                metric_names, match_groups.groups(), data_types
            ):

                # Support for missing "E" in before three digit exponents
                special_match = re.match(
                    '([+-]?\d*\.\d*)([+-]\d+)', value  # noqa: W605 E501
                )  # noqa: W605 E501
                if special_match is not None:
                    value = (
                        special_match.groups()[0]
                        + 'E'
                        + special_match.groups()[1]
                    )
                step_convergence_metrics[name] = data_type(value)
            linecount += 1
            results.append(step_convergence_metrics)
        return results

    @classmethod
    def parse_incar(cls, path) -> dict:
        """Parse the contents of a VASP INCAR file."""
        with open(path, 'r') as ifile:
            incar_content = ifile.read()

        # Regular expression to match INCAR parameters (key-value pairs)
        pattern = re.compile(r'\b([^=;\s]+)\s*=\s*([^!;#\n]+)')

        # Dictionary to store parsed parameters
        params = {}

        # Find all matches and construct a dictionary
        for line in incar_content.splitlines():
            for match in pattern.finditer(line):
                key = match.group(1).strip()
                value = match.group(2).strip()
                params[key] = value
        return params
