import copy
import os
import re
from dataclasses import replace
from typing import Any, Optional, Tuple

import numpy as np
import packaging.version as pv
from hyobj import PeriodicSystem, Units
from hyset import (ComputeResult, ComputeSettings, RunSettings,
                   create_compute_settings, run_sequentially)

from ....manager import File, NewRun
from ....utils import unique_filename
from ...abc import HylleraasQMInterface, Runner
from .output_parser import OutputParser

GAMMA_SINGLE = """Regular 1 x 1 x 1 mesh centered at Gamma
0
Gamma
1 1 1"""


class VASP(HylleraasQMInterface, NewRun):
    """VASP interface."""

    def __init__(
        self, method: dict, compute_settings: Optional[ComputeSettings] = None
    ):
        """VASP interface contructor.

        This constructor prepares the VASP interface for running several
        molecules using the same VASP input script and kpoints settings.

        Method need to contain.
        method['incar_file'] and/or method['incar_options'];
            method['incar_options'] overrides options in method['incar_file']
            method['pre_converge_incar_options'] sets up an initial run with
                the settings provided here.
                The WAVECAR from this run will be used for the wave function
                of the main run to be closer to convergence. We have
                implemented this functionality to run PBE before PBE0.
        method['potential_specification']
            ['species_mapping']: dict, maps elements to the actual potential,
                e.g. {"O" : "O_h"} for using a hard oxygen potential.
                In the absence of mappings, the potential will default to
                that with the same name as the element.
            ['path']: path to VASP potential folder
        method['kpoints'] None or dict
            ['file'] path
            ['string'] file contents
        method['run_opt']
        """
        if compute_settings is None:
            self.compute_settings = create_compute_settings()
        else:
            self.compute_settings = compute_settings

        self.output_file_suffix = '/vasprun.xml'
        self.input_file_suffix = None
        self.incar_string = self._get_incar_string(method)
        self.pre_converge_incar_string = self._get_pre_converge_incar_string(
            method
        )
        self.encut_ladder = method.get('encut_ladder', None)
        self.kpoints_string = self._get_kpoints_string(method)
        self.potential_specification = method.get('potential_specification')

        self.Runner: Runner = self.compute_settings.Runner

        self.OutputParser = OutputParser
        self.method = method
        self.units = Units('metal')

        self.run_settings = self.compute_settings.run_settings

    def check_version(self) -> pv.Version:
        """Check if program version is compatible with interface.

        Returns
        -------
        :obj:`packaging.Version'
            version object

        """
        version: pv.Version = None
        return version

    @run_sequentially
    def setup(self, molecule: PeriodicSystem, **kwargs) -> RunSettings:
        """Set up VASP simulation and return run_settings.

        Create run-settings to be used by the Runner that runs the
        simulation.
        """
        run_settings = copy.deepcopy(self.run_settings)
        if 'run_opt' in kwargs.keys():
            run_settings = replace(run_settings, **kwargs.get('run_opt'))

        program, launcher = self._set_executable(run_settings)
        setup_dict = {'program': program, 'launcher': launcher}

        # What is this doing?
        if molecule is None:
            return replace(run_settings, **setup_dict)

        poscar_string, outcar_reorder = self._get_poscar_string(
            molecule=molecule
        )
        reorder_string = np.array2string(outcar_reorder)
        potcar_string = self._get_potcar_string(molecule=molecule)
        kpoints_string = self._get_kpoints_string(self.method)

        folder_name = unique_filename(
            [
                self.incar_string,
                poscar_string,
                potcar_string,
                self.kpoints_string,
                str(self.pre_converge_incar_string),
                reorder_string,
            ]
        )

        sub_dir = getattr(run_settings, 'sub_dir')
        if sub_dir is not None:
            run_settings = replace(
                run_settings,
                **{
                    'sub_dir': os.path.join(
                        getattr(run_settings, 'sub_dir'), folder_name
                    )
                },
            )
        else:
            run_settings = replace(run_settings, **{'sub_dir': folder_name})

        if self.pre_converge_incar_string is not None:
            incar_file = File(name='main.INCAR', content=self.incar_string)
        else:
            incar_file = File(name='INCAR', content=self.incar_string)
        kpoints_file = File(name='KPOINTS', content=kpoints_string)
        poscar_file = File(name='POSCAR', content=poscar_string)
        potcar_file = File(name='POTCAR', content=potcar_string)
        reorder_file = File(
            name='outcar_reorder_indices.txt', content=reorder_string
        )
        outcar_file = File(name='vasprun.xml')
        stdout_file = File(name='stdout.out')
        files_to_write = [
            incar_file,
            kpoints_file,
            poscar_file,
            potcar_file,
            reorder_file,
        ]

        setup_dict.update(
            {
                'files_to_write': files_to_write,
                'output_file': outcar_file,
                'stdout_file': stdout_file,
            }
        )

        if (
            self.pre_converge_incar_string is not None
            and self.encut_ladder is None
        ):
            setup_dict.update({'post_cmd': 'rm WAVECAR'})

        return replace(run_settings, **setup_dict)

    def setup_pre_converge(self, molecule: PeriodicSystem, **kwargs):
        """Set up run settings for a VASP run that will run before main run."""
        run_settings = copy.deepcopy(self.setup(molecule, **kwargs))

        incar_file = File(name='INCAR', content=self.pre_converge_incar_string)
        outcar_file = File(
            name='pre_converge_vasprun.xml',
            content=self.pre_converge_incar_string,
        )
        stdout_pre_converge = File(name='stdout.out')
        files_to_write = getattr(run_settings, 'files_to_write')
        files_to_write.append(incar_file)
        return replace(
            run_settings,
            **{
                'files_to_write': files_to_write,
                'output_file': outcar_file,
                'stdout_file': stdout_pre_converge,
                'post_cmd': [
                    'mv INCAR pre_converge_INCAR',
                    'mv main.INCAR INCAR',
                    'mv vasprun.xml pre_converge_vasprun.xml',
                    'mv OUTCAR pre_converge_OUTCAR',
                    'mv OSZICAR pre_converge_OSZICAR',
                ],
            },
        )

    def setup_encut_ladder(self, molecule: PeriodicSystem, **kwargs):
        """Set up run settings for a VASP run that will run before main run."""
        encut_ladder = self.encut_ladder
        ladder_operations = []
        run_settings = copy.deepcopy(self.setup(molecule, **kwargs))
        for i, encut in enumerate(encut_ladder):
            incar_string = re.sub(
                r'^ENCUT\s*=\s*\d+',
                f'ENCUT = {encut}',
                self.incar_string,
                flags=re.MULTILINE,
            )
            incar_file = File(name=f'INCAR_{encut}', content=incar_string)
            post_cmd = [
                f'cp INCAR encut_{encut}/INCAR',
                f'cp vasprun.xml encut_{encut}/vasprun.xml',
                f'cp OUTCAR encut_{encut}/OUTCAR',
                f'cp OSZICAR encut_{encut}/OSZICAR',
            ]
            if i == len(encut_ladder) - 1:
                post_cmd += ['rm WAVECAR']
            ladder_operations.append(
                replace(
                    run_settings,
                    **{
                        'files_to_write': [incar_file],
                        'output_file': f'encut_{encut}/vasprun.xml',
                        'stdout_file': 'stdout.out',
                        'pre_cmd': [
                            f'mkdir encut_{encut}',
                            f'mv INCAR_{encut} INCAR',
                        ],
                        'post_cmd': post_cmd,
                    },
                )
            )
        return ladder_operations

    @property
    def author(self):
        """Return who wrote this interface."""
        return 'henriasv@uio.no'

    @run_sequentially
    def get_energy(self, molecule: PeriodicSystem):
        """Compute energy."""
        result = self.run(molecule)
        if len(result['energies']) == 1:
            return result['energies'][0]

    @run_sequentially
    def get_gradient(self, molecule: PeriodicSystem):
        """Compute gradient."""
        result = self.run(molecule)
        return result['forces']

    @run_sequentially
    def get_forces(self, molecule: PeriodicSystem):
        """Compute gradient."""
        result = self.run(molecule)
        return result['forces']

    def get_hessian(self, molecule: Any) -> np.array:
        """Compute Hessian."""
        pass

    def _set_executable(self, settings: RunSettings) -> Tuple[str, list]:
        """Set executable for compuation with VASP.

        Default behavior is mpirun -n <n> vasp_gpu
        """
        run_opt = self.method.get('run_opt', None)

        if run_opt is not None and run_opt.get('program') is not None:
            executable = run_opt.get('program')
        elif getattr(settings, 'program', None) is not None:
            executable = getattr(settings, 'program')
        else:
            executable = 'vasp_gpu'

        if run_opt is not None and run_opt.get('launcher') is not None:
            launcher = run_opt.get('launcher')
        elif len(getattr(settings, 'launcher')) > 0:
            launcher = getattr(settings, 'launcher')
        else:
            if settings.ntasks is not None and settings.ntasks != 0:
                launcher = f'mpirun -np {settings.ntasks}'
            else:
                launcher = 'mpirun -np 1'

        return executable, launcher

    def _get_incar_string(self, method: dict) -> str:
        """Get template for VASP from method.

        For VASP, we need to put each simulation in a folder in the working
        directory. This folder can have a name that is the hash of all its
        contents: KPOINTS, INCAR, POSCAR, POTCAR

        method['incar_options'] overrides options in method['incar_string']
        """
        incar_string = method.get('incar_string', '')

        incar_option_dict = {'SYSTEM': 'hylleraas_al'}

        lines = incar_string.replace(';', '\n').split('\n')
        for line in lines:
            split = line.replace(' ', '').split('=')
            if len(split) == 2:
                key, value = split
                incar_option_dict.update({key: value})
            elif len(split) == 1:
                if len(split[0]) == 0:
                    pass
                else:
                    raise ValueError(
                        'There is a line in your INCAR \
                                        that is not a key value line'
                    )
            else:
                raise ValueError(
                    'Something wrong with key value pairs\
                                    in INCAR'
                )

        incar_options = method.get('incar_options')
        if incar_options is not None:
            incar_option_dict.update(incar_options)

        incar_string = '\n'.join(
            [f'{key} = {value}' for key, value in incar_option_dict.items()]
        )
        return incar_string

    def _get_pre_converge_incar_string(self, method: dict):
        """Get template for VASP from method.

        For VASP, we need to put each simulation in a folder in the working
        directory. This folder can have a name that is the hash of all its
        contents: KPOINTS, INCAR, POSCAR, POTCAR

        method['incar_options'] overrides options in method['incar_string']
        """
        incar_option_dict = {'SYSTEM': 'hylleraas_al'}

        incar_options = method.get('pre_converge_incar_options', None)
        if incar_options is not None:
            incar_option_dict.update(incar_options)
        else:
            return None

        incar_string = '\n'.join(
            [f'{key} = {value}' for key, value in incar_option_dict.items()]
        )
        return incar_string

    def _get_kpoints_string(self, method: dict) -> str:
        kpoints_template = GAMMA_SINGLE
        kpoints_options = method.get('kpoints')

        if kpoints_options is None or kpoints_options == {}:
            return kpoints_template
        else:
            if isinstance(kpoints_options, dict):
                kpoints_string = kpoints_options.get('string')
                return kpoints_string
            else:
                raise ValueError('Kpoints options not correctly given')

    def _get_poscar_string(
        self,
        molecule: PeriodicSystem,
        comment: str = 'Autogenerated by \
Hylleraas Software Platform VASP interface',
    ):
        """Create VASP POSCAR from Hylleraas Molecule and unit cell."""
        poscar_content = comment + '\n'
        poscar_content += '1.0\n'

        # This way of calling is odd?
        pbc = molecule.get_pbc(molecule)

        poscar_content += ' '.join([f'{e:.16f}' for e in pbc[0]]) + '\n'
        poscar_content += ' '.join([f'{e:.16f}' for e in pbc[1]]) + '\n'
        poscar_content += ' '.join([f'{e:.16f}' for e in pbc[2]]) + '\n'

        numbers = np.array(molecule.atomic_numbers)
        positions = np.array(molecule.coordinates)
        names = np.array(molecule.atoms)

        # Need to supply particle types one by one in poscar.
        # Sorting so that we start with the lowest atomic number
        order = np.argsort(numbers)

        _, unique_indices, unique_counts = np.unique(
            np.array(numbers)[order], return_counts=True, return_index=True
        )
        unique_names = names[order][unique_indices]

        poscar_content += ' '.join(unique_names) + '\n'
        poscar_content += ' '.join(map(str, unique_counts)) + '\n'

        poscar_content += 'Cartesian\n'

        # Inserting particles in the same order as the sorted unique species
        # names
        for i in order:
            poscar_content += (
                ' '.join([f'{positions[i,j]}' for j in [0, 1, 2]]) + '\n'
            )

        return poscar_content, np.argsort(order)

    def _get_potcar_string(self, molecule: PeriodicSystem):
        mappings = {}
        mappings.update(self.potential_specification['species_mapping'])
        order = np.argsort(molecule.atomic_numbers)
        _, unique_indices = np.unique(
            np.array(molecule.atomic_numbers)[order], return_index=True
        )

        species_in_order = list(
            np.array(molecule.atoms)[order][unique_indices]
        )

        for i, species in enumerate(species_in_order):
            if species in mappings.keys():
                species_in_order[i] = mappings[species]

        return ''.join(
            [
                open(
                    os.path.join(
                        self.potential_specification['path'], folder, 'POTCAR'
                    )
                ).read()
                for folder in species_in_order
            ]
        )

    def parse(self, output: ComputeResult, **kwargs) -> dict:
        """Parse VASP output.

        Parameter
        ---------
        output: `:obj:ComputeResult`
            result of the computation
        kwargs: dict
            additional keyword arguments

        Returns
        -------
        dict
            parsed results

        """
        results: dict = {}
        if output.output_file:
            results.update(self.OutputParser.parse(output.output_file))
        if output.stdout:
            results.update(self.OutputParser.parse_stdout(output.stdout))
        if output.stderr:
            results['stderr'] = output.stderr

        return results

    def run(self, object: PeriodicSystem, *args, **kwargs):
        """Train model.

        Arguments
        ---------
        data (data_type): The training data.
        **kwargs: Additional keyword arguments.

        Returns
        -------
        dict: The output of the training.

        """
        run_opt = kwargs.pop('run_opt', {})
        compute_settings = []
        if self.pre_converge_incar_string is not None:
            compute_settings.append(
                self.setup_pre_converge(object, run_opt=run_opt)
            )

        compute_settings.append(self.setup(object, run_opt=run_opt))
        if self.encut_ladder is not None:
            compute_settings += self.setup_encut_ladder(
                object, run_opt=run_opt
            )

        output_unparsed = self.compute_settings.Runner.run(compute_settings)
        return self.parse(output_unparsed[-1])

    async def arun(self, object: PeriodicSystem, *args, **kwargs):
        """Train model.

        Arguments
        ---------
        data (data_type): The training data.
        **kwargs: Additional keyword arguments.

        Returns
        -------
        dict: The output of the training.

        """
        run_opt = kwargs.pop('run_opt', {})
        compute_settings = []
        if self.pre_converge_incar_string is not None:
            compute_settings.append(
                self.setup_pre_converge(object, run_opt=run_opt)
            )
        compute_settings.append(self.setup(object, run_opt=run_opt))
        if self.encut_ladder is not None:
            compute_settings += self.setup_encut_ladder(
                object, run_opt=run_opt
            )

        output_unparsed = await self.compute_settings.Runner.arun(
            compute_settings
        )
        return self.parse(output_unparsed[-1])
