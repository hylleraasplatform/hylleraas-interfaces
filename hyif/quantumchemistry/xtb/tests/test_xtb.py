# from pathlib import Path
import numpy as np
from hyobj import Molecule, Units
from hyset import create_compute_settings
from packaging.version import Version

from hyif import Xtb

compute_settings = create_compute_settings('local', force_recompute=True,
                                           debug=True)
myxtb = Xtb({'parser': 'full', 'units': Units('metal')},
            compute_settings=compute_settings)
xyz = '''3

O   0.0000   0.0000   0.0626
H  -0.7920   0.0000  -0.4973
H   0.7920   0.0000  -0.4973'''
mymol = Molecule(xyz, units='angstrom')


def test_init():
    """Test init."""
    assert hasattr(myxtb, 'compute_settings')
    assert hasattr(myxtb, 'run_settings')
    assert myxtb.run_settings.program == 'xtb'
    assert 'xtbrestart' in [
        f.name for f in myxtb.run_settings.files_for_restarting
    ]


def test_check_version():
    """Test check_version."""
    assert myxtb.check_version() >= Version('6.4.0')


# def test_setup():
#     """Test setup."""
#     rset = myxtb.setup(mymol)
#     assert rset.stdout_redirect.name \
#         == '3fa01cf6f147664ad455835d35ae4c888a98aef2.out'
#     assert rset.files_to_write[0][0].name \
#         == '3fa01cf6f147664ad455835d35ae4c888a98aef2.xyz'
#     assert rset.files_to_write[0][1].split('\n')[0] == '3'


# def test_setup_sequential():
#     """Test setup for sequential runs."""
#     rset = myxtb.setup([mymol, mymol])
#     assert isinstance(rset, list)
#     assert rset[0].stdout_redirect.name \
#         == '3fa01cf6f147664ad455835d35ae4c888a98aef2.out'


def test_run():
    """Test run."""
    result = myxtb.run(mymol)
    print(mymol, result)
    np.testing.assert_almost_equal(result['energy'],
                                   -5.070203413771,
                                   decimal=3)


def test_constraint_setup():
    """Test constraint setup."""
    setup = myxtb.setup(mymol,
                        program_opt={'constraints': [['angle', 0, 1, 2, 100]]})
    assert 'angle: 1, 2, 3, 100.0' in setup.files_to_write[1][1]


def test_units():
    """Test unit."""
    coord = np.array([[0, 0, 0.0626],
                      [-0.7920, 0, -0.4973],
                      [0.7920, 0, -0.4973]]) * 1.8897259886
    mymol2 = Molecule({'atoms': mymol.atoms, 'coordinates': coord},
                      units='bohr')
    ens = myxtb.get_energy([mymol, mymol2])
    np.testing.assert_almost_equal(ens[0], ens[1], decimal=5)
    en1 = myxtb.get_energy(mymol, units=Units('atomic'))
    np.testing.assert_almost_equal(en1 * 27.2114, ens[0],
                                   decimal=3)
    # en_ref = -5.070203413771/27.2114
    # result1 = myxtb.get_energy(mymol)
    # np.testing.assert_almost_equal(result1, en_ref, decimal=6)
    # myxtb2 = Xtb({'units': Units('metal')})
    # result2 = myxtb2.get_energy(mymol)
    # np.testing.assert_almost_equal(result2, result1*27.2114, decimal=4)


def test_gradient():
    """Test gradient."""
    myxtb = Xtb({'parser': 'full', 'units': Units('atomic')},
                compute_settings=compute_settings)
    result = myxtb.run(mymol, program_opt={'properties': 'gradient'})
    gradient_ref = np.array([[-0., 0., 0.00629255],
                             [-0.01051322, -0., -0.00314627],
                             [0.01051322, -0., -0.00314627]])
    np.testing.assert_allclose(result['gradient'],
                               gradient_ref,
                               rtol=1e-5,
                               atol=1e-5)
    result = myxtb.get_gradient(mymol)
    np.testing.assert_allclose(result, gradient_ref, rtol=1e-5, atol=1e-5)


def test_hessian():
    """Test hessian."""
    myxtb = Xtb({'parser': 'full', 'units': Units('atomic')},
                compute_settings=compute_settings)
    result = myxtb.run(mymol, program_opt={'properties': 'hessian'})
    frequencies_ref = np.array([1547.2243, 3480.9186, 3502.5085])
    np.testing.assert_allclose(result['frequencies'],
                               frequencies_ref,
                               rtol=1e-5,
                               atol=1e-5)
    modes = myxtb.get_normal_modes(mymol)
    np.testing.assert_allclose(frequencies_ref,
                               modes['frequencies'],
                               rtol=1e-5,
                               atol=1e-5)
    hessian = myxtb.get_hessian(mymol)
    np.testing.assert_allclose(hessian,
                               result['hessian'],
                               rtol=1e-5,
                               atol=1e-5)


def test_geometry_optimization():
    """Test geometry optimization."""
    myxtb = Xtb({'parser': 'full', 'units': Units('atomic')},
                compute_settings=compute_settings)
    result = myxtb.run(mymol,
                       program_opt={'properties': 'geometry_optimization'})
    np.testing.assert_almost_equal(result['energy'],
                                   -5.070544318645,
                                   decimal=4)


def test_constraint_geometry_optimization():
    """Test constraint geometry optimization."""
    myxtb = Xtb({'parser': 'full', 'units': Units('atomic')},
                compute_settings=compute_settings)
    result = myxtb.run(mymol,
                       program_opt={
                               'properties': 'geometry_optimization',
                               'constraints': [['angle', 1, 0, 2, 90]]
                           })
    np.testing.assert_almost_equal(result['energy'],
                                   -5.067889248589,
                                   decimal=3)


def test_input_parser():
    """Test input parser."""
    mol = myxtb.InputParser.parse(xyz)
    assert mol['atoms'] == ['O', 'H', 'H']
