from pathlib import Path
from typing import Union

from hyobj import Molecule

from ...abc import Parser


class InputParser(Parser):
    """Xtb input (==xyz.file) input parser."""

    @classmethod
    def parse(cls, file: Union[str, Path]) -> dict:
        """Parser XTB input."""
        result: dict = {}
        mol = Molecule(str(file))
        result['coordinates'] = mol.coordinates
        result['atoms'] = mol.atoms
        result['properties'] = {}

        return result
