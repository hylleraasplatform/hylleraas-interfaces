import copy
import re
import warnings
# from contextlib import suppress
from dataclasses import replace
# from pathlib import Path
from typing import Any, Optional

import numpy as np
# import numpy as np
import packaging.version as pv
from hyobj import MoleculeLike, PeriodicSystem, Units
from hyset import (ComputeResult, ComputeSettings, File, RunSettings,
                   create_compute_settings, run_sequentially)
from qcelemental import PhysicalConstantsContext

from ....manager import NewRun, PreSetup
from ....utils import unique_filename
from ...abc import HylleraasQMInterface
# from ...presets import SETTINGS_LEGACY as SETTINGS
from .input_parser import InputParser
from .output_parser import OutputParser

constants = PhysicalConstantsContext('CODATA2018')


# from .template import TEMPLATE

# from ...molecule import MoleculeLike


# try:
#     import pycp2k
#     found_pycp2k = True
# except ImportError:
#     found_pycp2k = False

# from hyset import ComputeSettings, create_compute_settings


VERSIONS_SUPPORTED = ['7.1', '2022.2']

DEFAULT_INPUT = """&GLOBAL
  PROJECT ${PROJECT_NAME}
  RUN_TYPE ENERGY_FORCE
  PRINT_LEVEL LOW
&END GLOBAL
&FORCE_EVAL
   METHOD QUICKSTEP
   &PRINT
      &FORCES ON
      &END FORCES
   &END PRINT
   &DFT
      BASIS_SET_FILE_NAME BASIS_SET
      BASIS_SET_FILE_NAME BASIS_MOLOPT
      BASIS_SET_FILE_NAME BASIS_MOLOPT_UCL
      POTENTIAL_FILE_NAME GTH_POTENTIALS
      CHARGE 0
      MULTIPLICITY 0
      &SCF
         MAX_SCF 5
         EPS_SCF 1e-05
         SCF_GUESS ATOMIC
         &OUTER_SCF
            EPS_SCF 1e-05
            MAX_SCF 100
         &END OUTER_SCF
         &OT
            MINIMIZER DIIS
            PRECONDITIONER FULL_SINGLE_INVERSE
         &END OT
      &END SCF
      &XC
         &VDW_POTENTIAL
            POTENTIAL_TYPE PAIR_POTENTIAL
            &PAIR_POTENTIAL
               TYPE DFTD3
               LONG_RANGE_CORRECTION .TRUE.
               REFERENCE_FUNCTIONAL PBE
               PARAMETER_FILE_NAME dftd3.dat
               EPS_CN 0.01
               R_CUTOFF 7.5
            &END PAIR_POTENTIAL
         &END VDW_POTENTIAL
         &XC_FUNCTIONAL PBE
         &END XC_FUNCTIONAL
         &XC_GRID
            XC_DERIV PW
         &END XC_GRID
      &END XC
      &MGRID
         REL_CUTOFF 60
         CUTOFF 800
      &END MGRID
      &QS
         EPS_DEFAULT 1e-12
      &END QS
   &END DFT
   &SUBSYS
    &KIND H
      ELEMENT   H
      BASIS_SET DZVP-GTH-PBE
      POTENTIAL GTH-PBE-q1
    &END KIND
    &KIND Li
      ELEMENT   Li
      BASIS_SET DZVP-MOLOPT-SR-GTH-q3
      POTENTIAL GTH-PBE-q3
    &END KIND
    &KIND Cl
      BASIS_SET DZVP-MOLOPT-GTH-q7
      POTENTIAL GTH-PBE-q7
    &END KIND
    &KIND O
      ELEMENT   O
      BASIS_SET DZVP-GTH-PBE
      POTENTIAL GTH-PBE-q6
    &END KIND
    &KIND C
      ELEMENT   C
      BASIS_SET DZVP-GTH-PBE
      POTENTIAL GTH-PBE-q4
    &END KIND
    &KIND Mg
      ELEMENT   Mg
      BASIS_SET DZVP-GTH-PBE-q2
      POTENTIAL GTH-PBE-q2
    &END KIND
    &CELL
TO_REPLACE_WITH_CELL_COORDINATES
    &END CELL
    &COORD
TO_REPLACE_WITH_MOLECULE_COORDINATES
    &END COORD
   &END SUBSYS
&END FORCE_EVAL"""


class CP2K(HylleraasQMInterface, NewRun, PreSetup):
    """CP2K interface."""

    def __init__(
        self, method: dict, compute_settings: Optional[ComputeSettings] = None
    ):
        # if not found_pycp2k:
        #     raise ImportError('could not find package pycp2k')

        if compute_settings is None:
            self.compute_settings = create_compute_settings()
        else:
            self.compute_settings = compute_settings

        # self.executable: str
        # self.launcher: Union[list, str]
        # self.executable, self.launcher = self._set_executable(
        #     method.get('program_opt', {}),
        #     self.compute_settings.ntasks,
        #     self.compute_settings.smp_threads)

        # self.template = self.get_template(method)
        self.template = method.get('template', DEFAULT_INPUT)
        try:
            with open(self.template, 'r') as f:
                self.template = f.read()
        except (FileNotFoundError, OSError):
            pass

        self.Runner = self.compute_settings.Runner
        # self.runner: Runner = getattr(
        #     self.compute_settings,
        #     str(SETTINGS[self.compute_settings.name]['runner']),
        # )

        # self.InputParser: Parser
        # self.OutputParser: Parser
        # self.InputParser, self.OutputParser = self._set_parser(
        #     self.compute_settings)

        self.InputParser = InputParser
        self.OutputParser = OutputParser

        # to_check = method.get('check_version', True)
        # self.version: pv.Version = self.check_version() if to_check else None

        # self.calc = pycp2k.CP2K()
        # self.calc.parse(self.template)
        # # Path(self.template).unlink()

        # CP2K_INPUT = self.calc.CP2K_INPUT  # noqa N806
        # GLOBAL = CP2K_INPUT.GLOBAL  # noqa N806
        # FORCE_EVAL = CP2K_INPUT.FORCE_EVAL_list[0]  # noqa N806
        # SUBSYS = FORCE_EVAL.SUBSYS  # noqa N806
        # DFT = FORCE_EVAL.DFT  # noqa N806
        # SCF = DFT.SCF  # noqa N806

        # self.subsys = SUBSYS

        # if self.options:
        #     for k, v in self.options.items():
        #         if self.is_int(v) or isinstance(v, int):
        #             exec('%s = %d' % (k, int(v)))
        #         elif self.is_float(v) or isinstance(v, float):
        #             exec('%s = %20.14f' % (k, float(v)))
        #         else:
        #             exec("%s = '%s'" % (k, v))
        self.method = method
        self.units = self.method.get('units', Units('atomic'))

        self.run_settings = self.compute_settings.run_settings
        self.version: pv.Version = (
            self.check_version()
            if self.method.get(
                'check_version', self.compute_settings.arch_type == 'local'
            )
            else None
        )

    @run_sequentially
    def setup(self, molecule: MoleculeLike, **kwargs) -> RunSettings:
        """Set up CP2K calculation.

        Parameters
        ----------
        molecule : Molecule
            Molecule object.
        **kwargs
            Additional keyword arguments.

        Returns
        -------
        RunSettings
            RunSettings object.

        """
        run_settings = copy.deepcopy(self.run_settings)
        program = self._set_executable(run_settings)
        if 'run_opt' in kwargs.keys():
            if 'program' in kwargs['run_opt'].keys():
                program = kwargs['run_opt'].pop('program')
            run_settings = replace(run_settings, **kwargs.get('run_opt'))

        program_opt = kwargs.pop('program_opt', {})
        input_str = program_opt.pop('input', self.template)
        input_str = program_opt.pop('template', input_str)

        try:
            unit = molecule.units.length[0]
        except AttributeError:
            warnings.warn(
                'could not determine unit of molecule '
                + 'coordinates, will assume bohr'
            )
            unit = 'bohr'
        else:
            if unit.lower() in 'angstroms':
                unit = 'ANGSTROM'
            elif unit.lower() in 'bohrs':
                unit = 'BOHR'
            else:
                raise ValueError('unit must be bohr or angstrom')

        if 'TO_REPLACE_WITH_MOLECULE_COORDINATES' in input_str:
            mol_str = ''
            for i, atom in enumerate(molecule.atoms):
                mol_str += f'      {atom}'
                mol_str += ' '.join(
                    ['{0:15.9f}'.format(c) for c in molecule.coordinates[i]]
                )
                if 'molecule_ids' in molecule.properties:
                    mol_str += f' {molecule.properties["molecule_ids"][i]}'
                    if 'residue_ids' in molecule.properties:
                        mol_str += f' {molecule.properties["residue_ids"][i]}'
                mol_str += '\n'

            mol_str += f'      UNIT {unit}'
            input_str = input_str.replace(
                'TO_REPLACE_WITH_MOLECULE_COORDINATES', mol_str
            )

        unit_cell = program_opt.pop('unit_cell', None)
        if unit_cell is None:
            try:
                unit_cell = molecule.hmatrix  # type: ignore
            except AttributeError as exc:
                if not isinstance(molecule, PeriodicSystem):
                    extra_perimeter = program_opt.pop('extra_perimeter', 5.0)
                    max_dist = [
                        molecule.coordinates[:, i].max()
                        - molecule.coordinates[:, i].min()
                        + extra_perimeter
                        for i in range(3)
                    ]
                    unit_cell = np.diag(max_dist)
                else:
                    raise ValueError('unit_cell must be provided') from exc
        box_str = ''
        if unit == 'BOHR':
            box_str = '[bohr]'

        if 'TO_REPLACE_WITH_CELL_COORDINATES' in input_str:
            if unit_cell is None:
                raise ValueError('unit_cell must be provided')
            cell_str = ''
            cell_str += '      A ' + box_str
            cell_str += ' '.join(['{0:15.9f}'.format(c) for c in unit_cell[0]])
            cell_str += '\n'
            cell_str += '      B ' + box_str
            cell_str += ' '.join(['{0:15.9f}'.format(c) for c in unit_cell[1]])
            cell_str += '\n'
            cell_str += '      C ' + box_str
            cell_str += ' '.join(['{0:15.9f}'.format(c) for c in unit_cell[2]])
            cell_str += '\n'

            if not isinstance(molecule, PeriodicSystem):
                cell_str += '      PERIODIC NONE\n'

            input_str = input_str.replace(
                'TO_REPLACE_WITH_CELL_COORDINATES', cell_str
            )

        # Try to replace variables that are defined in program_opt
        for key, value in program_opt.items():
            str_to_be_replaced = '{{' + key + '}}'
            if str_to_be_replaced in input_str:
                input_str = input_str.replace(str_to_be_replaced, str(value))
            else:
                warnings.warn(
                    f'Variable {key} is defined in program_opt, '
                    + 'but not used in input file.'
                )

        hash_str = unique_filename(input_str.split('\n'))

        input_str = re.sub(
            r'(?i)(project).*', f'PROJECT {hash_str}', input_str, count=1
        )

        # Search through the input string for variables that are not replaced
        # and raise an error if any are found.
        for line in input_str.split('\n'):
            if re.search(r'{{.*}}', line) and '{{hash_str}}' not in line:
                raise ValueError(
                    f'Variable {line} is not defined in program_opt.'
                )
        input_filename = hash_str + '.inp'
        input_file = File(name=input_filename, content=input_str)
        output_filename = hash_str + '.out'
        # output_file_container = input_file_container.with_suffix('.out')

        args = run_settings.args.copy()
        args.insert(0, f'{input_filename}')
        args.insert(0, '-i')

        files_to_write = [input_file]

        # TODO: why is type: ignore needed?
        setup_dict = {
            'program': program,
            'args': args,
            'files_to_write': files_to_write,  # type: ignore
            'stdout_file': hash_str + '.out',
            'stderr_file': hash_str + '.stderr',
            'output_file': output_filename,
        }

        return replace(run_settings, **setup_dict)

    @run_sequentially
    def parse(self, output: ComputeResult, **kwargs) -> dict:
        """Parse cp2k output.

        Parameter
        ---------
        output: `:obj:ComputeResult`
            result of the computation
        kwargs: dict
            additional keyword arguments

        Returns
        -------
        dict
            parsed results

        """
        results: dict = {}
        if output.output_file:
            results.update(self.OutputParser.parse(output.output_file))
        if output.stderr:
            # raise RuntimeError(output.stderr)
            # warnings.warn(output.stderr)
            results['stderr'] = output.stderr

        return results

    # def run(self, molecule, *args, **kwargs):
    #     """Run cp2k calculation context-based."""
    #     with compute(self, molecule, *args, **kwargs) as r:
    #         return r

    # async def arun(self, molecule, *args, **kwargs):
    #     """Run cp2k calculation context-based and asyncronous."""
    #     async with acompute(self, molecule, *args, **kwargs) as r:
    #         return r

    def _set_executable(self, settings: RunSettings) -> str:
        """Set executable for seriel/parallel compuation with CP2K.

        Parameter
        ---------
        program_opt: dict
            program options
        num_procs: Union[None, int]
            number of MPI processes
        smp_threads: Union[None, int]
            number of SMP threads

        Returns
        -------
        Tuple[str, list]
            name of binary

        """
        if settings.ntasks is not None and settings.ntasks > 1:
            if settings.cpus_per_task == 1:
                executable = 'cp2k.popt'
            else:
                executable = 'cp2k.psmp'
        else:
            if (
                settings.cpus_per_task is not None
                and settings.cpus_per_task > 1
            ):
                executable = 'cp2k.ssmp'
            else:
                executable = 'cp2k.sopt'

        return executable

    def check_version(self) -> pv.Version:
        """Check if program version is compatible with interface.

        Returns
        -------
        :obj:`packaging.Version'
            version object

        """
        version: pv.Version = None

        out = self.run(
            None, run_opt={'program': 'cp2k.ssmp', 'args': '--version'}
        )
        version = pv.parse(out['version'])

        if version not in (pv.parse(v) for v in VERSIONS_SUPPORTED):
            raise NotImplementedError(
                f' CP2K version {version} not supported.',
                f' Please contact {self.author} ',
            )

        return version

    @run_sequentially
    def get_energy(self, molecule: MoleculeLike, **kwargs):
        """Compute energy."""
        result = self.run(molecule, **kwargs)
        return result['energy']

    @run_sequentially
    def get_gradient(self, molecule: MoleculeLike, **kwargs):
        """Compute gradient."""
        result = self.run(molecule, **kwargs)
        return result['gradient']
        # gradient = result['gradient']
        # # if (molecule.properties.get('unit', 'angstrom') == 'angstrom'):
        # if kwargs.get('unit', 'bohr') == 'angstrom':
        #     gradient /= constants.bohr2angstroms
        # return gradient

    def get_hessian(self, molecule: Any):
        """Compute hessian."""
        pass

    # def is_float(self, instr):
    #     """Check if string is float."""
    #     try:
    #         float(instr)
    #     except Exception:
    #         return False
    #     else:
    #         return True

    # def is_int(self, instr):
    #     """Check if string is integer."""
    #     if isinstance(instr, int):
    #         return True
    #     return instr.isdigit()

    # @dataclass
    # class AseAtom:
    #     """Conversion."""

    #     symbol: str
    #     position: Union[list, np.array]

    # class AseCell:
    #     """Required by pycp2k."""

    #     def __init__(self, cell, pbc):
    #         self.cell = cell
    #         self.pbc = pbc

    #     def get_cell(self):
    #         """Return unit cell."""
    #         return self.cell

    #     def get_pbc(self):
    #         """Return periodic boundary conditions."""
    #         return self.pbc

    @property
    def author(self):
        """Return who wrote this interface."""
        return 'tilmannb@uio.no'
