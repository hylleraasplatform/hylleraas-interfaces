from pathlib import Path
from typing import List

import ase
import numpy as np
from hyobj import Constants

from ...abc import Parser

ERROR_EXCEPTIONS: list = []
WARNING_EXCEPTIONS: list = []


class OutputParser(Parser):
    """CP2K output parser."""

    @classmethod
    def parse(cls, outpath) -> dict:
        """Parse the cp2k output file and extract relevant information.

        Arguments
        ---------
        outpath (str): The path to the cp2k output file.

        Returns
        -------
        dict: A dictionary containing the parsed information
        from the output file. Parses also the trajectory file.

        """
        result: dict = {}
        errors: list = []
        warnings: list = []
        is_converged: bool = False
        program_ended: bool = False
        try:
            with open(outpath, 'rt') as output_file:
                lines = output_file.readlines()
        except FileNotFoundError:
            return {'errors': ['File not found'], 'is_converged': False}
        conv_fac = 1.0
        for i, line in enumerate(lines):
            if 'Total number of' in line:
                if '- Atoms:' in lines[i + 1]:
                    num_atoms = int(lines[i + 1].split()[-1])  # type: ignore
                    result['num_atoms'] = num_atoms
            if 'ENERGY|' in line:
                en = float(line.split()[-1])
                result.setdefault('energy', []).append(en)
            if 'STRESS| Analytical stress tensor [GPa]' in line:
                stress = []
                for j in range(3):
                    stress.append(
                        [float(x) for x in lines[i + 2 + j].split()[2:]]
                    )
                # Convert from GPa to a.u.
                result['stress'] = (
                    np.array(stress) / 29421.02648438959
                ).flatten()

            if 'ATOMIC FORCES in [a.u.]' in line:
                istart = i + 3
                forces = []
                j = istart
                elements = []
                while 'SUM OF ATOMIC FORCES' not in lines[j]:
                    line_split = lines[j].split()
                    # Negative sign to turn forces into gradients
                    xyz = [float(line_split[k]) for k in range(3, 6)]
                    forces.append(xyz)
                    elements.append(line_split[2])
                    j += 1
                result.setdefault('force', []).append(
                    np.array(forces).flatten()
                )

            if 'CP2K| version string:' in line:
                result['version'] = line.split()[-1]

            if 'GLOBAL| Total number of message passing processes' in line:
                result['mpi_procs'] = line.split()[-1]

            if 'GLOBAL| Number of threads for this process' in line:
                result['smp_threads'] = line.split()[-1]

            if 'error' in line.lower():
                if not any([ex in line for ex in ERROR_EXCEPTIONS]):
                    errors.append(line)

            if 'warning' in line.lower():
                warning = line
                if not any([ex in warning for ex in WARNING_EXCEPTIONS]):
                    warnings.append(warning)

            bad_warnings = [
                'SCF run NOT converged',
                'The cutoff radius is '
                + 'larger than half the minimal cell dimension.',
            ]
            is_converged = True
            for warn in warnings:
                for bad_warning in bad_warnings:
                    if bad_warning.lower() in warn.lower():
                        is_converged = False

            if 'abort' in line.lower():
                if not any([ex in line for ex in ERROR_EXCEPTIONS]):
                    errors.append(line)

            if len(errors) > 0:
                is_converged = False

            if 'Writing TRAJECTORY' in line:
                result['trajectory_file'] = line.split()[-1]
            if 'CELL| Vector' in line:
                key = 'cell' + line.split()[2]
                if 'angstrom' in line.lower():
                    conv_fac = 1 / Constants.bohr2angstroms
                value = [
                    float(line.split()[4]),
                    float(line.split()[5]),
                    float(line.split()[6]),
                ]
                value = [x * conv_fac for x in value]
                result.setdefault(key, []).append(value)

            if 'T I M I N G' in line:
                if ' CP2K ' in lines[i + 5]:
                    result['walltime'] = float(lines[i + 5].split()[-1])
            if 'PROGRAM ENDED' in line:
                program_ended = True

        if 'cella' in result and 'cellb' in result and 'cellc' in result:
            result['box'] = [
                result['cella'][i] + result['cellb'][i] + result['cellc'][i]
                for i in range(len(result['cella']))
            ]
            del result['cella']
            del result['cellb']
            del result['cellc']

        # Strip .out extension from outpath, there may be more than one .
        if 'trajectory_file' not in result:
            hash_str = '.'.join(str(outpath).split('.')[:-1])
            # look for dcd, xyz
            for ext in ['dcd', 'xyz']:
                fn = hash_str + '-pos-1.' + ext
                if Path(fn).exists():
                    result['trajectory_file'] = fn
                    break

        if 'trajectory_file' in result:
            traj_filename = str(
                Path(outpath).parent / result['trajectory_file']
            )
            if traj_filename.rsplit('.', maxsplit=1)[-1] == 'dcd':
                traj = ase.io.read(
                    traj_filename, index=':', format='cp2k-dcd', aligned=True
                )
                # Check whether cell has zero entries and if so ignore
                # them and use the cell from the last frame
                if 9 != sum([x == 0 for x in traj[0].get_cell().flatten()]):
                    result['box'] = np.array(
                        [
                            frame.get_cell().flatten() * conv_fac
                            for frame in traj
                        ]
                    )
            else:
                traj = ase.io.read(traj_filename, index=':')
            result['coordinates'] = np.array(
                [frame.get_positions().flatten() * conv_fac for frame in traj]
            )

        result['errors'] = errors
        result['warnings'] = warnings
        if not program_ended:
            is_converged = False
        result['is_converged'] = is_converged
        result['output_file'] = str(outpath)

        for key, value in result.items():
            if isinstance(value, List):
                if len(value) == 1 and not isinstance(value[0], str):
                    result[key] = value[0]

        if 'stress' in result.keys() and 'box' in result.keys():
            volumes = np.array(
                [
                    np.linalg.det(box.reshape(3, 3))
                    for box in np.atleast_2d(result['box'])
                ]
            )
            result['virial'] = np.array(result['stress']) * volumes
            result.pop('stress')
        return result
