import numpy as np
import pandas as pd
from hyobj import DataSet, PeriodicSystem
from hyset import create_compute_settings

from hyif import CP2K  # type: ignore

mymol = PeriodicSystem(
    {
        'atoms': ['O', 'H', 'H'],
        'coordinates': [0, 0, 0, 0, 0, 0.74, 0, 0.74, 0],
        'pbc': [10, 0, 0, 0, 10, 0, 0, 0, 10],
    }
)

myenv_cp2k = create_compute_settings('local', ntasks=2, cpus_per_task=1)


def test_version_check():
    """Test cp2k version check."""
    CP2K({'check_version': False}, myenv_cp2k)
    # version = mycp2k.check_version()
    # assert version >= pv.Version('7.1.0')


def test_setup():
    """Test cp2k setup."""
    mycp2k = CP2K({'check_version': False}, compute_settings=myenv_cp2k)
    mymol = PeriodicSystem(
        {
            'atoms': ['O', 'H', 'H'],
            'coordinates': [0, 0, 0, 0, 0, 0.74, 0, 0.74, 0],
            'pbc': [10, 0, 0, 0, 10, 0, 0, 0, 10],
        }
    )
    setup = mycp2k.setup(mymol, run_opt={'program': 'cp2k.ssmp'})
    assert setup.program == 'cp2k.ssmp'

    setup = mycp2k.setup(mymol, run_opt={})
    assert setup.program == 'cp2k.popt'

    # print(setup.files_to_write[0][1])
    # with pytest.raises(ValueError):
    #     mycp2k.InputParser.parse(setup.files_to_write[0][1])
    # np.testing.assert_allclose(parsed['coordinates'],
    #                            mymol1.coordinates.ravel()*1.8897259886,
    #                            rtol=1e-6, atol=1e-6)


# def test_run():
#     """Test cp2k run."""
#     mycp2k = CP2K({}, compute_settings=myenv_cp2k)
#     result = mycp2k.run(mymol1)
#     ref_en = -1.161696966614423
#     np.testing.assert_almost_equal(result['energy'], ref_en, decimal=4)
#     en = mycp2k.get_energy(mymol1)
#     np.testing.assert_almost_equal(en, ref_en, decimal=4)

#     en2 = mycp2k.get_energy(mymol2)
#     np.testing.assert_almost_equal(en, en2, decimal=4)

#     mycp2k2 = CP2K({'units': Units('real')}, compute_settings=myenv_cp2k)
#     en3 = mycp2k2.get_energy(mymol1)

#     np.testing.assert_almost_equal(en3, 627.509469 * ref_en, decimal=4)

#     en4 = mycp2k.get_energy(mymol1, units=Units('metal'))
#     np.testing.assert_almost_equal(en4, 27.211386245988 * ref_en, decimal=4)

#     grad = mycp2k.get_gradient(mymol1)
#     grad_ref = np.array([[-0.0, -0.0, 0.015942], [-0.0, -0.0, -0.015942]])
#     np.testing.assert_allclose(grad, grad_ref, rtol=1e-5, atol=1e-5)


# def test_units():
#     """Test cp2k units."""
#     grad1 = mycp2k.get_gradient(mymol1)
#     mycp2k2 = CP2K({'units': Units('real')})
#     grad2 = mycp2k2.get_gradient(mymol1)
#     # fac = constants.hartree2kcalmol/constants.bohr2angstrom
#     fac = 627.509469/0.52917721092
#     np.testing.assert_allclose(grad1*fac,
#                                grad2, rtol=1e-5, atol=1e-5)


def test_parallel():
    """Test parallel input."""
    myenv = create_compute_settings('local', ntasks=2, cpus_per_task=2)
    mycp2k = CP2K({'check_version': False}, compute_settings=myenv)
    setup = mycp2k.setup(mymol)
    print(setup.launcher)

    assert setup.program == 'cp2k.psmp'

    myenv = create_compute_settings('local', ntasks=1, cpus_per_task=6)
    mycp2k = CP2K({'check_version': False}, compute_settings=myenv)
    setup = mycp2k.setup(mymol)
    assert setup.cpus_per_task == 6
    assert setup.program == 'cp2k.ssmp'

    myenv = create_compute_settings('local', ntasks=12, cpus_per_task=1)
    mycp2k = CP2K({'check_version': False}, compute_settings=myenv)
    setup = mycp2k.setup(mymol)
    assert setup.ntasks == 12
    assert setup.program == 'cp2k.popt'


def test_parser():
    """Test cp2k parser."""
    mycp2k = CP2K({'check_version': False}, compute_settings=myenv_cp2k)
    data = DataSet(
        'virial_out/af1947fec2d2b651ce0d574164daead05a65e9ce.out',
        parser=mycp2k.OutputParser,
        units=mycp2k.units,
    )
    data2 = DataSet(
        'virial_out/af1947fec2d2b651ce0d574164daead05a65e9ce.inp',
        parser=mycp2k.InputParser,
        units=mycp2k.units,
    )

    data = DataSet(
        pd.concat([data.data, data2.data], axis=1), units=mycp2k.units
    )

    # Check no nan values
    assert not data.data.isnull().values.any()

    # check that stress is in columns
    assert 'virial' in data.data.columns

    # verify virial values
    stress = (
        np.array(
            [
                [3.20377560194e00, 1.34543613208e-01, 1.96107936578e-01],
                [1.34543613208e-01, 3.16139095344e00, -1.19329865927e-01],
                [1.96107936578e-01, -1.19329865927e-01, 3.27046263858e00],
            ]
        )
        / 29421.02648438959
    )
    box = np.array(data.data.iloc[0]['box'].values[0]).flatten().reshape(3, 3)
    volume = np.linalg.det(box)
    virial_ref = (stress * volume).flatten()
    virial_parsed = data.data.iloc[0]['virial']

    assert np.allclose(virial_parsed, virial_ref, rtol=1e-5, atol=1e-5)
