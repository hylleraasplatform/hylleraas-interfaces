import re
from pathlib import Path
from typing import Dict, List, Optional, Tuple

import numpy as np
from hyobj import Constants, Units

from ...molecule import Molecule


class OrcaInput:
    """Orca input class."""

    @classmethod
    def parse(cls, inpath):
        """Parse orca input."""
        result = {}
        molecule = cls.read_molecule_from_file(inpath)

        result['input_file'] = str(inpath)
        result['atoms'] = molecule.atoms
        result['coordinates'] = molecule.coordinates.flatten().tolist()
        return result

    @classmethod
    def gen_orca_input(
        cls,
        main_input: list,
        specific_input: dict,
        molecule: Molecule,
        template: Optional[str] = None,
    ) -> str:
        """Generate input for orca.

        Parameters
        ----------
        main_input : list
            main_input (starting with '!')
        specific_input : dict
            specific_input (starting with '%')
        molecule : :obj:`Molecule`
            (hylleraas-compatible) Molecule object
        template : str
            template for orca input

        Returns
        -------
        str
            input string

        """
        input_str = ''
        if template is not None:
            input_str = template + '\n'

        for x in main_input:
            input_str += f'!{x}\n'
        for k, v in specific_input.items():
            input_str += f'%{k}'
            if isinstance(v, dict):
                input_str += '\n'
                for k1, v1 in v.items():
                    input_str += f'\t{k1} {v1}\n'
                input_str += 'end\n'
            elif isinstance(v, str):
                input_str += ' '
                input_str += v + '\n'
            elif isinstance(v, list):
                input_str += '\n'
                for vv in v:
                    input_str += str(vv) + '\n'
                input_str += 'end\n'
            else:
                input_str += ' '
                input_str += str(v) + '\n'

        try:
            if hasattr(molecule, 'atoms'):
                atoms = molecule.atoms
                coords = molecule.coordinates
                coords = np.array(coords).reshape(-1, 3)
                if hasattr(molecule, 'properties'):
                    prop = molecule.properties
                else:
                    prop = {}
            # elif hasattr(molecule, '_atoms'):
            #     atoms = molecule._atoms
            #     coords = molecule._coordinates
            #     if hasattr(molecule, '_properties'):
            #         prop = molecule._properties
            #     else:
            #         prop = {}
        except TypeError as e:
            print(f'could not read Molecule from {type(molecule)}, {e}')

        charge = prop.get('charge', 0)
        spin = prop.get('spin_multiplicity', 1)

        input_str += f'* xyz {str(charge)} {str(spin)}\n'

        for i in range(len(atoms)):
            input_str += (
                (2) * ' ' + '{:>2}{:20.14f}{:20.14f}{:20.14f}\n'
            ).format(atoms[i], coords[i, 0], coords[i, 1], coords[i, 2])
        input_str += '*\n'

        return input_str

    @classmethod
    def read_molecule_from_file(cls, filename: Path) -> Molecule:
        """Read molecule from file.

        Parameters
        ----------
        filename: :obj:`pathlib.Path`
            path to input file

        Returns
        -------
        Molecule
            Hylleraas-compatible Molecule object

        """
        try:
            if not filename.exists():
                raise FileNotFoundError(
                    'could not find ' f'input file {filename}'
                )
        except TypeError:
            raise TypeError(f'expected {type(Path())}, got {type(filename)}')

        unit = 'bohr'
        atoms = []
        coordinates = []
        lines = (row for row in open(filename, 'r'))
        while True:
            line = lines.__next__()
            if '!' in line:
                ls = (w.lower() for w in line.replace('!', '').split())
                if any(w == 'angs' for w in ls):
                    unit = 'angstrom'
            if '*' in line:
                charge, spin = str(line).split()[2:4]
                break
        while True:
            line = lines.__next__()
            if '*' in line:
                break
            atoms.append(line.split()[0])
            coordinates.append([float(x) for x in line.split()[1:4]])

        if unit == 'angstrom':
            coordinates = np.array(coordinates) / Constants.bohr2angstroms

        return Molecule(
            atoms=atoms,
            coordinates=np.array(coordinates),
            properties={'charge': int(charge), 'spin_multiplicity': int(spin)},
            units=Units('atomic'),
        )

    @classmethod
    def read_method_from_file(cls, filename: Path) -> Tuple[List, Dict]:
        """Read method from file.

        Parameters
        ----------
        filename: :obj:`pathlib.Path`
            path to inputfile

        Returns
        -------
        Tuple[list, dict]
            main_input (starts with '!') and specific_input (starts with '%')

        """
        try:
            if not filename.exists():
                raise FileNotFoundError(
                    'could not find ', f' input file {filename}'
                )
        except TypeError:
            raise TypeError(f'expected {type(Path())}, got {type(filename)}')

        main_input: list = []
        specific_input: dict = {}
        lines = (row for row in open(filename, 'r'))
        stack = []
        for line in lines:
            line_split = line.split()
            line_dsplit = re.split('(?<=[!%])', line_split[0])
            if line_dsplit[0] == '!':
                # main input
                # what about more keywords? is that possible in orca?
                main_input.append(line_dsplit[1])
            elif line_dsplit[0] == '%':
                # special in
                stack.append(line_dsplit[1])
                mydict: dict = {}
                continue

            if line_split[0].lower() == 'end':
                key = stack.pop()
                specific_input[key] = mydict

            if len(stack) > 0:
                # what about more keywords? is that possible in orca?
                mydict[line_split[0]] = line_split[1]

        return main_input, specific_input
