from dataclasses import dataclass

from ...abc import Parser as BaseParser


@dataclass
class Parser:
    """Interface to Parser."""

    InputParser: BaseParser
    OutputParser: BaseParser
