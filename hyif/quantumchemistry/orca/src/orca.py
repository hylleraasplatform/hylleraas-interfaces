from __future__ import annotations

import copy
# import warnings
# from contextlib import suppress
from dataclasses import replace
from pathlib import Path
from typing import Any, Callable, Dict, List, Optional, Tuple, Union

import numpy as np
import packaging.version as pv
from hyobj import Units
from hyset import (ComputeResult, ComputeSettings, RemoteRunSettings,
                   RunSettings, create_compute_settings, run_sequentially)

# from ....manager import acompute, compute
from ....manager import File, NewRun, PreSetup
from ....utils import unique_filename
from ...abc import HylleraasQMInterface, Parser
from ...molecule import Molecule
# from ...presets import SETTINGS_LEGACY as SETTINGS
from .input_parser import OrcaInput
from .output_parser import OrcaOutput
from .parser import Parser as AllParser

QCMETHODS: dict = {'rimp2': 'ri-mp2'}
PROPERTIES: dict = {
    'geometry_optimization': 'opt',
    'analytical_gradient': 'engrad',
    'numerical_gradient': 'numgrad',
    'numerical_hessian': 'numfreq',
    'normal_modes': 'freq',
    'analytical_hessian': 'freq',
    'transition_state': 'OptTS',
}

VERSIONS_SUPPORTED = ['5.0.3', '5.0.4', '6.0.1']


class Orca(HylleraasQMInterface, NewRun, PreSetup):
    """Orca interface."""

    def __init__(
        self, method: dict, compute_settings: Optional[ComputeSettings] = None
    ):
        self.method = method

        if compute_settings is None:
            self.compute_settings = create_compute_settings()
        else:
            self.compute_settings = compute_settings

        # self.Runner: Runner = self._set_runner(self.compute_settings)
        self.Parser: AllParser = self._set_parser(self.compute_settings)
        self.OutputParser = self.Parser.OutputParser
        self.InputParser = self.Parser.InputParser

        self.version: pv.Version = (
            self.check_version()
            if self.method.get(
                'check_version', self.compute_settings.arch_type == 'local'
            )
            else None
        )

        self.main_input, self.specific_input = self.get_method(self.method)
        self.program = method.get('program', 'orca')
        self.input_file_suffix = '.inp'
        self.output_file_suffix = '.out'
        self.run_settings = self.compute_settings.run_settings
        self.units = self.method.get('units', Units())

        self.memory = self.method.get(
            'memory', getattr(self.compute_settings, 'memory_per_cpu', 2000)
        )

        self.template = method.get('template', None)
        try:
            with open(self.template, 'r') as f:
                self.template = f.read()
        except (FileNotFoundError, OSError, TypeError):
            pass

    @run_sequentially
    def parse(self, output: ComputeResult, **kwargs) -> dict:
        """Parse output."""
        outfile = output.output_file
        result = {}
        result['stdout'] = output.stdout
        result['stderr'] = output.stderr

        if outfile is not None:
            result.update(self.OutputParser.parse(outfile))
            result['output_file'] = str(outfile)

        # if result.get('warnings'):
        #     warnings.warn('\n'.join(result['warnings']))

        # if result.get('errors'):
        #     warnings.warn('\n'.join(result['errors']))

        if output.files_to_parse:
            for f in output.files_to_parse:
                pp = self.OutputParser
                try:
                    p = Path(f.work_path_local)
                except AttributeError:
                    p = Path(f)
                if f.suffix == '.hess':
                    h = pp.extract_hessian(p)  # type: ignore
                    result['hessian'] = h
                    h = pp.extract_vibrational_frequencies(p)  # type: ignore
                    result['frequencies'] = h
                    h = pp.extract_normal_modes(p)  # type: ignore
                    result['normal_modes'] = h
                if f.suffix == '.engrad':
                    g = pp.extract_gradient(p)  # type: ignore
                    result['gradient'] = g
                if f.suffix == '.xyz':
                    xyz = pp.extract_geometry(p)  # type: ignore
                    result['final_geometry'] = xyz

        return result

    @run_sequentially
    def setup(self, molecule: Molecule, **kwargs) -> RunSettings:
        """Set up calculation."""
        run_settings = self.pre_setup(**kwargs)
        if molecule is None:
            return run_settings

        program_opt = kwargs.pop('program_opt', {})

        main_input, _, _ = self.add_kw(
            program_opt.get('main_input', None),
            main_input=None,
            specific_input={},
        )
        _, specific_input, _ = self.add_kw(
            program_opt.get('specific_input', None),
            main_input=[],
            specific_input=None,
        )
        main_input = self._set_coord_unit(molecule, main_input)

        specific_input.update(self._set_parallel_input(self.compute_settings))
        input_str = self.InputParser.gen_orca_input(  # type: ignore
            main_input, specific_input, molecule, self.template
        )

        hash_str = program_opt.get(
            'hash', unique_filename(input_str.split('\n'))
        )
        input_file = File(name=hash_str + '.inp', content=input_str)

        files_to_parse = []
        if any([x in main_input for x in ['hess', 'freq', 'numfreq']]):
            hessian_file = File(name=hash_str + '.hess')
            files_to_parse.append(hessian_file)
        if any([x in main_input for x in ['numgrad', 'engrad']]):
            gradient_file = File(name=hash_str + '.engrad')
            files_to_parse.append(gradient_file)
        if any([x in main_input for x in ['OptTS', 'opt']]):
            xyz_file = File(name=hash_str + '.xyz')
            files_to_parse.append(xyz_file)

        output_file = File(name=hash_str + '.out')
        stderr_file = File(name=hash_str + '.stderr')
        files_to_write = [input_file]

        running_dict = {
            'args': [input_file.name],
            'stdout_file': output_file,
            'stderr_file': stderr_file,
            'output_file': output_file,
            'files_to_write': files_to_write,
            'files_to_parse': files_to_parse,
        }
        # orca requires full path when running in parallel
        if run_settings.ntasks > 1:
            c = self.compute_settings
            if isinstance(run_settings, RemoteRunSettings):
                running_dict['env'] = getattr(run_settings, 'env', [])
                running_dict['env'].append('ORCA_EXE=$(which orca)')
                running_dict['launcher'] = ''
                orca_ex = '${ORCA_EXE}'
            else:
                orca_ex = c.find_var_in_env_local(
                    'orca', env=run_settings.env  # type: ignore
                )
        else:
            orca_ex = self.program
        running_dict['program'] = orca_ex
        return replace(run_settings, **running_dict)

    def _set_parallel_input(
        self, compute_settings: ComputeSettings, **kwargs
    ) -> dict:
        """Set variables for parallel computing.

        Parameter
        ---------
        compute_settings: ComputeSettings
            object containing informations about launcher, mpi_procs
            and smp_threads
        """
        parallel_settings: dict = {}
        num_procs = compute_settings.ntasks
        memory = kwargs.get('memory', self.memory)

        if num_procs is not None:
            if num_procs >= 1:
                parallel_settings.update({'PAL': {'NPROCS ': str(num_procs)}})
        if memory is not None:
            if memory > 1:
                parallel_settings.update({'MaxCore': str(memory)})
        return parallel_settings

    def _set_parser(self, compute_settings: ComputeSettings) -> AllParser:
        """Set parsers for input, output, errors.

        Parameter
        ---------
        compute_settings: :obj:`ComputeSettings`
            If the ComputeSettings instance contains an 'options' dict
            with keys 'InputParser', 'OutputParser', the parsers will be
            taken from there.

        Returns
        -------
        :obj:`Parser`

        """
        return AllParser(
            InputParser=OrcaInput, OutputParser=OrcaOutput  # type: ignore
        )  # type: ignore

    # def abs_path(self,
    #              filename: Union[str, Path],
    #              options: Optional[dict] = {},
    #              container: Optional[bool] = False) -> Path:
    #     """Generate absolute path wrt work dir."""
    #     return self.compute_settings.Runner.abs_path(filename,
    #                                                  options=options,
    #                                                  container=container)

    # @contextmanager
    # def _run_ctx(self, input_dict: dict) -> Generator[str, None, None]:
    #     """Run calcultion in context.

    #     Parameter
    #     ---------
    #     input_dict : dict
    #         input

    #     Returns
    #     -------
    #     Generator
    #         generates context

    #     """
    #     output = str(self.runner.run(input_dict))
    #     try:
    #         yield output
    #     finally:
    #         return None

    def check_version(self) -> Optional[pv.Version]:
        """Check if program version is compatible with interface.

        Returns
        -------
        :obj:`packaging.Version'
            version object

        """
        version: pv.Version = None

        version_test_run_settings = copy.deepcopy(self.compute_settings)
        version_test_run_settings.run_settings = replace(
            version_test_run_settings.run_settings,
            program='orca',
            args=['gibberish'],
        )
        output = self.compute_settings.Runner.run(
            version_test_run_settings.run_settings
        )
        version = pv.parse(OrcaOutput.parse(output.stdout)['version'])

        if version not in (pv.parse(v) for v in VERSIONS_SUPPORTED):
            raise NotImplementedError(
                f' Orca version {version} not supported.',
                f' Please contact {self.author} for support.',
            )

        return version

    @property
    def author(self):
        """Return author's email adress."""
        return 'tilmann.bodenstein@kjemi.uio.no'

    def _translate_kw(self, kwlist: Union[list, str], mapping: dict) -> list:
        """Translate keywords for orca.

        Parameter
        ---------
        kwlist: str or list
            keywords

        Returns
        -------
        list
            list of orca keywords

        """
        if not kwlist:
            return []
        kwlist = [kwlist] if not isinstance(kwlist, list) else kwlist
        return [mapping.get(str(x).lower(), str(x)) for x in kwlist]

    def get_method(self, method: dict) -> Tuple[list, dict]:
        """Get method information in orca format.

        Parameter
        ---------
        method : dict
            input dictionary as come from hylleraas.Method

        Returns
        -------
        Tuple(list, dict)
            main_input (starts with '!') and specific input (starts with '%')

        """
        main_input: list = []
        specific_input: dict = {}
        input_file = method.get('input_file', None)
        if input_file is not None:
            (main_input, specific_input) = (
                self.InputParser.read_method_from_file(  # type: ignore
                    Path(input_file)
                )
            )

        main_input, _, _ = self.add_kw(
            method.get('main_input', None),
            main_input=main_input,
            specific_input={},
        )
        _, specific_input, _ = self.add_kw(
            method.get('specific_input', None),
            main_input=[],
            specific_input=specific_input,
        )

        qcmethod = self._translate_kw(method.get('qcmethod', None), QCMETHODS)
        main_input, _, _ = self.add_kw(
            qcmethod, main_input=main_input, specific_input={}
        )
        basis = str(method.get('basis', '')).lower()
        basis = method.get('basis', '')
        if isinstance(basis, str):
            pass
        elif isinstance(basis, list):
            # todo: 1 basis for each atom
            # H 0.0 0.0 1.0 newgto "ZORA-def2-TZVP" end
            # note: sectioning, e.g.
            #     # %basis
            # newgto C "def2-TZVP" end
            # end
            # is already supported by specific input dict
            raise Exception
        else:
            raise TypeError(f'basis must be str or list, not {type(basis)}')

        main_input, _, _ = self.add_kw(
            basis, main_input=main_input, specific_input={}
        )
        props = self._translate_kw(method.get('properties', None), PROPERTIES)
        main_input, _, _ = self.add_kw(
            props, main_input=main_input, specific_input={}
        )

        _, specific_input, _ = self.add_kw(
            {'constrain': method.get('constrain', None)},
            main_input=[],
            specific_input=specific_input,
        )

        _, specific_input, _ = self.add_kw(
            {'scf': method.get('scf', None)},
            main_input=[],
            specific_input=specific_input,
        )

        return main_input, specific_input

    @classmethod
    def get_input_molecule(
        cls, input_file: Union[str, Path], parser: Parser = None
    ) -> Molecule:
        """Get molecule from input file.

        Parameters
        ----------
        input_file: :obj:`pathlib.Path`
            path to input file
        parser: :obj:`Parser`, optional
            parser for reading inputfile (must provide method 'parse')

        Returns
        -------
        :obj:`Molecule`
            Hylleraas compatible Molecule object

        """
        parse: Callable
        if parser is None:
            parse = OrcaInput.read_molecule_from_file  # type: ignore
        else:
            parse = parser.parse

        return parse(Path(input_file))  # type: ignore

    @classmethod
    def get_input_method(
        cls, input_file: Path, parser: Parser = None
    ) -> Tuple[List, Dict]:
        """Get orca input objects from input file.

        Parameters
        ----------
        input_file: :obj:`pathlib.Path`
            path to input file
        parser: :obj:`Parser`, optional
            parser for reading inputfile (must provide method 'parse')

        Returns
        -------
        Tuple(list, dict)
            main_input (starting with '!') and specific input
            (starting with '%')

        """
        parse: Callable
        if parser is None:
            parse = OrcaInput.read_method_from_file  # type: ignore
        else:
            parse = parser.parse

        return parse(Path(input_file))  # type: ignore

    def add_kw(
        self,
        kw: Any,
        main_input: Optional[list] = None,
        specific_input: Optional[dict] = None,
    ) -> Tuple[list, dict, bool]:
        """Add keyword to orca input objects.

        Parameters
        ----------
        kw : Any
            keyword
        main_input: list
            optional
        specific_input : dict
            optional

        Returns
        -------
        Tuple[list, dict, bool]
            main_input, specific_input and boolean if adding was successfull

        """
        if main_input is None:
            main_input = self.main_input.copy()
        if specific_input is None:
            specific_input = self.specific_input.copy()

        added = False
        if not kw:
            return main_input, specific_input, added

        if isinstance(kw, str):
            if kw.lower() not in main_input or kw not in main_input:
                main_input.append(kw.lower())
                added = True
        elif isinstance(kw, list):
            for k in kw:
                if k.lower() not in main_input or k not in main_input:
                    main_input.append(k)
                    added = True
        elif isinstance(kw, dict):
            if 'constrain' in kw.keys():
                if kw['constrain'] is not None:
                    specific_input['geom'] = self._gen_constrain_list(
                        kw['constrain']
                    )
                    added = True
            if 'scf' in kw.keys():
                if kw['scf'] is not None:
                    specific_input['scf'] = kw['scf']
                    added = True
            if 'properties' in kw.keys():
                prop = kw['properties']
                if prop in PROPERTIES:
                    if prop not in main_input:
                        main_input.append(PROPERTIES[prop])
                        added = True
                else:
                    raise Exception(
                        f'property {prop} not supported ', 'in Orca interface'
                    )

            kw_clean = kw.copy()
            if 'constrain' in kw_clean:
                del kw_clean['constrain']
            if 'scf' in kw_clean:
                del kw_clean['scf']
            if 'properties' in kw_clean:
                del kw_clean['properties']

            specific_input.update(kw_clean)
            added = True

        return main_input, specific_input, added

    def _gen_constrain_list(self, constr: Any) -> list:
        """Generate constrain input for orca.

        Parameters
        ----------
        constr: input string or list or list of lists
            constrain input

        Returns
        -------
        list
            corresponding list entry for orca specific input

        """
        constr = [constr] if not isinstance(constr, list) else constr
        constr = [constr] if not isinstance(constr[0], list) else constr

        if len(constr) == 0:
            return []
        const_list = ['  Constraints']
        for c in constr:
            if not isinstance(c, list):
                raise Exception(
                    "usage : ['dihedral', 1, 2, 0, 4, 120.] or "
                    "['cartesian', 4], etc. "
                )
            if c[0] == 'distance':
                cstr = '  { B ' + f'{c[1]} ' + f'{c[2]} ' + f'{c[3]} ' + ' C }'
                const_list.append(cstr)
            if c[0] == 'angle':
                cstr = (
                    '  { A '
                    + f'{c[1]} '
                    + f'{c[2]} '
                    + f'{c[3]} '
                    + f'{c[4]} '
                    + ' C }'
                )
                const_list.append(cstr)
            if c[0] == 'dihedrial':
                cstr = (
                    '  { D '
                    + f'{c[1]} '
                    + f'{c[2]} '
                    + f'{c[3]} '
                    + f'{c[4]} '
                    + f'{c[5]} '
                    + ' C }'
                )
                const_list.append(cstr)
            if c[0] == 'cartesian':
                cstr = '  { C ' + f'{c[1] }' + ' C }'
                const_list.append(cstr)
        const_list.append('  end')
        return const_list

    def _set_coord_unit(
        self, molecule: Molecule, main_input: Optional[list] = None
    ) -> list:
        """Set unit for molecule coordinates.

        Parameter
        ---------
        molecule : :obj:`Molecule`
            Molecule (hylleraas compatible)
        main_input : list, optional
            main_input, defaults to self.main_input

        Returns
        -------
        list
            updated main_input

        """
        if main_input is None:
            main_input = self.main_input.copy()
        mapping = {'bohr': 'bohrs', 'angstrom': 'angs'}

        try:
            # unit = molecule.properties.get('unit', 'bohr')
            unit = molecule.units.length[0]  # type: ignore
        except AttributeError:
            # warnings.warn(
            #     'could not find unit in molecule ' + 'assuming bohr as unit'
            # )
            unit = 'bohr'

        main_input = [m for m in main_input if m not in ('bohrs', 'angs')]
        main_input, _, _ = self.add_kw(mapping[unit], main_input=main_input)

        return main_input

    @run_sequentially
    def get_energy(
        self, molecule: Molecule, options: Optional[dict] = {}
    ) -> float:
        """Compute energy with orca.

        Paramters
        ---------
        molecule : :obj:`Molecule` (hylleraas-compatible)
            molecule for which energy should be computed

        Returns
        -------
        float
            energy

        """
        result = self.run(molecule, options=options)
        return result['energy']

    @run_sequentially
    def get_gradient(
        self,
        molecule: Molecule,
        options: Optional[dict] = {},
        gradient_type: Optional[str] = None,
    ) -> np.ndarray:
        """Compute gradient with orca.

        Paramters
        ---------
        molecule : :obj:`Molecule` (hylleraas-compatible)
            molecule for which energy should be computed
        gradient_type: str, optional
            gradient type ('analytical' or 'numerical')

        Returns
        -------
        np.ndarray
            gradient in unit Hartee/(Molecule.properties['unit'])

        """
        kw = self._set_deriv_type('gradient', gradient_type)

        opt = options.copy()
        opt.update({'main_input': [kw]})
        # result = self.run(molecule, options=opt)
        result = self.run(molecule, **opt)

        # gradient=result['gradient']
        # below has more digits
        # gradient = self.OutputParser.extract_gradient(  # type: ignore
        #     Path(result['output_file'])
        # )
        # if 'angs' in self.main_input:
        #     gradient = gradient / Constabohr2angstrom
        return result['gradient']

    def _set_deriv_type(self, derivative: str, input_type: str) -> str:
        """Set type of derivative.

        Parameter
        ---------
        derivative: str
            'gradient' or 'hessian'
        input_type : str
            'analytical' or 'numerical' or None.
            if None, taken from method input, if that is also
            None, choose analytical.

        Returns
        -------
        str
            derivative type

        """
        mapping = {
            'analytical': PROPERTIES['analytical_' + derivative],
            'numerical': PROPERTIES['numerical_' + derivative],
        }
        if input_type is not None:
            return mapping[input_type]
        else:
            return mapping[self.method.get(derivative + '_type', 'analytical')]

    @run_sequentially
    def get_hessian(
        self,
        molecule,
        options: Optional[dict] = {},
        hessian_type: Optional[str] = None,
    ) -> np.ndarray:
        """Compute hessian with orca.

        Paramters
        ---------
        molecule : :obj:`Molecule` (hylleraas-compatible)
            molecule for which energy should be computed
        hessian_type: str, optional
            hessian type ('analytical' or 'numerical')

        Returns
        -------
        np.ndarray
            hessian in unit Hartee/(Molecule.properties['unit'])**2

        """
        kw = self._set_deriv_type('hessian', hessian_type)
        opt = options.copy()
        opt.update({'main_input': [kw]})
        result = self.run(molecule, **opt)
        # hessian = self.OutputParser.extract_hessian(  # type: ignore
        #     Path(result['output_file'])  # type: ignore
        # )
        # # if 'angs' in self.main_input:
        # #     hessian = hessian / bohr2angstrom**2
        # return hessian
        return result['hessian']

    @run_sequentially
    def get_normal_modes(
        self,
        molecule,
        options: Optional[dict] = {},
        hessian_type: Optional[str] = None,
    ) -> dict:
        """Compute normal modes with orca.

        Paramters
        ---------
        molecule : :obj:`Molecule` (hylleraas-compatible)
            molecule for which energy should be computed
        hessian_type: str, optional
            hessian type ('analytical' or 'numerical')

        Returns
        -------
        dict
            frequencies (with minus sign indicating negative force constants,
            a.k.a. 'imaginary frequencies'), normal modes and hessian

        """
        kw = self._set_deriv_type('hessian', hessian_type)
        opt = options.copy()
        opt.update({'main_input': [kw]})
        result = self.run(molecule, **opt)
        return {
            'hessian': result['hessian'],
            'frequencies': result['frequencies'],
            'normal_modes': result['normal_modes'],
        }
