import copy
import pathlib
# import shutil
from dataclasses import replace
from typing import Optional

from hyobj import Molecule, Units
from hyset import (ComputeResult, ComputeSettings, RunSettings,
                   create_compute_settings, run_sequentially)

from ...manager import acompute, compute
# from ..importme import HylleraasInterface
from ...utils import get_val_from_arglist, key_in_arglist, unique_filename
from .parser import LondonInput, LondonOutput


class London:
    """London interface."""

    def __init__(self, method,
                 compute_settings: Optional[ComputeSettings] = None):
        self.method = method
        if compute_settings is None:
            self.compute_settings = create_compute_settings()
        else:
            self.compute_settings = compute_settings

        if key_in_arglist(self.method, look_for='executable'):
            self.executable = get_val_from_arglist(self.method,
                                                   look_for='executable')
        else:
            self.executable = 'london.x'

        # if key_in_arglist(self.method, look_for='filename'):
        #     self.filename = get_val_from_arglist(self.method,
        #                                          look_for='filename')
        # else:
        #     self.filename = 'mylondoncalc'

        self.run_settings = self.compute_settings.run_settings
        self.run_settings['program'] = self.executable
        self.units = self.method.get('units', Units())

        # self.filename_inp = self.filename + str('.inp')
        # self.filename_out = self.filename + str('.out')

    @classmethod
    def get_input_method(cls, *args) -> dict:
        """Get method input."""
        method: dict = {}
        for arg in args:
            if isinstance(arg, (str, pathlib.Path)):
                if pathlib.Path(arg).exists():
                    return LondonInput.london2json(str(arg))

        return method

    @classmethod
    def get_input_molecule(cls, *args) -> dict:
        """Get molecule input."""
        molecule: dict = {}
        for arg in args:
            if isinstance(arg, (str, pathlib.Path)):
                if pathlib.Path(arg).exists():
                    return LondonInput.london2mol(str(arg))

        return molecule

    def check_version(self):
        """Check version."""
        pass

    @run_sequentially
    def setup(self, molecule: Molecule, **kwargs) -> RunSettings:
        """Set up LONDON calculation."""
        run_settings = copy.deepcopy(self.run_settings)
        if kwargs.get('run_opt'):
            run_settings = replace(run_settings, **kwargs['run_opt'])

        if molecule is None:
            return run_settings

        input_str = LondonInput.gen_london_input(self.method, molecule)
        input_file = unique_filename(input_str.split('\n')) + '.inp'
        input_file_local = run_settings.abs_path(input_file)
        input_file_container = run_settings.abs_path(input_file,
                                                     container=True)
        output_file_local = input_file_local.with_suffix('.out')
        output_file_container = input_file_container.with_suffix('.out')

        files_to_write = [(input_file_local, input_str)]
        args = [input_file_container, '>', output_file_container]
        run_settings = replace(run_settings, files_to_write=files_to_write,
                               args=args, output_file=output_file_local)

        return run_settings

    def run(self, molecule, *args, **kwargs):
        """Run london calculation context-based."""
        with compute(self, molecule, *args, **kwargs) as r:
            return r

    async def arun(self, molecule, *args, **kwargs):
        """Run london calculation context-based and asyncronous."""
        async with acompute(self, molecule, *args, **kwargs) as r:
            return r

    @run_sequentially
    def parse(self, output: ComputeResult, **kwargs) -> dict:
        """Parse london output."""
        energy = LondonOutput.london_output_parser_energy(output.output_file)
        gradient = LondonOutput.london_output_parser_gradient(
            output.output_file)
        return {'energy': energy, 'gradient': gradient}

    @run_sequentially
    def get_energy(self, molecule: Molecule, **kwargs) -> float:
        """Compute energy using london.

        Parameters
        ----------
        molecule: Molecule object.
            Molecule object.

        Returns
        -------
        float
            energy: Energy in Hartree.

        """
        result = self.run(molecule, **kwargs)
        return result['energy']  # ASSUMING HARTEES

    @run_sequentially
    def get_gradient(self, molecule, **kwargs):
        """Compute gradient using london.

        Parameters
        ----------
        molecule: Molecule object.
            Molecule object.

        Returns
        -------
        np.ndarray
            gradient: gradient in Hartree/<molecule length unit>

        """
        # TODO: check units, make sure gradient is actually calculated
        result = self.run(molecule, **kwargs)
        return result['gradient']  # ASSUMING HARTEES/bohrs

    # def get_energy(self, molecule):
    #     """Compute energy with LONDON.

    #     Returns
    #     -------
    #     :obj:`float`
    #         energy

    #     """
    #     if shutil.which(self.executable) is None:
    #         raise Exception(f'{self.executable} not found in PATH')
    #     write_file = open(self.filename_inp, 'w')
    #     input_str = LondonInput.gen_london_input(self.method, molecule)
    #     write_file.write(input_str)
    #     write_file.close()
    #     # os.system(f'{self.executable}
    #     # {self.filename_inp} > {self.filename_out}')
    #     energy = LondonOutput.london_output_parser_energy(self.filename_out)
    #     return energy

    # def get_gradient(self, molecule):
    #     """Parse gradient from Energy computation with LONDON.

    #     Returns
    #     -------
    #     :obj:`array`
    #         gradient

    #     """
    #     if not os.path.isfile(self.filename_out):
    #         if shutil.which(self.executable) is None:
    #             raise Exception(f'{self.executable} not found in PATH')
    #         write_file = open(self.filename_inp, 'w')
    #         input_str = LondonInput.gen_london_input(self.method, molecule)
    #         write_file.write(input_str)
    #         write_file.close()
    #         # os.system(f'{self.executable}
    #         # {self.filename_inp} > {self.filename_out}')

    #     gradient = LondonOutput.london_output_parser_gradient(
    #         self.filename_out, molecule.num_atoms).ravel()

    #     return gradient

    # @property
    # def version(self):
    #     """Set interface version."""
    #     return '0.0'

    @property
    def author(self):
        """Set author's name or email adress."""
        return 'tilmann.bodenstein@kjemi.uio.no'
