import collections.abc
import inspect
import json
from typing import Dict, Optional

import numpy as np
from hyobj import Constants
from qcelemental import periodictable

# constants = PhysicalConstantsContext('CODATA2018')


class LondonInput:
    """Read input from London input File."""

    def __init__(self):
        pass

    @classmethod
    def london2mol(cls, filename: str) -> dict:
        """Read Molecule from London input file."""
        delim = '='
        with open(f'{filename}', 'r') as in_file:
            lines = in_file.readlines()

        atoms = []
        coordinates = []
        for i, line in enumerate(lines):
            if 'charge' in line and '-' not in line:
                j = i + 1
                break

        myline = lines[j]
        while '}' not in myline:
            if delim in myline:
                j += 1
                myline = lines[j]
            else:
                atom, *coord = myline.split()
                atoms.append(atom)
                coordinates.append(np.array(coord, dtype=np.float32))
                j += 1
                myline = lines[j]
        return {'atoms': atoms, 'coordinates': coordinates}

    @classmethod
    def london2json(cls, filename: str) -> dict:
        """Read London import quick-and-dirty."""
        delim = '='
        with open(f'{filename}', 'r') as in_file:
            lines = in_file.readlines()
        method_dict: dict = {}
        stack = []

        for line in lines:
            if '{' in line:
                key = line.split('{')[0].strip()
                stack.append(key)
                if len(stack) == 1:
                    json_str = f'{{"{key}":{{'
                    sub_dict = {}
                else:
                    json_sub_str = f'{{"{key}":{{'
            elif '}' in line:
                key = stack.pop()
                if len(stack) > 0:
                    json_sub_str += '}}'
                    json_sub_str = json_sub_str.replace(', }', '}')
                    sub_dict = json.loads(json_sub_str.replace(', }', '}'))
                else:
                    json_str += '}}'
                    json_str = json_str.replace(', }', '}')

                    my_dict = json.loads(json_str)
                    if sub_dict is not None:
                        for key2, val in sub_dict.items():
                            my_dict[key][key2] = val
                    method_dict.update(my_dict)
            if delim in line:
                kw, val = line.split(delim)
                if kw.strip() == 'charge':
                    key = stack.pop()
                    json_str += '}}'
                    json_str = json_str.replace(', }', '}')
                    my_dict = json.loads(json_str)
                    # print('updated', my_dict[key].update(sub_dict))
                    if my_dict[key] == {}:
                        my_dict[key] = sub_dict
                    else:
                        my_dict[key] = (lambda a, b: a.update(b) or a)(
                            my_dict[key], sub_dict)
                    method_dict.update(my_dict)
                    break
                if len(stack) == 1:
                    json_str += f'"{kw.strip()}" : "{val.strip()}", '
                else:
                    json_sub_str += f'"{kw.strip()}" : "{val.strip()}", '

        return method_dict

    @classmethod
    def gen_london_input(cls, method, molecule) -> str:
        """Generate input for LONDON.

        Parameters
        ----------
        method : dict
            London options dictionary
        molecule : :obj:`Hylleraas.Molecule`
            molecule input

        Returns
        -------
        :obj:`str`
            input file

        """
        if isinstance(method, dict):
            _method = method
        elif inspect.isclass(type(method)):
            _method = method.method

        london_input = LondonInput.set_london_options(_method)

        indent = 2
        input = LondonInput.printf_dict(london_input, indent=indent)

        scale = Constants.bohr2angstroms/molecule.units.length[1]
        london_atom_string = ''
        num_atoms = len(molecule.atoms)
        for i in range(0, num_atoms):
            atomic_charge = periodictable.to_atomic_number(molecule.atoms[i])
            london_atom_string += (indent+2) * ' ' + \
                'charge = {:5.2f}\n'.format(float(atomic_charge))
            london_atom_string += (
                indent+2) * ' ' + '{:>2}{:20.14f}{:20.14f}{:20.14f}\n'.format(
                    molecule.atoms[i],
                    molecule.coordinates[i][0] * scale,
                    molecule.coordinates[i][1] * scale,
                    molecule.coordinates[i][2] * scale,
                )

        return input.replace('LONDONATOMDEFINITION',
                             london_atom_string.rstrip('\n'))

    @staticmethod
    def printf_dict(d: Dict,
                    instring: Optional[str] = '',
                    indent: Optional[int] = 2) -> str:
        """Print (nested) dictionary LONDON style."""
        string = ''
        string += instring

        for key, value in d.items():
            if 'executable' in str(key):
                continue
            string += indent*' ' + str(key) + ' = '
            if isinstance(value, Dict):
                string += '{\n'
                string += LondonInput.printf_dict(value, '', indent + 2)
                if str(key) == 'system':
                    string += 'LONDONATOMDEFINITION\n'
                string += indent*' ' + '}\n'
            else:
                string += str(value) + '\n'
        return string

    @staticmethod
    def set_london_options(options: dict) -> dict:
        """Set options for LONDON.

        Parameters
        ----------
        options : :obj:`dict`
            user-defined options

        Returns
        -------
        :obj:`dict`
            full (default + user-defined) options dictionary for LONDON

        """
        # 1. read user defined options (check)
        default_opt: dict = {}
        default_opt['analysis'] = {
            'analysis-only': 'no', 'xml-print-potential-basis-overlap': 'no',
            'xml-print-potential-basis-kinetic': 'no',
            'export-operators': 'no', 'linear-dependence-tol': 1e-06
        }
        default_opt['scf'] = {
            'spin-symmetry-constraint': 'Restricted Hartree-Fock',
            'linear-dependence-tol': 1e-06, 'uhf-spin-projection': 'free',
            'disable-spin-zeeman-in-fock-matrix': 'no',
            'noisy-init-guess': 'no', 'min-scf-iterations': 0,
            'max-scf-iterations': 160, 'use-density-fitting': 'no',
            'use-admm': 'no', 'analyze-using-dft-grid': 'no', 'diis': {
                'convergence-tolerance': 1e-06, 'subspace-dimension': 7,
                'diagonalization-temperature': 0
            }
        }
        default_opt['casscf'] = {
            'max-active-orbitals': 2000, 'number-of-frozen-orbitals': 0,
            'number-of-states-in-casscf-optimization': 1,
            'number-of-ci-states': 1, 'spin-projection': 'default',
            'num-1exc-init-vec': 0, 'num-2exc-init-vec': 0,
            'use-noisy-init-vec': 'no', 'ci-max-subspace-dim': 'default',
            'ci-convergence-tol': 1e-06
        }
        default_opt['mp2'] = {
            'spin-projection': 'default', 'number-of-weights': 8,
            'basis-functions-per-block': 20, 'cs-tolerance': 0,
            'print-mp2-components': 0
        }

        default_opt['rsp'] = {
            'number-of-roots-requested': 1, 'root-selection-method': 'energy',
            'excitation-basis': 'MObasis', 'starting-guess': 'UnitGuess',
            'solver_choice': 'GenEig', 'method_choice': 'RPA',
            'precondition': 'false', 'initial-subspace-dimension': 40,
            'maximum-subspace-dimension': 240, 'convergence-tolerance': 1e-05,
            'max-rsp-iterations': 100
        }
        default_opt['coupled-cluster'] = {
            'truncation-level': 'CCSD', 'property-request': 'off',
            'spin-projection': 'default', 'max-it': 150,
            'write-t-amplitudes': 'yes', 'read-t-amplitudes': 'no', 'diis': {
                'convergence-tolerance': 1e-06, 'subspace-dimension': 7,
                'diagonalization-temperature': 0
            }
        }
        default_opt['finite-difference'] = {
            'run-finite-difference': 'false', 'run-type': 'gradient',
            'use-ref-density': 'false', 'step-size': 0.0005
        }

        default_opt['system'] = {
            'molecular-charge': 0, 'correlation-model': 'Hartree-Fock',
            'hamiltonian': {
                'electron-mass': 1, 'speed-of-light': 137.036,
                'adiabatic-connection-lambda': 1,
                'nuclear-charge-distribution': 'point charge',
                'magnetic-field': (0, 0, 0), 'gauge-origin': (0, 0, 0),
                'linear-magnetic-field': '(0, 0, 0),(0, 0, 0),(0, 0, 0)',
                'linear-magnetic-origin': (0, 0, 0), 'electric-field':
                    (0, 0, 0), 'electric-origin': (0, 0, 0),
                'linear-electric-field': '(0, 0, 0), (0,0,0), (0, 0, 0)',
                'include-basis-expanded-scalar-potential': 'no',
                'include-basis-expanded-vector-potential': 'no',
                'include-repulsion-from-external-density': 'no',
                'external-charge-density-scale-factor': 1,
                'include-coulomb-distance-to-external-density': 'no',
                'external-charge-density-distance-factor': 1, 'integral': {
                    'use-coulomb-integral-permutation-symmetry': 'yes',
                    'use-cauchy-schwarz-screening': 'yes',
                    'cauchy-schwarz-tol': 1e-15, 'boys-function-tol': 1e-17
                }, 'dft': {
                    'jp-functional': 'none', 'vrg-functional': 'none',
                    'tau-correction': 'none',
                    'vrg-hard-rs-cutoff': '62035.049',
                    'vrg-soft-density-cutoff': '1e-14',
                    'exact-exchange-fraction': 'default',
                    'radial-grid-type': 'LMG', 'allow-grid-pruning': 'yes',
                    'gc2-radial-grid-threshold': 1e-13,
                    'lebedev-angular-target-degree': 35,
                    'lebedev-angular-minimal-degree': 15,
                    'weight-screen-threshold': 1e-20, 'becke-hardness': 3,
                    'allow-becke-atom-size-correction': 'yes'
                }
            }, 'use-london-orbitals': 'no',
            'gto-contraction-type': 'all, contracted', 'basis': 'STO-3G'
        }

        user_opt = [item for item in options]
        if 'system' not in user_opt:
            raise KeyError('system key needed for generating LONDON input')

        london_keywords = [
            'analysis', 'scf', 'casscf', 'mp2', 'rsp', 'coupled-cluster',
            'finite-difference', 'system'
        ]
        london_opt: dict = {}

        for key in london_keywords:
            if key in user_opt:
                london_opt[key] = LondonInput.update_dict(
                    default_opt[key], options[key])

        return london_opt

    @staticmethod
    def update_dict(d, u):
        """Update (nested) dictionary d with dictionary u."""
        for k, v in u.items():
            if isinstance(v, collections.abc.Mapping):
                d[k] = LondonInput.update_dict(d.get(k, {}), v)
            else:
                d[k] = v
        return d


class LondonOutput:
    """Read from London output file."""

    # def __init__(self):
    #     pass

    @staticmethod
    def london_output_parser_energy(filename: str) -> float:
        """Extract energy from LONDON output file.

        Returns
        -------
        :obj:`float`
            energy

        """
        val = None
        with open(f'{filename}', 'r') as output_file:
            lines = output_file.readlines()
        for line in lines:
            if '(final) Total:' in line:
                val = float(line.split()[2])
                break

        return val

    @staticmethod
    def london_output_parser_gradient(filename: str) -> np.array:
        """Extract gradient from LONDON output file.

        Returns
        -------
        :obj:`np.array`
            gradient

        """
        scale = Constants.bohr2angstroms

        gradient = []
        i = 0
        with open(f'{filename}', 'r') as output_file:
            for line in output_file:
                if 'Molecular Energy Gradient:' in line:
                    found_grad = True
                if 'Ftotal' in line:
                    split_line = (line.replace(',',
                                               ' ').replace(')', ' ').replace(
                                                   '(', ' ').split())
                    gradient.append(float(split_line[-3]) * scale)
                    gradient.append(float(split_line[-2]) * scale)
                    gradient.append(float(split_line[-1]) * scale)
                    i = i + 1
        if not found_grad:
            raise Exception('could not find gradient in london file')
        return np.array(gradient).reshape(i, -1)

    @staticmethod
    def total_spin(filename: str) -> float:
        """Extract total spin from London output file."""
        with open(f'{filename}', 'r') as output_file:
            lines = output_file.readlines()
        for line in lines:
            if 'Total spin quantum number' in line:
                return float(line.split()[6])
        return None
