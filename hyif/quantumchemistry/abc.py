from abc import ABC, abstractmethod
from pathlib import Path
from typing import Any, Optional, Union

import numpy as np
import packaging.version as pv


class HylleraasQMInterface(ABC):
    """Base class for quantum chemistry Interfaces."""

    @property
    @abstractmethod
    def check_version(self) -> Union[None, str, pv.Version]:
        """Check the version."""

    @property
    @abstractmethod
    def author(self) -> str:
        """Set the authors email adress."""

    @abstractmethod
    def get_energy(self, molecule: Any) -> Optional[float]:
        """Compute Energy."""

    @abstractmethod
    def get_gradient(self, molecule: Any) -> Optional[np.array]:
        """Compute Gradient."""

    @abstractmethod
    def get_hessian(self, molecule: Any) -> Optional[np.array]:
        """Compute Hessian."""


class Runner(ABC):
    """Runner base class."""

    @abstractmethod
    def run(self, input_dict: dict, **kwargs) -> Union[str, dict]:
        """Run a calculation."""


class Parser(ABC):
    """Parser base class."""

    @abstractmethod
    def parse(self, input: Union[str, Path]) -> dict:
        """Parse file or input_str."""
