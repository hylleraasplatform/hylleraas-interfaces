import toml

# Load the pyproject.toml file
with open('pyproject.toml', 'r') as pyproject_file:
    data = toml.load(pyproject_file)

# Increment the patch version
version_parts = data['project']['version'].split('.')
version_parts[-1] = str(int(version_parts[-1]) + 1)
new_version = '.'.join(version_parts)

# Update the version in the data dictionary
data['project']['version'] = new_version

# Write the updated data back to pyproject.toml
with open('pyproject.toml', 'w') as pyproject_file:
    toml.dump(data, pyproject_file)

# Optionally print the new version to stdout
print(new_version)
